import {Component, Input, OnChanges} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";

@Component({
    selector: 'fs-loading-box',
    templateUrl: './loading-box.component.html',
    styleUrls: ['./loading-box.component.scss']
})
export class LoadingBoxComponent implements OnChanges{

    @Input() loadingOn: boolean = false;

    constructor(
        private ngxSpinnerService: NgxSpinnerService
    ) {}

    ngOnChanges() {
        this.loadingOn ? this.ngxSpinnerService.show() : this.ngxSpinnerService.hide();
    }

}
