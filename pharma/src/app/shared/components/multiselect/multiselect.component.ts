import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {Observable} from "rxjs";

/** @title Datepicker with custom calendar header */
@Component({
    selector: 'fs-multiselect',
    templateUrl: './multiselect.component.html',
    styleUrls: ['./multiselect.component.scss'],
})
export class MultiselectComponent implements OnInit, OnChanges{

    @Input() list: object[] = [];
    @Input() listPreSelected: object[] = [];
    @Input() listItemIdField: string = 'id';
    @Input() listItemNameField: string = 'name';
    @Input() placeholderText: string = 'Filtrar';
    @Input() reset: Observable<void> = new Observable<void>();

    @Output() listSelectedEmit = new EventEmitter<object[]>();

    public listSelected: object[] = [];
    public dropdownSettings: IDropdownSettings = {};
    public multiSelectEnable: boolean = true;

    constructor() {
        this.dropdownSettings = {
            singleSelection: false,
            idField: this.listItemIdField,
            textField: this.listItemNameField,
            searchPlaceholderText: 'Pesquise',
            selectAllText: 'Marcar todos',
            unSelectAllText: 'Desmarcar todos',
            noDataAvailablePlaceholderText: 'Não há dados disponíveis',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

    ngOnInit(): void {
        this.reset.subscribe(() => {
            this.resetMultiselect();
        });
    }

    ngOnChanges(): void {
        this.dropdownSettings = {
            singleSelection: false,
            idField: this.listItemIdField,
            textField: this.listItemNameField,
            searchPlaceholderText: 'Pesquise',
            selectAllText: 'Marcar todos',
            unSelectAllText: 'Desmarcar todos',
            noDataAvailablePlaceholderText: 'Não há dados disponíveis',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }
    selectOne(itemSelected: object) {
        // console.log(itemSelected);
        this.listSelected.push(itemSelected)
        this.listSelectedEmit.emit(this.listSelected);
    }
    deselectOne(itemSelected: object) {
        // console.log(itemSelected);
        let listHere = this.listSelected.filter(item => item[this.listItemIdField] != itemSelected[this.listItemIdField])
        this.listSelected = listHere;
        this.listSelectedEmit.emit(listHere);
    }
    selectAll(items: object[]) {
        this.listSelected = items;
        this.listSelectedEmit.emit(items);
    }
    deselectAll() {
        this.listSelected = [];
        this.listSelectedEmit.emit([]);
    }

    resetMultiselect(): void {
        this.listPreSelected = [];
        this.multiSelectEnable = false;
        setTimeout(() => {
            this.multiSelectEnable = true;
        },1);
    }
}
