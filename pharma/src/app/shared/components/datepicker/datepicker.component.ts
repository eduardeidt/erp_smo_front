import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';

import {FunctionsService} from "../../../core/services/functions/functions.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {Subject} from 'rxjs';
import {MatCalendar} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MatDateFormats} from '@angular/material/core';
import {takeUntil} from 'rxjs/operators';
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";

/** @title Datepicker with custom calendar header */
@Component({
    selector: 'fs-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent implements OnInit, OnChanges{

    @Input() currentDate: string = '';
    @Input() datepickerMonth: boolean = false;
    @Input() datepickerYear: boolean = false;
    @Input() labelText: string = '';
    @Input() maxDate: string = '';
    @Input() minDate: string = '';
    @Input() resetDate: boolean = false;
    @Input() startDate: string = '';

    @Output() dateSelectedBR = new EventEmitter();
    @Output() dateSelectedEN = new EventEmitter();

    public dateForm: FormGroup;
    public dateSelectedMonth: string = '';
    public dateSelectedSaved: string = '1';
    public dateSelectedShow: string = '';
    public dateSelectedYear: string = '';
    public minDateSaved = '1';
    public maxDateSaved = '1';
    public startDateBr = '1';

    constructor(
        public functionsService: FunctionsService,
        public formBuilder: FormBuilder,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
    ) {
        this.dateForm = this.formBuilder.group({
            'date': new FormControl('', [Validators.required]),
        });
    }

    ngOnInit(): void {
        if (this.startDate != '') {
            let date = new Date(this.startDate);
            date.setDate(date.getDate() + 1);
            this.dateSelectedSaved = this.startDate;
            this.startDateBr = this.dateSelectedShow = this.functionsService.dateENtoBR(this.startDate);
            this.dateForm.controls['date'].setValue(date);
            this.dateEmit(this.startDateBr);
        }
        if (this.startDate == '' && (this.datepickerMonth || this.datepickerYear)){
            let dateSelected = new Date();
            this.dateSelectedSaved = this.functionsService.getSmallDateEN(dateSelected);
            this.dateEmit(this.functionsService.getSmallDateBR(this.dateSelectedSaved));
        }
    }

    ngOnChanges(): void {
        this.minDateSaved = this.minDate;
        this.maxDateSaved = this.maxDate;
        if (this.dateSelectedSaved != '1' && this.resetDate == true) {
            this.dateForm.reset();
            return;
        }
        if (this.currentDate != null && this.currentDate != '') {
            let date = new Date(this.currentDate);
            date.setDate(date.getDate()+1);
            this.dateForm.controls['date'].setValue(date);
            this.dateEmit(this.functionsService.getSmallDateBR(date));
        }
        if (this.dateSelectedSaved != '1' && this.minDate != '' && this.functionsService.removeSings(this.dateSelectedSaved) < this.functionsService.removeSings(this.minDate)) {
            let date = new Date(this.minDate);
            date.setDate(date.getDate()+1);
            this.dateForm.controls['date'].setValue(date);
            this.dateSelectedSaved = this.minDate;
            this.dateEmit(this.functionsService.getSmallDateBR(date));
        }
        if (this.dateSelectedSaved != '1' && this.maxDate != '' && this.functionsService.removeSings(this.dateSelectedSaved) > this.functionsService.removeSings(this.maxDate)) {
            let date = new Date(this.maxDate);
            date.setDate(date.getDate()+1);
            this.dateForm.controls['date'].setValue(date);
            this.dateSelectedSaved = this.maxDate;
            this.dateEmit(this.functionsService.getSmallDateBR(date));
        }
    }

    chosenMonthHandler(date) {
        this.dateForm.controls['date'].setValue(date);
        this.dateEmit(this.functionsService.getSmallDateBR(date));
    }


    getDatepickerYear(): boolean {
        return this.datepickerYear;
    }
    getDatepickerYearSelected(): string {
        return this.dateSelectedYear;
    }

    chosenYearHandler(date) {
        this.dateForm.controls['date'].setValue(date);
        this.dateEmit(this.functionsService.getSmallDateBR(date));
    }

    dateEmit(date): void {
        this.dateSelectedShow = date;
        date = this.functionsService.dateBRtoEN(date);
        this.dateSelectedSaved = date;
        this.dateSelectedMonth = this.functionsService.getMonthName(date);
        this.dateSelectedYear = date.split('-')[0];
        this.dateSelectedBR.emit(this.functionsService.dateENtoBR(date));
        this.dateSelectedEN.emit(date);
    }


    public customHeader = DatepickerCustomHeaderComponent;
}

/** Custom header component for datepicker. */
@Component({
    selector: 'datepicker-custom-header',
    styleUrls: ['./datepicker.component.scss'],
    template: `
        <div class="example-header">
            <span class="example-header-label">{{periodLabel}}</span>
            <span class="arrow" (click)="previousClicked()">
                <fa-icon [icon]="iconSolidService.faChevronLeft" size="1x"></fa-icon>
            </span>
            <span class="arrow" (click)="nextClicked()">
                <fa-icon [icon]="iconSolidService.faChevronRight" size="1x"></fa-icon>
            </span>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatepickerCustomHeaderComponent<D> implements OnDestroy {

    private _destroyed = new Subject<void>();

    @Input() datepickerYear: boolean = this.datePicker.getDatepickerYear();

    constructor(
        public datePicker: DatepickerComponent,
        public iconSolidService: IconSolidService,
        private _calendar: MatCalendar<D>, private _dateAdapter: DateAdapter<D>,
        @Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats, cdr: ChangeDetectorRef) {
        _calendar.stateChanges
            .pipe(takeUntil(this._destroyed))
            .subscribe(() => cdr.markForCheck());
    }

    ngOnDestroy() {
        this._destroyed.next();
        this._destroyed.complete();
    }

    get periodLabel() {
        if (this.datepickerYear) {
            return 'Ano atual: '+this.datePicker.getDatepickerYearSelected();
        }
        return this._dateAdapter
            .format(this._calendar.activeDate, this._dateFormats.display.monthYearLabel)
            .toLocaleUpperCase();
    }

    nextClicked() {
        this._calendar.activeDate = this._dateAdapter.addCalendarYears(this._calendar.activeDate, this.datepickerYear ? 24 : 1);
    }

    previousClicked() {
        this._calendar.activeDate = this._dateAdapter.addCalendarYears(this._calendar.activeDate, this.datepickerYear ? -24 : -1);
    }

}
