import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

import {DatabaseService} from "../../../core/services/database/database.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";

@Component({
    selector: 'fs-search-line',
    templateUrl: './search-line.component.html',
    styleUrls: ['./search-line.component.scss']
})
export class SearchLineComponent implements OnInit, OnChanges {

    @Input() borderBottom: boolean = false;
    @Input() paddingLeft: string = '';
    @Input() paddingRight: string = '';
    @Input() placeholderText: string = 'Pesquisar';
    @Input() searchLineForm: FormGroup;
    @Input() searchValue: string = '';
    @Input() selectFull: boolean = false;

    @Output() onKeyPress = new EventEmitter<string>();
    @Output() onchange = new EventEmitter<string>();

    @ViewChild('searchInput') searchInput: ElementRef;

    private interval;

    constructor(
        public formBuilder : FormBuilder,
        public router: Router,
        public databaseService: DatabaseService,
        public iconSolidService: IconSolidService,
    ){
        this.searchLineForm = this.formBuilder.group({
            'searchLineInput': new FormControl('', [Validators.required]),
        });
    }

    ngOnInit(): void {
        this.searchLineForm.controls['searchLineInput'].valueChanges.subscribe((value)=>{
            this.search(value);
        })
    }

    ngOnChanges(): void {
        if (this.searchValue != '') {
            this.searchLineForm.controls['searchLineInput'].setValue(this.searchValue);
            clearTimeout(this.interval);
        }
    }

    setfocus(): void {
        this.searchInput.nativeElement.focus();
    }

    reset(): void {
        this.searchLineForm.controls['searchLineInput'].setValue('');
    }

    search(value: string): void {
        clearTimeout(this.interval);

        this.interval = setTimeout(()=>{
            this.onKeyPress.emit(value)
        },300)
    }
}
