import {Component, Inject} from "@angular/core";
import {MAT_SNACK_BAR_DATA} from "@angular/material/snack-bar";

@Component({
    template:`
        <span class="message {{data.classe}}">
            {{data.message}}
        </span>
    `,
    selector: 'snack-bar-component',

    styles: [`.message{text-align: center;}`],
})
export class SnackBarComponent {


    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {
    }

}
