import {Component, EventEmitter, Input, Output} from '@angular/core';

import {IconSolidService} from "../../../core/services/icons/icon-solid.service";

@Component({
    selector: 'fs-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {

    @Input() currentPage: number = 1;
    @Input() lastPageNum: number = 1;

    @Output() firstPage = new EventEmitter<string>();
    @Output() previousPage = new EventEmitter<string>();
    @Output() nextPage = new EventEmitter<string>();
    @Output() lastPage = new EventEmitter<string>();

    constructor(
        public iconSolidService: IconSolidService,
    ) {}

    emitFirstPage(): void {
        this.firstPage.emit();
    }
    emitReturnPage(): void {
        this.previousPage.emit();
    }
    emitNextPage(): void {
        this.nextPage.emit();
    }
    emitLastPage(): void {
        this.lastPage.emit();
    }
}
