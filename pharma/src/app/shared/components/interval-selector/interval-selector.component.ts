import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {storageKeys} from "../../../core/variables/storageKeys";
import {StorageService} from "../../../core/services/storage/storage.service";

@Component({
    selector: 'fs-interval-selector',
    templateUrl: './interval-selector.component.html',
    styleUrls: ['./interval-selector.component.scss']
})
export class IntervalSelectorComponent implements OnInit {

    @Input() collaboratorId: string = '';
    @Input() firstDateLabel: string = '';
    @Input() firstDateStart: string = '';
    @Input() secondDateLabel: string = '';
    @Input() secondDateStart: string = '';
    @Input() skipMonthBtn: boolean = true;

    @Output() firstDateSelected = new EventEmitter<string>();
    @Output() secondDateSelected = new EventEmitter<string>();

    public firstDate: string = '';
    public firstDateSkip: string = '';
    public secondDate: string = '';
    public secondDateSkip: string = '';
    public minDate: string = '';
    public maxDate: string = '';

    public companyId: string = this.storageService.decodeFromLocalStorage(storageKeys.companyId);

    constructor(
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
    ) {}

    ngOnInit(): void {
        if (this.firstDate == '' && this.firstDateStart == '') {
            let firstDate: string = this.storageService.decodeFromLocalStorage(storageKeys.intervalFirstDate);
            if (!firstDate){
                firstDate = this.functionsService.getFirstLastDay()[0];
            }
            this.firstDateStart = firstDate;
        }
        if (this.secondDate == '' && this.secondDateStart == '') {
            let secondDate: string = this.storageService.decodeFromLocalStorage(storageKeys.intervalSecondDate);
            if (!secondDate){
                secondDate = this.functionsService.getFirstLastDay()[1];
            }
            this.secondDateStart = secondDate;
        }
    }

    setFirstDate(date) {
        this.firstDate = date;
        this.storageService.encodeToLocalStorage(storageKeys.intervalFirstDate, date);
        this.emitFirstDate(date);
    }
    setSecondDate(date) {
        this.secondDate = date;
        this.storageService.encodeToLocalStorage(storageKeys.intervalSecondDate, date);
        this.emitSecondDate(date);
    }

    emitFirstDate(date) {
        this.firstDateSelected.emit(date);
    }
    emitSecondDate(date) {
        this.secondDateSelected.emit(date);
    }

    skipMonth(n): void {
        let firstDateLastDay = this.functionsService.getFirstLastDay(this.firstDate)[1];
        let firstDateNext = '';
        let secondDateLastDay = this.functionsService.getFirstLastDay(this.secondDate)[1];
        let secondDateNext = '';

        if (n == 1) {
            firstDateNext = this.functionsService.getMonthAfterEN(this.firstDate);
            secondDateNext = this.functionsService.getMonthAfterEN(this.secondDate);
        } else {
            firstDateNext = this.functionsService.getMonthBeforeEN(this.firstDate);
            secondDateNext = this.functionsService.getMonthBeforeEN(this.secondDate);
        }
        if (this.firstDate == firstDateLastDay) {
            firstDateNext = this.functionsService.getFirstLastDay(firstDateNext)[1];
        }
        if (this.secondDate == secondDateLastDay) {
            secondDateNext = this.functionsService.getFirstLastDay(secondDateNext)[1];
        }

        this.firstDate = firstDateNext;
        this.secondDate = secondDateNext;
        this.firstDateSkip = this.firstDate;
        this.secondDateSkip = this.secondDate;
    }
}
