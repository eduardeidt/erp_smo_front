import {Component, Input, OnChanges} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";

@Component({
    selector: 'fs-loading-screen',
    templateUrl: './loading-screen.component.html',
    styleUrls: ['./loading-screen.component.scss']
})
export class LoadingScreenComponent implements OnChanges{

    @Input() loadingOn: boolean = false;

    constructor(
        private ngxSpinnerService: NgxSpinnerService
    ) {}

    ngOnChanges() {
        this.loadingOn ? this.ngxSpinnerService.show() : this.ngxSpinnerService.hide();
    }

}
