import {Component, ElementRef,EventEmitter, Input, OnChanges, OnInit, Output, QueryList, Renderer2, ViewChild, ViewChildren} from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";

/** @title Datepicker with custom calendar header */
@Component({
    selector: 'fs-autocomplete',
    templateUrl: './autocomplete.component.html',
    styleUrls: ['./autocomplete.component.scss'],
})
export class AutocompleteComponent implements OnInit, OnChanges{

    @Input() list: object[] = [];
    @Input() listItemIdFieldName: string = 'id';
    @Input() objectNameDisplay: string[] = ['description'];
    @Input() placeholderText: string = 'Pesquisar';
    @Input() resetOnFocus: boolean = false;
    @Input() reset: Observable<void> = new Observable<void>();

    @Output() inputValue = new EventEmitter<string>();
    @Output() listSelectedId = new EventEmitter<string[]>();

    @ViewChild('fsAutoCompleteBox') fsAutoCompleteBox: ElementRef;
    @ViewChildren('acItem') acItem: QueryList<object>;

    public autoCompleteForm: FormGroup;
    public dropdownSettings: IDropdownSettings = {};
    public listSelected: string[] = [];
    public showAutoComplete: boolean = false;

    private interval;

    constructor(
        private formBuilder: FormBuilder,
        private renderer: Renderer2
    ) {
        this.autoCompleteForm = this.formBuilder.group({
            'allSelected': new FormControl(false, [Validators.required]),
            'text': new FormControl('', [Validators.required]),
        });
    }

    ngOnInit(): void {
        this.renderer.listen('window', 'click',(e:Event)=>{
            if (!this.fsAutoCompleteBox.nativeElement.contains(e.target)) {
                this.hideAutoCompleteBox()
            }
        });
        this.reset.subscribe(() => {
            this.resetAutoComplete();
        })
    }

    ngOnChanges(): void {
        if (this.list.length == 0) {return;}

        let listShow: object[] = [];
        this.autoCompleteForm.controls['allSelected'].setValue(false);
        this.list.forEach((item,itemIndex) => {
            let description: string = '';
            this.objectNameDisplay.forEach(field=>{
                description = description+' - '+this.list[itemIndex][field];
            })
            listShow.push({...this.list[itemIndex],itemText: description.slice(3)});
        })
        this.list = listShow;
    }

    showAutoCompleteBox(): void {
        this.showAutoComplete = true;
    }
    hideAutoCompleteBox(): void {
        this.showAutoComplete = false;
    }

    checkResetOnFocus(): void {
        if (this.resetOnFocus) {
            let text: string = this.autoCompleteForm.controls['text'].value;
            this.inputValue.emit(text);
        }
    }

    getAutoCompleteList(): void {
        let text: string = this.autoCompleteForm.controls['text'].value;
        clearTimeout(this.interval);
        this.interval = setTimeout(() => {
            this.inputValue.emit(text);
        },200)
    }

    selectId(id: string): void {
        if (this.listSelected.find(item => item == id )) {
            this.listSelected = this.listSelected.filter(item => item != id);
        } else {
            this.listSelected.push(id);
        }
    }
    verifyAll(): void {
        if (this.autoCompleteForm.controls['allSelected'].value) {
            this.list.forEach(item => {
                this.listSelected.push(item[this.listItemIdFieldName]);
            });
        } else {
            this.listSelected = [];
        }
    }

    resetAutoComplete(): void {
        this.autoCompleteForm.reset();
        this.listSelected = [];
    }

    emitList(): void {
        this.listSelectedId.emit(this.listSelected);
        this.hideAutoCompleteBox();
    }
}
