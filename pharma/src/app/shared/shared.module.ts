import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MAT_DATE_LOCALE, MatNativeDateModule} from "@angular/material/core";
import {MatIconModule} from "@angular/material/icon";
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RouterModule} from "@angular/router";

import {AutocompleteComponent} from "./components/autocomplete/autocomplete.component";
import {DatepickerComponent, DatepickerCustomHeaderComponent} from './components/datepicker/datepicker.component';
import {DateExt} from "../core/pipes/date-ext.pipe";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {LoadingScreenComponent} from "./components/loading-screen/loading-screen.component";
import {LoadingBoxComponent} from "./components/loading-box/loading-box.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MultiselectComponent} from "./components/multiselect/multiselect.component";
import {ModalConfirmComponent} from "./modal/confirm/modal-confirm.component";
import {ModalConfirmDenyComponent} from "./modal/confirm-deny/modal-confirm-deny.component";
import {ModalDefaultComponent} from "./modal/default/modal-default.component";
import {NgxMaskModule} from "ngx-mask";
import {NgxSpinnerModule} from "ngx-spinner";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {SearchLineComponent} from "./components/search-line/search-line.component";
import {SnackBarComponent} from "./components/snack-bar/snack-bar.component";
import {PaginationComponent} from "./components/pagination/pagination.component";
import {IntervalSelectorComponent} from "./components/interval-selector/interval-selector.component";

@NgModule({
    declarations: [
        AutocompleteComponent,
        DatepickerComponent,
        DatepickerCustomHeaderComponent,
        IntervalSelectorComponent,
        LoadingBoxComponent,
        LoadingScreenComponent,
        MultiselectComponent,
        PaginationComponent,
        SearchLineComponent,
        ModalConfirmComponent,
        ModalConfirmDenyComponent,
        ModalDefaultComponent,
        SnackBarComponent
    ],
    entryComponents: [
        SnackBarComponent
    ],
    exports: [
        AutocompleteComponent,
        DatepickerComponent,
        DatepickerCustomHeaderComponent,
        IntervalSelectorComponent,
        LoadingBoxComponent,
        LoadingScreenComponent,
        MatSnackBarModule,
        MultiselectComponent,
        PaginationComponent,
        SearchLineComponent,
        ModalConfirmComponent,
        ModalConfirmDenyComponent,
        ModalDefaultComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        NgxMaskModule,
        MatSnackBarModule,
        FontAwesomeModule,
        MatProgressSpinnerModule,
        NgxSpinnerModule,
        MatIconModule,
        NgMultiSelectDropDownModule,
        FormsModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatInputModule,
        MatNativeDateModule,
    ],
    providers: [
        DateExt,
        {
            provide: MAT_DATE_LOCALE, useValue: 'pt-BR'
        }
    ]
})
export class SharedModule {
}

