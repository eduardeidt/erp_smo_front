import {Component, ElementRef, EventEmitter, Input, Output, Renderer2, ViewChild} from '@angular/core';

@Component({
    selector: 'fs-modal-default',
    templateUrl: './modal-default.component.html',
    styleUrls: ['./modal-default.component.scss']
})
export class ModalDefaultComponent {

    @Input() showModal: boolean = true;
    @Input() widthClass: string = '';

    @Output() hideModal = new EventEmitter<boolean>();

    @ViewChild('modalFilter') modalFilter: ElementRef;

    constructor(
        private renderer: Renderer2
    ) {
        this.renderer.listen('window', 'click',(e:Event)=>{
            if(e.target == this.modalFilter.nativeElement){
                this.closeModal();
            }
        });
    }

    closeModal(){
        this.hideModal.emit(false);
    }
}
