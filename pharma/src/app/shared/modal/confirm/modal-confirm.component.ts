import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";

import {DatabaseService} from "../../../core/services/database/database.service";

@Component({
    selector: 'fs-modal-confirm',
    templateUrl: './modal-confirm.component.html',
    styleUrls: ['./modal-confirm.component.scss']
})
export class ModalConfirmComponent implements OnInit {

    @Input() btnText: string = '';
    @Input() showModal: boolean = true;
    @Input() title: string = '';
    @Input() text: string = '';

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() confirmClick = new EventEmitter<any>();

    constructor(
        public formBuilder: FormBuilder,
        public databaseService: DatabaseService,
        public router: Router,
    ) {}

    ngOnInit(): void {}

    closeModal(): void {
        this.hideModal.emit(false);
    }

    confirm(): void {
        this.confirmClick.emit(true);
    }
}
