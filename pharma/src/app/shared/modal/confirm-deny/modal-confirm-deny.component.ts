import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder} from "@angular/forms";

import {DatabaseService} from "../../../core/services/database/database.service";

@Component({
    selector: 'fs-modal-confirm-deny',
    templateUrl: './modal-confirm-deny.component.html',
    styleUrls: ['./modal-confirm-deny.component.scss']
})
export class ModalConfirmDenyComponent {

    @Input() btnConfirmText: string = 'Sim';
    @Input() btnDenyText: string = 'Não';
    @Input() showModal: boolean = true;
    @Input() title: string = '';
    @Input() text: string = '';

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() confirmClick = new EventEmitter<any>();
    @Output() denyClick = new EventEmitter<any>();

    constructor(
        public formBuilder: FormBuilder,
        public databaseService: DatabaseService,
    ) {}

    closeModal(): void {
        this.hideModal.emit(false);
    }

    confirm(): void {
        this.confirmClick.emit(true);
    }

    deny(): void {
        this.denyClick.emit(true);
    }
}
