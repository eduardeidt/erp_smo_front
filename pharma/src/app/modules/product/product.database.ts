import {Injectable} from '@angular/core';

import {DatabaseService} from '../../core/services/database/database.service';

@Injectable({
    providedIn: 'root'
})
export class ProductDatabase {

    constructor(
        private databaseService: DatabaseService,
    ){}


    deleteGuiadaFarmacia(params, callback) {
        this.databaseService.delete('/product/deleteGuiaDaFarmacia', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    deleteProduct(productCode, callback) {
        this.databaseService.delete('/product/' + productCode, [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    deleteProductFromPromotion(params, callback) {
        this.databaseService.delete('/productPromotion/removeFromActivePromotion', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

    getFilterListSubstance(params, callback) {
        this.databaseService.get('/substance/getSubstance', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getFilterListPharmacological(params, callback) {
        this.databaseService.get('/substance/getPharmacological', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getFilterListTherapeutic(params, callback) {
        this.databaseService.get('/substance/getTherapeutic', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getProductFilterData(callback) {
        this.databaseService.get('/product/getDataForFilterProduct', [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getProduct(productId, callback) {
        this.databaseService.get('/product/' + productId, [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getProductListGuide(params, callback) {
        this.databaseService.get('/product/indexGuiaDaFarmacia', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getProductAutocomplete(params, callback) {
        this.databaseService.get('/product/getByAutoComplete', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getProductPromotionHistoric(product_code, callback) {
        this.databaseService.get('/productPromotion/getProductPromotionHistory', {product_code})
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getProductSimpleStock(callback) {
        this.databaseService.get('/stock_simple', [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postProductList(formData, callback) {
        this.databaseService.post('/product/index', formData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postProductListSimple(formData, callback) {
        this.databaseService.post('/product/simple', formData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }



    putProduct(productData, callback) {
        this.databaseService.put('/product/update', productData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    putProductListGuide(params, callback) {
        this.databaseService.put('/product/updateGuiaDaFarmacia', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    putProductGuideFraction(params, callback) {
        this.databaseService.put('/product/setFractionGuiaDaFarmacia', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

}
