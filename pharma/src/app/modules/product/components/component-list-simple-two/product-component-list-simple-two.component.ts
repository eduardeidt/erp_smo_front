import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

import {ConfigurationDatabase} from "../../../configuration/configuration.database";
import {FunctionsService} from "../../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../../core/services/icons/icon-solid.service";
import {ProductDatabase} from "../../product.database";
import {StorageService} from "../../../../core/services/storage/storage.service";
import {storageKeys} from "../../../../core/variables/storageKeys";
import {Observable} from "rxjs";

@Component({
    selector: 'fs-product-component-list-simple-two',
    templateUrl: './product-component-list-simple-two.component.html',
    styleUrls: ['./product-component-list-simple-two.component.scss']
})
export class ProductComponentListSimpleTwoComponent implements OnInit, OnChanges {

    @Input() amountDaysToExpire: object = null;
    @Input() inputPaginationCurrentPage: number = 1;
    @Input() inputPaginationLastPage: number = 1;
    @Input() inputProductEdited: Observable<object> = new Observable<object>();
    @Input() inputProductList: object[] = null;

    @Output() reloadListEmitter: EventEmitter<number> = new EventEmitter<number>();
    @Output() productEditedDataTwoEmitter: EventEmitter<object> = new EventEmitter<object>();

    public modalProduct: boolean = false;
    public modalInactiveProduct: boolean;
    public modalProductPromotion: boolean = false;
    public modalProductRemovePromotion: boolean = false;
    public modalProductRemovePromotionGrouped: boolean = false;
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public productFilterOn: boolean = true;
    public productFilterSim: boolean = false;
    public productFilterSimCompact: boolean = false;
    public productLineForm: FormGroup;
    public productList: object[] = [{loading: true}];
    public productListCurrentOrder: string = '';
    public productListFilteredSearch: object[] = [];
    public productSelectedCode: string = null;
    public productSelectedPromotionId: string = null;
    public productSelectedSellValue: string = null;
    public productFilterReset: EventEmitter<void> = new EventEmitter<void>();
    public productMoneyIconArray: object[] = [
        {color: 'cl-red', title: 'Tributado'},
        {color: 'cl-green', title: 'ST'},
        {color: 'cl-blue', title: 'Isento'},
        {color: 'cl-gray', title: 'Não Tributado'}
    ];
    public searchForm: FormGroup;
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public titleService: Title,
        private configurationDatabase: ConfigurationDatabase,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        private productDatabase: ProductDatabase,
        public storageService: StorageService,
    ) {
        this.productLineForm = this.formBuilder.group({
            'lines': this.formBuilder.array([this.createLine()]),
        });
        this.searchForm = this.formBuilder.group({
            'search': new FormControl('', [Validators.required]),
        });
    }

    createLine(): FormGroup {
        return this.formBuilder.group({
            'commission': new FormControl('0', [Validators.required]),
            'fix_cost': new FormControl('0', [Validators.required]),
            'icms': new FormControl('0', [Validators.required]),
            'icms1': new FormControl('0', [Validators.required]),
            'icms2': new FormControl('0', [Validators.required]),
            'icms3': new FormControl('0', [Validators.required]),
            'icms4': new FormControl('0', [Validators.required]),
            'sell_price': new FormControl('0', [Validators.required]),
            'sell_price_select': new FormControl(null, [Validators.required]),
            'contribSocial': new FormControl('0', [Validators.required]),
            'irrf': new FormControl('0', [Validators.required]),
            'extraIrrf': new FormControl('0', [Validators.required]),
            'percPis': new FormControl('0', [Validators.required]),
            'percCofins': new FormControl('0', [Validators.required]),
            'mediumCostSimple': new FormControl('0', [Validators.required]),
            'typeProduct': new FormControl('0', [Validators.required]),
            'taxCode': new FormControl('0', [Validators.required]),
            'taxType': new FormControl('0', [Validators.required]),
        });
    }

    get lineForm() {
        return this.productLineForm.controls;
    }

    get lines() {
        return this.lineForm['lines'] as FormArray;
    }

    addLine() {
        this.lines.push(this.createLine());
    }

    ngOnInit(): void {
        this.inputProductEdited.subscribe(productData => {
            let lineIndex = this.productList.findIndex(item => item['code'] == productData['code']);
            this.lines.at(lineIndex).get('sell_price').setValue(productData['sell_price'])
            this.calcLine(lineIndex, productData['sell_price']);
        })
    }

    ngOnChanges(): void {
        if (this.inputProductList != null && this.inputProductList.length > 0 && !this.inputProductList[0]['loading']) {
            this.setLineForm(this.inputProductList);
            this.paginationCurrentPage = this.inputPaginationCurrentPage;
            this.paginationLastPage = this.inputPaginationLastPage;
        }
    }
    setLineForm(productList: object[]): void {
        this.lines.clear();
        this.productList = [];
        for (let productIndex = 0; productIndex < productList.length; productIndex++) {
            this.addLine();
            Object.keys(this.lines.at(productIndex).value).forEach(field => {
                this.lines.at(productIndex).get(field).setValue(productList[productIndex]['tax'][field] || 0);
            });
            // this.lines.at(productIndex).get('mediumCostSimple').setValue(productList[productIndex]['product_extra_simple_two']['medium_cost'] || 0);
            this.lines.at(productIndex).get('commission').setValue(productList[productIndex]['product_extra_simple_two']['commission']);

            let promotionSelectAvailable: boolean = true;
            if (productList[productIndex]['promotion_rule'].length == 0) {
                promotionSelectAvailable = false;
            } else {
                this.lines.at(productIndex).get('sell_price_select').setValue(productList[productIndex]['promotion_rule'][0]['price']);
            }

            this.productList.push({
                ...productList[productIndex],
                icmsValue: '',
                icms: '',
                icms1: '',
                icms2: '',
                icms3: '',
                icms4: '',
                ContribSocial: '',
                irrfCalc: '',
                extraIrrfCalc: '',
                percPis: '',
                percCofins: '',
                custo: '',
                margemLiquida: '',
                receitaLiquida: '',
                custoVariavel: '',
                lucroLiquidoValor: '',
                lucroLiquidoPerc: '',
                promotionSelectAvailable: promotionSelectAvailable,
                lucroLiquidoPercClass: '',
                tributeCompact: '',
            });
        }
        this.productList.forEach((prod, prodInd) => {
            this.inputLoad(prod['code']);
        });
        this.filterProductSearch(this.searchForm.controls['search'].value);
        this.productListCurrentOrder = 'description';
        this.productFilterOn = false;
    }
    inputLoad(productCode: string): void {
        let lineIndex = this.productList.findIndex(item => item['code'] == productCode);
        let sell_price = this.lines.at(lineIndex).get('sell_price_select').value ||  this.lines.at(lineIndex).get('sell_price').value;
        this.calcLine(lineIndex, sell_price)
    }
    calcLine(lineIndex: number, sell_price: string): void {
        let calcResult: object = this.cal_tax_product(
            parseFloat(this.lines.at(lineIndex).get('icms1').value),
            parseFloat(this.lines.at(lineIndex).get('icms2').value),
            parseFloat(this.lines.at(lineIndex).get('icms3').value),
            parseFloat(this.lines.at(lineIndex).get('icms4').value),
            parseFloat(sell_price),
            parseFloat(this.lines.at(lineIndex).get('mediumCostSimple').value),
            parseFloat(this.lines.at(lineIndex).get('typeProduct').value),
            parseFloat(this.lines.at(lineIndex).get('taxCode').value),
            parseFloat(this.lines.at(lineIndex).get('commission').value),
            parseFloat(this.lines.at(lineIndex).get('fix_cost').value),
            parseFloat(this.lines.at(lineIndex).get('taxType').value),
        );

        //valid here the value "icms" is "0";
        // console.log(calcResult);

        this.productList[lineIndex]['icms'] = !isNaN(calcResult['icms']) ? parseFloat(calcResult['icms']).toFixed(2) : '0.00';
        this.productList[lineIndex]['icmsValue'] = !isNaN(calcResult['icmsValue']) ? parseFloat(calcResult['icmsValue']).toFixed(2) : '0.00';
        this.productList[lineIndex]['custo'] = !isNaN(calcResult['custo']) ? parseFloat(calcResult['custo']).toFixed(2) : '0.00';
        this.productList[lineIndex]['margemLiquida'] = !isNaN(calcResult['margemLiquida']) ? parseFloat(calcResult['margemLiquida']).toFixed(2) : '0.00';
        this.productList[lineIndex]['receitaLiquida'] = !isNaN(calcResult['receitaLiquida']) ? parseFloat(calcResult['receitaLiquida']).toFixed(2) : '0.00';
        this.productList[lineIndex]['custoVariavel'] = !isNaN(calcResult['custoVariavel']) ? parseFloat(calcResult['custoVariavel']).toFixed(2) : '0.00';
        this.productList[lineIndex]['lucroLiquidoValor'] = !isNaN(calcResult['lucroLiquidoValor']) ? parseFloat(calcResult['lucroLiquidoValor']).toFixed(2) : '0.00';
        this.productList[lineIndex]['lucroLiquidoPerc'] = !isNaN(calcResult['lucroLiquidoPerc']) ? parseFloat(calcResult['lucroLiquidoPerc']).toFixed(2) : '0.00';

        this.lines.at(lineIndex).get('icms').setValue(!isNaN(calcResult['icms']) ? parseFloat(calcResult['icms']).toFixed(2) : '0.00');


        if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) <= 10) {
            this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-red';
        }
        if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) > 10 && parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) <= 30) {
            this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-blue';
        }
        if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) > 30) {
            this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-green';
        }
    }

    cal_tax_product(icms1: number, icms2: number, icms3: number, icms4: number, sell_price: number, mediumCostSimple: number, typeProduct: number, taxCode: number, commission: number, fix_cost: number, taxType: number): object {


        /**
         *1,2,5,6 tem 4 %
         * MONOFASICO NAO TEM PIS/COFINS, TANTO EM 25% OU 17%
         */

        /*
         TaxCode 0=>'Tributado',            1=>'Monofasico',            3=>'Aliquita 0%',            4=>'Isento',
         taxType 0=>'Tributado',            1=>'Substituição',            2=>'Isento',            3=>'Não Tributado',

        Produto ST + Monofasico/isento/aliquota 0% = 2,02%
        78936683

        ICMS ST / PIS-COFINS tributado= 2,64%
        7896549302520

        ICMS TRIBUTO + MONOFASICO = 3,38%
        7896261013483

        ICMS TRIBUTADO + PIS COFINS TRIBUTADO   4%
        7500435169486


         */
        let icms = 0;

        if (['1'].includes(taxType.toString()) && ['1', '3', '4'].includes(taxCode.toString())) {
            icms = icms1;
        } else if (['1'].includes(taxType.toString()) && ['0'].includes(taxCode.toString())) {
            icms = icms2;
        } else if (['0'].includes(taxType.toString()) && ['1', '3'].includes(taxCode.toString())) {
            icms = icms3;
            // console.log(icms);
        } else if (['0'].includes(taxType.toString()) && ['0'].includes(taxCode.toString())) {
            icms = icms4;
        }
        let arrayCheck1 = ['0', '3'];
        let icmsValue = sell_price * icms /100;
        let cost: number = (icmsValue + mediumCostSimple) + (sell_price * commission /100);
        let costTax: number = icmsValue + (sell_price * commission /100);
        let margemLiquida = sell_price * 100 / cost - 100;
        let receitaLiquida = sell_price - costTax;
        let lucroLiquidoValor = receitaLiquida - mediumCostSimple;
        let lucroLiquidoPerc = lucroLiquidoValor / sell_price * 100;
        // console.log(icms);
        // console.log(icmsValue);
        // console.log(lucroLiquidoPerc);
        // console.log(sell_price);
        // console.log(lucroLiquidoValor);
        // console.log(costTax);
        // console.log(lucroLiquidoPerc);
        return {
            icms: icms,
            icmsValue: icmsValue,
            custo: cost,
            margemLiquida: margemLiquida,
            receitaLiquida: receitaLiquida,
            lucroLiquidoValor: lucroLiquidoValor,
            lucroLiquidoPerc: lucroLiquidoPerc,
        };
    }

    validLineValue(productCode: string, inputName): void {
        let productIndex = this.productList.findIndex(item => item['code'] == productCode);
        let inputValue = this.lines.at(productIndex).get(inputName).value;
        if (inputValue.includes(',')) {
            inputValue = inputValue.split(',').join('.');
            this.lines.at(productIndex).get(inputName).setValue(inputValue);
        }
    }

    inputChange(productCode: string, sellPrice: boolean = false): void {
        let lineIndex = this.productList.findIndex(item => item['code'] == productCode);
        let sell_price = this.lines.at(lineIndex).get('sell_price_select').value ||  this.lines.at(lineIndex).get('sell_price').value;

        if (sellPrice) {
            this.lines.at(lineIndex).get('sell_price_select').setValue(null);
            sell_price = this.lines.at(lineIndex).get('sell_price').value;
        }
        this.calcLine(lineIndex, sell_price)
        this.productEditedDataTwoEmitter.emit({code: productCode,sell_price: sell_price});
    }

    orderByString(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName) {
            this.productListFilteredSearch.reverse();
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoder(this.productList, fieldName);
        }
        this.productListCurrentOrder = fieldName;
    }

    orderByNumber(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName) {
            this.productListFilteredSearch.reverse();
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoderNumber(this.productList, fieldName);
        }
        this.productListCurrentOrder = fieldName;
    }

    filterProductSearch(inputText: string): void {
        this.searchForm.controls['search'].setValue(inputText);
        this.productListFilteredSearch = this.productList.filter(item => {
            return (item['description'].concat(item['presentation'])).toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        });
    }

    productFilterToggleSimulation(): void {
        this.productFilterSim = !this.productFilterSim;
    }

    hideProduct(code: string): void {
        let productIndex = this.productList.findIndex(product => product['code'] == code);
        this.productList.splice(productIndex, 1);
        this.lines.removeAt(productIndex);
        this.filterProductSearch(this.searchForm.controls['search'].value);
    }

    showModalProduct(code: string): void {
        this.productSelectedCode = code;
        this.modalProduct = true;
    }

    hideModalProduct(): void {
        this.productSelectedCode = null;
        this.modalProduct = false;
    }

    showModalInactiveProduct(productCode: string): void {
        this.productSelectedCode = productCode;
        this.modalInactiveProduct = true;
    }

    inactiveProduct(): void {
        let codeProd: string = this.productSelectedCode;
        this.productDatabase.deleteProduct(this.productSelectedCode, (res) => {
            if (!res.error) {
                this.functionsService.openSnackBar('Produto inativado com sucesso.');
                this.productList = this.productList.filter(prod => prod['code'] != codeProd);
                this.filterProductSearch(this.searchForm.controls['search'].value);
                this.hideModalInactiveProduct();
            }
        });
    }

    hideModalInactiveProduct(): void {
        this.modalInactiveProduct = false;
        this.productSelectedCode = null;
    }

    showModalProductPromotion(code: string, sellValue: string = null): void {
        this.productSelectedCode = code;
        this.productSelectedSellValue = sellValue;
        this.modalProductPromotion = true;
    }

    hideModalProductPromotion(): void {
        this.productSelectedCode = null;
        this.productSelectedSellValue = null;
        this.modalProductPromotion = false;
    }

    showModalProductRemovePromotion(code: string, promotionId: string = null): void {
        this.productSelectedCode = code;
        this.productSelectedPromotionId = promotionId;
        this.modalProductRemovePromotion = true;
    }

    productRemovePromotion(): void {
        this.showModalProductRemoveGrouped();
    }

    showModalProductRemoveGrouped(): void {
        let productIndex = this.productList.findIndex(product => product['code'] == this.productSelectedCode);
        if (this.productList[productIndex]['product_code_main'] != null) {
            this.modalProductRemovePromotion = false;
            this.modalProductRemovePromotionGrouped = true;
        } else {
            this.confirmDenyGrouped();
        }
    }

    confirmDenyGrouped(grouped: boolean = false): void {
        this.productDatabase.deleteProductFromPromotion({
            product_code: this.productSelectedCode,
            promotion_id: this.productSelectedPromotionId,
            grouped: grouped
        }, (res) => {
            if (!res.error) {
                this.functionsService.openSnackBar('Produto removido com sucesso.');
                this.loadProductList();
                this.hideModalProductRemovePromotion();
            }
        });
    }

    hideModalProductRemovePromotion(): void {
        this.productSelectedCode = null;
        this.productSelectedPromotionId = null;
        this.modalProductRemovePromotion = false;
        this.modalProductRemovePromotionGrouped = false;
    }

    loadProductList(page: number = null): void {
        this.reloadListEmitter.emit(page);
    }

    countStorage(product: object): number {
        return product['product_extra_simple_two']['stock']
    }
    getDateBr(date: string): string {
        if (!date) return '';
        return this.functionsService.dateENtoBR(date)
    }
    getExpirationDateClass(daysToExpire: number): string {
        if (!this.amountDaysToExpire) {
            return '';
        }

        if (daysToExpire <= parseInt(this.amountDaysToExpire['short_expiration_date'])) {
            return 'cl-red';
        }

        if (daysToExpire > parseInt(this.amountDaysToExpire['short_expiration_date']) &&
            daysToExpire <= parseInt(this.amountDaysToExpire['medium_expiration_date'])) {
            return 'cl-orange';
        }

        if (daysToExpire > parseInt(this.amountDaysToExpire['medium_expiration_date']) &&
            daysToExpire <= parseInt(this.amountDaysToExpire['long_expiration_date'])) {
            return 'cl-lemon-green';
        }
    }

}
