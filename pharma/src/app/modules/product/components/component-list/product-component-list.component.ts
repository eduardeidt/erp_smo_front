import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

import {ProductDatabase} from "../../product.database";
import {FunctionsService} from "../../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../../core/services/storage/storage.service";
import {storageKeys} from "../../../../core/variables/storageKeys";
import {Observable} from "rxjs";

@Component({
    selector: 'fs-product-component-list',
    templateUrl: './product-component-list.component.html',
    styleUrls: ['./product-component-list.component.scss']
})
export class ProductComponentListComponent implements OnInit, OnChanges {

    @Input() amountDaysToExpire: object = null;
    @Input() inputPaginationCurrentPage: number = 1;
    @Input() inputPaginationLastPage: number = 1;
    @Input() inputProductEdited: Observable<object> = new Observable<object>();
    @Input() inputProductList: object[] = null;

    @Output() reloadListEmitter: EventEmitter<number> = new EventEmitter<number>();
    @Output() productEditedData: EventEmitter<object> = new EventEmitter<object>();

    public modalProduct: boolean = false;
    public modalInactiveProduct: boolean;
    public modalProductPromotion: boolean = false;
    public modalProductRemovePromotion: boolean = false;
    public modalProductRemovePromotionGrouped: boolean = false;
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public productFilterOn: boolean = true;
    public productFilterSim: boolean = false;
    public productFilterSimCompact: boolean = false;
    public productLineForm: FormGroup;
    public productList: object[] = [{loading: true}];
    public productListCurrentOrder: string = '';
    public productListFilteredSearch: object[] = [];
    public productSearchForm: FormGroup;
    public productSelectedCode: string = null;
    public productSelectedPromotionId: string = null;
    public productSelectedSellValue: string = null;
    public productFilterReset: EventEmitter<void> = new EventEmitter<void>();
    public productMoneyIconArray: object[] = [{color: 'cl-red', title: 'Tributado'}, {color: 'cl-green', title: 'ST'}, {
        color: 'cl-blue',
        title: 'Isento'
    }, {color: 'cl-gray', title: 'Não Tributado'},];
    public searchForm: FormGroup;
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public titleService: Title,
        public productDatabase: ProductDatabase,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
    ) {
        this.productLineForm = this.formBuilder.group({
            'lines': this.formBuilder.array([this.createLine()]),
        });
        this.searchForm = this.formBuilder.group({
            'search': new FormControl('', [Validators.required]),
        });
    }

    createLine(): FormGroup {
        return this.formBuilder.group({
            'commission': new FormControl('0', [Validators.required]),
            'fix_cost': new FormControl('0', [Validators.required]),
            'icms': new FormControl('0', [Validators.required]),
            'credit': new FormControl('0', [Validators.required]),
            'sell_price': new FormControl('0', [Validators.required]),
            'sell_price_select': new FormControl(null, [Validators.required]),
            'contribSocial': new FormControl('0', [Validators.required]),
            'irrf': new FormControl('0', [Validators.required]),
            'extraIrrf': new FormControl('0', [Validators.required]),
            'percPis': new FormControl('0', [Validators.required]),
            'percCofins': new FormControl('0', [Validators.required]),
            'mediumCostNormal': new FormControl('0', [Validators.required]),
            'typeProduct': new FormControl('0', [Validators.required]),
            'taxCode': new FormControl('0', [Validators.required]),
            'taxType': new FormControl('0', [Validators.required]),
            'accounting_cost': new FormControl('0', [Validators.required]),
            'diff_between_costs': new FormControl('0', [Validators.required]),
        });
    }

    get lineForm() {
        return this.productLineForm.controls;
    }

    get lines() {
        return this.lineForm['lines'] as FormArray;
    }

    addLine() {
        this.lines.push(this.createLine());
    }
    ngOnInit(): void {
        this.inputProductEdited.subscribe(productData => {
            let lineIndex = this.productList.findIndex(item => item['code'] == productData['code']);
            this.lines.at(lineIndex).get('sell_price').setValue(productData['sell_price'])
            this.calcLine(productData['productCode'], productData['sell_price']);
        })
    }

    ngOnChanges(): void {
        if (this.inputProductList != null && this.inputProductList.length > 0 && !this.inputProductList[0]['loading']) {
            this.setLineForm(this.inputProductList);
            this.paginationCurrentPage = this.inputPaginationCurrentPage;
            this.paginationLastPage = this.inputPaginationLastPage;
        }
    }
    setLineForm(productList: object[]): void {
        this.lines.clear();
        this.productList = [];
        for (let productIndex = 0; productIndex < productList.length; productIndex++) {

            this.addLine();
            Object.keys(this.lines.at(productIndex).value).forEach(field => {
                this.lines.at(productIndex).get(field).setValue(productList[productIndex]['tax'][field] || 0);
            });
            this.lines.at(productIndex).get('credit').setValue(12);
            // this.lines.at(productIndex).get('mediumCostNormal').setValue(productList[productIndex]['product_extra_normal']['medium_cost'] || 0);
            this.lines.at(productIndex).get('commission').setValue(productList[productIndex]['product_extra_normal']['commission']);

            let promotionSelectAvailable: boolean = true;
            if (productList[productIndex]['promotion_rule'].length == 0) {
                promotionSelectAvailable = false;
            } else {
                this.lines.at(productIndex).get('sell_price_select').setValue(productList[productIndex]['promotion_rule'][0]['price']);
            }
            this.productList.push({
                ...productList[productIndex],
                icmsValue: '',
                creditValue: '',
                ContribSocial: '',
                irrfCalc: '',
                extraIrrfCalc: '',
                percPis: '',
                percCofins: '',
                custo: '',
                margemLiquida: '',
                receitaLiquida: '',
                custoVariavel: '',
                lucroLiquidoValor: '',
                lucroLiquidoPerc: '',
                promotionSelectAvailable: promotionSelectAvailable,
                lucroLiquidoPercClass: '',
                tributeCompact: ''

            });
        }
        this.productList.forEach((prod, prodInd) => {
            this.inputLoad(prod['code']);
        });
        this.filterProductSearch(this.searchForm.controls['search'].value);
        this.productListCurrentOrder = 'description';
        this.productFilterOn = false;
    }
    inputLoad(productCode: string): void {
        let lineIndex = this.productList.findIndex(item => item['code'] == productCode);
        let sell_price = this.lines.at(lineIndex).get('sell_price_select').value ||  this.lines.at(lineIndex).get('sell_price').value;
        this.calcLine(lineIndex, sell_price,true)
    }
    calcLine(lineIndex: number, sell_price: string, validCredit: boolean = false) {
        let calcResult: object = this.cal_tax_product(
            parseFloat(this.lines.at(lineIndex).get('icms').value),
            parseFloat(this.lines.at(lineIndex).get('credit').value),
            parseFloat(sell_price),
            parseFloat(this.lines.at(lineIndex).get('contribSocial').value),
            parseFloat(this.lines.at(lineIndex).get('irrf').value),
            parseFloat(this.lines.at(lineIndex).get('extraIrrf').value),
            parseFloat(this.lines.at(lineIndex).get('percPis').value),
            parseFloat(this.lines.at(lineIndex).get('percCofins').value),
            parseFloat(this.lines.at(lineIndex).get('mediumCostNormal').value),
            parseFloat(this.lines.at(lineIndex).get('typeProduct').value),
            parseFloat(this.lines.at(lineIndex).get('taxCode').value),
            parseFloat(this.lines.at(lineIndex).get('commission').value),
            parseFloat(this.lines.at(lineIndex).get('fix_cost').value),
            parseFloat(this.lines.at(lineIndex).get('taxType').value),
        );

        this.productList[lineIndex]['creditValue'] = !isNaN(calcResult['creditValue']) ? parseFloat(calcResult['creditValue']).toFixed(2) : '0.00';
        this.productList[lineIndex]['icmsValue'] = !isNaN(calcResult['icmsValue']) ? parseFloat(calcResult['icmsValue']).toFixed(2) : '0.00';
        this.productList[lineIndex]['ContribSocial'] = !isNaN(calcResult['ContribSocial']) ? parseFloat(calcResult['ContribSocial']).toFixed(2) : '0.00';
        this.productList[lineIndex]['irrfCalc'] = !isNaN(calcResult['irrfCalc']) ? parseFloat(calcResult['irrfCalc']).toFixed(2) : '0.00';
        this.productList[lineIndex]['extraIrrfCalc'] = !isNaN(calcResult['extraIrrfCalc']) ? parseFloat(calcResult['extraIrrfCalc']).toFixed(2) : '0.00';
        this.productList[lineIndex]['percPis'] = !isNaN(calcResult['percPis']) ? parseFloat(calcResult['percPis']).toFixed(2) : '0.00';
        this.productList[lineIndex]['percCofins'] = !isNaN(calcResult['percCofins']) ? parseFloat(calcResult['percCofins']).toFixed(2) : '0.00';
        this.productList[lineIndex]['custo'] = !isNaN(calcResult['custo']) ? parseFloat(calcResult['custo']).toFixed(2) : '0.00';
        this.productList[lineIndex]['margemLiquida'] = !isNaN(calcResult['margemLiquida']) ? parseFloat(calcResult['margemLiquida']).toFixed(2) : '0.00';
        this.productList[lineIndex]['receitaLiquida'] = !isNaN(calcResult['receitaLiquida']) ? parseFloat(calcResult['receitaLiquida']).toFixed(2) : '0.00';
        this.productList[lineIndex]['custoVariavel'] = !isNaN(calcResult['custoVariavel']) ? parseFloat(calcResult['custoVariavel']).toFixed(2) : '0.00';
        this.productList[lineIndex]['lucroLiquidoValor'] = !isNaN(calcResult['lucroLiquidoValor']) ? parseFloat(calcResult['lucroLiquidoValor']).toFixed(2) : '0.00';
        this.productList[lineIndex]['lucroLiquidoPerc'] = !isNaN(calcResult['lucroLiquidoPerc']) ? parseFloat(calcResult['lucroLiquidoPerc']).toFixed(2) : '0.00';
        // this.productList[lineIndex]['credit'] = !isNaN(calcResult['credit']) ? calcResult['credit'].toFixed(2) : '0.00';

        if (this.productList[lineIndex]['tax']['taxType'] == 1 || this.productList[lineIndex]['tax']['taxType'] == 2 || this.productList[lineIndex]['tax']['taxType'] == 3 && validCredit) {
            this.lines.at(lineIndex).get('credit').setValue(0);
            this.lines.at(lineIndex).get('icms').setValue(0);
            // this.lines.at(lineIndex).get('icmsValue').setValue(0);
        }

        let calcTribut = calcResult['icmsValue'] + calcResult['ContribSocial'] + calcResult['irrfCalc'] + calcResult['percPis'] + calcResult['percCofins'];
        this.productList[lineIndex]['tributeCompact'] = !isNaN(calcTribut) ? calcTribut.toFixed(2) : '0.00';

        if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) <= 10) {
            this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-red';
        }
        if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) > 10 && parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) <= 30) {
            this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-blue';
        }
        if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) > 30) {
            this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-green';
        }
    }
    cal_tax_product(icms: number, credit: number, sell_price: number, contribSocial: number, irrf: number, extraIrrf: number, percPis: number, percCofins: number, mediumCostNormal: number, typeProduct: number, taxCode: number, commission: number, fix_cost: number, taxType: number): object {
        /**
         * Se for importado ou fora do Estado o credito é 4%
         *1,2,5,6 tem 4 %
         * MONOFASICO NAO TEM PIS/COFINS, TANTO EM 25% OU 17%
         */

        let ir = sell_price * irrf / 100;
        let extraIrrfCalc = sell_price * extraIrrf / 100;
        let irrfCalc: number = extraIrrfCalc + ir;
        let arrayCheck1 = ['0', '3'];
        if (arrayCheck1.includes(taxCode.toString())) {
            percCofins = sell_price * percCofins / 100;
            percPis = sell_price * percPis / 100;
        }
        let creditValue: number = 0;
        let icmsValue = 0;
        let arrayCheck2 = ['1', '2', '3'];
        if (!arrayCheck2.includes(taxType.toString())) {
            creditValue = mediumCostNormal * ((100 - credit) / 100);
            icmsValue = sell_price * icms / 100;
        }else{
            creditValue = mediumCostNormal;
        }
        contribSocial = sell_price * contribSocial / 100;
        // let creditValue = mediumCostNormal * ((100 - credit) / 100);
        let custoVariavel: number = creditValue;
        let cost: number = (icmsValue + contribSocial + irrfCalc + percPis + percCofins + creditValue) + (sell_price * commission / 100);
        let costTax: number = icmsValue + contribSocial + irrfCalc + percPis + percCofins + (sell_price * commission / 100);
        let margemLiquida = sell_price * 100 / cost - 100;
        let receitaLiquida = sell_price - costTax;
        let lucroLiquidoValor = receitaLiquida - creditValue;
        let lucroLiquidoPerc = lucroLiquidoValor / sell_price * 100;

        return {
            icmsValue: icmsValue,
            creditValue: creditValue,
            ContribSocial: contribSocial,
            irrfCalc: irrfCalc,
            extraIrrfCalc: extraIrrfCalc,
            percPis: percPis,
            percCofins: percCofins,
            custo: cost,
            margemLiquida: margemLiquida,
            receitaLiquida: receitaLiquida,
            custoVariavel: custoVariavel,
            lucroLiquidoValor: lucroLiquidoValor,
            lucroLiquidoPerc: lucroLiquidoPerc,
            // credit: credit
        };
    }

    validLineValue(productCode: string, inputName): void {
        let productIndex = this.productList.findIndex(item => item['code'] == productCode);
        let inputValue = this.lines.at(productIndex).get(inputName).value;
        if (inputValue.includes(',')) {
            inputValue = inputValue.split(',').join('.');
            this.lines.at(productIndex).get(inputName).setValue(inputValue);
        }
    }

    inputChange(productCode: string, sellPrice: boolean = false): void {
        const lineIndex = this.productList.findIndex(item => item['code'] == productCode);
        let sell_price = this.lines.at(lineIndex).get('sell_price_select').value ||  this.lines.at(lineIndex).get('sell_price').value;

        if (sellPrice) {
            this.lines.at(lineIndex).get('sell_price_select').setValue(null);
            sell_price = this.lines.at(lineIndex).get('sell_price').value;
        }
        this.calcLine(lineIndex, sell_price)
        this.productEditedData.emit({code: productCode,sell_price: sell_price});
    }

    orderByString(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName) {
            this.productListFilteredSearch.reverse();
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoder(this.productList, fieldName);
        }
        this.productListCurrentOrder = fieldName;
    }

    orderByNumber(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName) {
            this.productListFilteredSearch.reverse();
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoderNumber(this.productList, fieldName);
        }
        this.productListCurrentOrder = fieldName;
    }

    filterProductSearch(inputText: string): void {
        this.searchForm.controls['search'].setValue(inputText);
        this.productListFilteredSearch = this.productList.filter(item => {
            return (item['description'].concat(item['presentation'])).toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        });
    }

    productFilterToggleSimulation(): void {
        this.productFilterSim = !this.productFilterSim;
    }

    productFilterToggleSimulationSmall(): void {
        this.productFilterSimCompact = !this.productFilterSimCompact;
    }

    hideProduct(code: string): void {
        let productIndex = this.productList.findIndex(product => product['code'] == code);
        this.productList.splice(productIndex, 1);
        this.lines.removeAt(productIndex);
        this.filterProductSearch(this.searchForm.controls['search'].value);
    }

    showModalProduct(code: string): void {
        this.productSelectedCode = code;
        this.modalProduct = true;
    }

    hideModalProduct(): void {
        this.productSelectedCode = null;
        this.modalProduct = false;
    }

    showModalInactiveProduct(productCode: string): void {
        this.productSelectedCode = productCode;
        this.modalInactiveProduct = true;
    }

    inactiveProduct(): void {
        let codeProd: string = this.productSelectedCode;
        this.productDatabase.deleteProduct(this.productSelectedCode, (res) => {
            if (!res.error) {
                this.functionsService.openSnackBar('Produto inativado com sucesso.');
                this.productList = this.productList.filter(prod => prod['code'] != codeProd);
                this.filterProductSearch(this.searchForm.controls['search'].value);
                this.hideModalInactiveProduct();
            }
        });
    }

    hideModalInactiveProduct(): void {
        this.modalInactiveProduct = false;
        this.productSelectedCode = null;
    }

    showModalProductPromotion(code: string, sellValue: string = null): void {
        this.productSelectedCode = code;
        this.productSelectedSellValue = sellValue;
        this.modalProductPromotion = true;
    }

    hideModalProductPromotion(): void {
        this.productSelectedCode = null;
        this.productSelectedSellValue = null;
        this.modalProductPromotion = false;
    }

    showModalProductRemovePromotion(code: string, promotionId: string = null): void {
        this.productSelectedCode = code;
        this.productSelectedPromotionId = promotionId;
        this.modalProductRemovePromotion = true;
    }

    productRemovePromotion(): void {
        this.showModalProductRemoveGrouped();
    }

    showModalProductRemoveGrouped(): void {
        let productIndex = this.productList.findIndex(product => product['code'] == this.productSelectedCode);
        if (this.productList[productIndex]['product_code_main'] != null) {
            this.modalProductRemovePromotion = false;
            this.modalProductRemovePromotionGrouped = true;
        } else {
            this.confirmDenyGrouped();
        }
    }

    confirmDenyGrouped(grouped: boolean = false): void {
        this.productDatabase.deleteProductFromPromotion({
            product_code: this.productSelectedCode,
            promotion_id: this.productSelectedPromotionId,
            grouped: grouped
        }, (res) => {
            if (!res.error) {
                this.functionsService.openSnackBar('Produto removido com sucesso.');

                this.hideModalProductRemovePromotion();
            }
        });
    }

    hideModalProductRemovePromotion(): void {
        this.productSelectedCode = null;
        this.productSelectedPromotionId = null;
        this.modalProductRemovePromotion = false;
        this.modalProductRemovePromotionGrouped = false;
    }

    loadProductList(page: number = null): void {
        this.reloadListEmitter.emit(page);
    }

    countStorage(product: object): number {
        return product['product_extra_normal']['stock']
    }

    getDateBr(date: string): string {
        if (!date) return '';
        return this.functionsService.dateENtoBR(date)
    }
    getExpirationDateClass(daysToExpire: number): string {
        if (!this.amountDaysToExpire) {
            return '';
        }

        if (daysToExpire <= parseInt(this.amountDaysToExpire['short_expiration_date'])) {
            return 'cl-red';
        }

        if (daysToExpire > parseInt(this.amountDaysToExpire['short_expiration_date']) &&
            daysToExpire <= parseInt(this.amountDaysToExpire['medium_expiration_date'])) {
            return 'cl-orange';
        }

        if (daysToExpire > parseInt(this.amountDaysToExpire['medium_expiration_date']) &&
            daysToExpire <= parseInt(this.amountDaysToExpire['long_expiration_date'])) {
            return 'cl-lemon-green';
        }
    }
}
