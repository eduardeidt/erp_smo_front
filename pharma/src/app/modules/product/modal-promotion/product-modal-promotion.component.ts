import {Component, EventEmitter, Input, OnChanges, Output, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {DatabaseService} from '../../../core/services/database/database.service';
import {StorageService} from '../../../core/services/storage/storage.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {ProductDatabase} from "../product.database";

@Component({
    selector: 'fs-product-modal-promotion',
    templateUrl: './product-modal-promotion.component.html',
    styleUrls: ['./product-modal-promotion.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ProductModalPromotionComponent implements OnChanges {

    @Input() showModal: boolean = true;
    @Input() productCode: string = null;
    @Input() productSellValue: string = null;
    @Input() promotionId: string = null;
    @Input() oldPromotion: boolean = false;

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() productPosted = new EventEmitter<any>();
    @Output() productUpdated = new EventEmitter<any>();
    @Output() reloadList = new EventEmitter<void>();

    public modalGrouped: boolean = false;
    public productData: object = [];
    public productForm: FormGroup;
    public promotionList: object[];
    public promotionHistoricList: object[];
    public promos: object[] = [];

    constructor(
        public formBuilder: FormBuilder,
        public databaseService: DatabaseService,
        public functionsService: FunctionsService,
        public iconSolidService: IconSolidService,
        public productDatabase: ProductDatabase,
        public storageService: StorageService,
    ) {
        this.productForm = this.formBuilder.group({
            'alter_sell_value': new FormControl(''),
            'product_code': new FormControl('', [Validators.required]),
            'cost_price': new FormControl('', [Validators.required]),
            'description': new FormControl('' ),
            'sell_value': new FormControl(''),
            'sell_value_original': new FormControl(''),
            'new_price': new FormControl('', [Validators.required]),
            'promotion': new FormControl('', [Validators.required]),
            'discount': new FormControl('', [Validators.required, Validators.max(100), Validators.min(0)]),
            'promo_quantity': new FormControl('', [Validators.required]),
            'promo_price': new FormControl('', [Validators.required]),
            'promo_discount': new FormControl('', [Validators.required, Validators.max(100), Validators.min(0)])
        });
    }

    ngOnChanges(): void {
        if (this.productCode && this.showModal) {
            this.productDatabase.getProduct(this.productCode, (res) => {
                if (!res.error) {
                    this.productData = res.data['product'];
                    this.productForm.controls['description'].setValue(this.productData['description']);
                    this.productForm.controls['description'].disable();
                    this.productForm.controls['sell_value'].setValue(this.productData['product_extras'][0]['sell_value']);
                    this.productForm.controls['sell_value'].disable();
                    this.productForm.controls['sell_value_original'].setValue(this.productData['product_extras'][0]['sell_value']);
                    if(this.productSellValue != null) {
                        this.productForm.controls['new_price'].setValue(this.productSellValue);
                        this.calcValue('new_price');
                    }
                } else {
                    this.closeModal();
                }
            });
            this.databaseService.getPromotionNotInactive((res) => {
                if (!res.error) {
                    let firstElement = [{'id': '', 'name': ''}];
                    this.promotionList = firstElement.concat(res.data['promotions']);
                    this.promotionList = res.data['promotions'];
                }
            });
            this.productDatabase.getProductPromotionHistoric(this.productCode,(res) => {
                if (!res.error) {
                    this.promotionHistoricList = res.data;
                }
            });
            this.productForm.controls['promo_quantity'].setValue('');
            this.productForm.controls['promo_discount'].setValue('');
            this.productForm.controls['promo_price'].setValue('');
        }
    }

    addPromo(): void {
        let promoQuantity = this.productForm.controls['promo_quantity'].value;
        let promoDiscount = this.productForm.controls['promo_discount'].value;
        let promoPrice = this.productForm.controls['promo_price'].value;

        if (promoQuantity == '') {
            this.functionsService.openSnackBar('Digite a quantidade.');
            return;
        }
        if (promoDiscount == '') {
            this.functionsService.openSnackBar('Digite o desconto.');
            return;
        }
        if (promoPrice == '') {
            this.functionsService.openSnackBar('Digite a valor.');
            return;
        }
        if (this.promos.findIndex(promotion => promotion['promo_quantity'] == promoQuantity) >= 0) {
            this.functionsService.openSnackBar('Uma promoção para essa quantidade já foi cadastrada.');
            return;
        }

        this.promos.push({promo_quantity: promoQuantity, promo_discount: promoDiscount, promo_price: promoPrice});

        this.productForm.controls['promo_quantity'].setValue('');
        this.productForm.controls['promo_discount'].setValue('');
        this.productForm.controls['promo_price'].setValue('');
    }

    removePromo(i): void {
        this.promos.splice(i, 1);
    }

    calcValue(inputName): void {
        let newPriceField = this.productForm.controls['new_price'];
        let discount = parseFloat(this.productForm.controls['discount'].value);
        let newPrice = parseFloat(newPriceField.value != null ? newPriceField.value.toString().replace(',', '.') : 0);
        let sellValue = parseFloat(this.productForm.controls['sell_value'].value);

        let discountPromo = parseFloat(this.productForm.controls['promo_discount'].value);
        let newPricePromo = parseFloat(this.productForm.controls['promo_price'].value);

        //
        if (inputName == 'discount') {
            let calc = (sellValue - ((sellValue * discount) / 100)).toFixed(2);
            this.productForm.controls['new_price'].setValue(calc == 'NaN' ? sellValue : calc);
        }
        if (inputName == 'new_price') {
            let calc = (((sellValue - newPrice) / sellValue) * 100).toFixed(2);
            this.productForm.controls['discount'].setValue(calc == 'NaN' || calc == 'Infinite' ? 0 : calc);
        }

        // Promoção
        if (inputName == 'promo_discount') {
            let calc = (sellValue - ((sellValue * discountPromo) / 100)).toFixed(2);
            this.productForm.controls['promo_price'].setValue(calc == 'NaN' ? sellValue : calc);
        }
        if (inputName == 'promo_price') {
            let calc = (((sellValue - newPricePromo) / sellValue) * 100).toFixed(2);
            this.productForm.controls['promo_discount'].setValue(calc == 'NaN' || calc == 'Infinite' ? 0 : calc);
        }
    }

    sendProduct(): void {
        if (this.productForm.controls['discount'].value == '' && this.promos.length == 0) {
            this.functionsService.openSnackBar('Informe o desconto.');
            return;
        }
        if (this.productForm.controls['new_price'].value == '' && this.promos.length == 0) {
            this.functionsService.openSnackBar('Informe o novo preço.');
            return;
        }
        if (this.productForm.controls['promotion'].value == null || this.productForm.controls['promotion'].value == '') {
            this.functionsService.openSnackBar('Selecione uma promoção.');
            return;
        }

        this.showModalGrouped();
    }
    showModalGrouped(): void {
        if (this.productData['product_code_main'] != null) {
            this.modalGrouped = true;
        } else {
            this.confirmDenyGrouped();
        }
    }
    confirmDenyGrouped(grouped: boolean = false): void {
        this.postProduct({...this.productForm.value,
            promos: JSON.stringify(this.promos),
            code: this.productCode,
            grouped: grouped,
            oldPromotion: this.oldPromotion ? '1': '0',
        });
        this.modalGrouped = false;
    }
    postProduct(productData: object): void {
        this.databaseService.postProductPromo(this.productCode, productData, (res) => {
            if (!res.error) {
                this.productUpdated.emit({...productData});
                this.functionsService.openSnackBar('Produto atualizado com sucesso.');
                this.reloadList.emit();
                this.closeModal();
            }
        });
    }


    validInputValue(inputName: string): void {
        let inputValue = this.productForm.controls[inputName].value;
        if (inputValue.includes(',')) {
            this.productForm.controls[inputName].setValue(inputValue.split(',').join('.'));
        }
    }

    getDateBR(date: string): string {
        if (date != null){
            return this.functionsService.dateENtoBR(date);
        }else{
            return '';
        }
    }

    closeModal(): void {
        this.hideModal.emit(false);
        this.productForm.reset();
        this.productData = [];
        this.promotionList = [];
        this.promos = [];
    }
}
