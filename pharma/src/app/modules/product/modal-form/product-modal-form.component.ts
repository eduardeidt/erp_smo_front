import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

import {StorageService} from "../../../core/services/storage/storage.service";
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {ProductDatabase} from "../product.database";

@Component({
    selector: 'fs-product-modal-form',
    templateUrl: './product-modal-form.component.html',
    styleUrls: ['./product-modal-form.component.scss']
})
export class ProductModalFormComponent implements OnChanges {

    @Input() showModal: boolean = true;
    @Input() productCode: string;

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() productPosted = new EventEmitter<any>();
    @Output() productUpdated = new EventEmitter<any>();

    public modalGrouped: boolean = false;
    public productData: object[] = [];
    public stateFields: object = {product_code:{class:''},bars:{class:''},description:{class:''}, sell_value:{class:''}, cost_price:{class:''}, medium_cost:{class:''}, profit:{class:''}};
    public updateOn: boolean = false;

    public productForm: FormGroup;

    constructor(
        public formBuilder: FormBuilder,
        public productDatabase: ProductDatabase,
        public storageService: StorageService,
        public functionsService: FunctionsService,
    ){
        this.productForm = this.formBuilder.group({
            'product_code': new FormControl('', [Validators.required]),
            'bars': new FormControl('', [Validators.required]),
            'cost_price': new FormControl('', [Validators.required]),
            'description': new FormControl('', [Validators.required]),
            'presentation': new FormControl('', [Validators.required]),
            'sell_value': new FormControl('', [Validators.required]),
            'medium_cost': new FormControl('', [Validators.required]),
            'profit': new FormControl('', [Validators.required]),
        });
    }

    ngOnChanges(): void {
        if (this.productCode && this.showModal) {
            this.updateOn = true;
            this.productDatabase.getProduct(this.productCode,(res)=>{
                if (!res.error) {
                    this.productData = res.data['product'];
                    Object.keys(this.productForm.controls).forEach(field => {
                        this.productForm.controls[field].setValue(this.productData['product_extras'][0][field]);
                    });
                    this.productForm.controls['bars'].setValue(this.productData['bar_code']);
                    this.productForm.controls['description'].setValue(this.productData['description']);
                    this.productForm.controls['presentation'].setValue(this.productData['presentation']);
                    this.productForm.controls['product_code'].disable();
                    this.productForm.controls['medium_cost'].disable();
                } else {
                    this.closeModal();
                }
            });
        }
    }

    calcValue(inputName): void {
        // console.log('calcValue -> ipt: ',inputName);
        let profit = parseFloat(this.productForm.controls['profit'].value);
        let cost_price = parseFloat(this.productForm.controls['cost_price'].value);
        let sell_value = parseFloat(this.productForm.controls['sell_value'].value);
        if (inputName == 'cost_price' || inputName == 'profit') {
            this.productForm.controls['sell_value'].setValue(((profit/100+1) * cost_price).toFixed(2));
        }
        if (inputName == 'sell_value') {
            this.productForm.controls['cost_price'].setValue((sell_value *100 / (100+ profit)).toFixed(2));
        }
    }

    sendProduct(): void {
        // let errorFieldCount: number = 0;
        // Object.keys(this.productForm.controls).forEach(field => {
        //     if (!this.productForm.controls[field].valid && field == 'description') {
        //         errorFieldCount++;
        //     }
        // })
        // if (errorFieldCount > 0) {return;}

        this.showModalGrouped();
    }
    showModalGrouped(): void {
        if (this.productData['product_code_main'] != null) {
            this.modalGrouped = true;
        } else {
            this.confirmDenyGrouped();
        }
    }

    confirmDenyGrouped(grouped: boolean = false): void {
        this.putProduct({...this.productForm.value, code: this.productCode,grouped: grouped});
        this.modalGrouped = false;
    }

    putProduct(productData: object): void {
        this.productDatabase.putProduct(productData,(res)=>{
            if (!res.error) {
                this.productUpdated.emit({...productData});
                this.functionsService.openSnackBar('Produto atualizado com sucesso.');
                this.closeModal();
            }
        });
    }

    validPriceValue(inputName): void {
        let inputValue = this.productForm.controls[inputName].value;
        if (inputValue.includes(',')){
            inputValue = inputValue.split(',').join('.');
            this.productForm.controls[inputName].setValue(inputValue);
        }
    }

    closeModal(): void {
        this.updateOn = false;
        this.productForm.reset();
        this.hideModal.emit(false);
    }
}
