import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

import {DatabaseService} from '../../../core/services/database/database.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconRegularService} from '../../../core/services/icons/icon-regular.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {StorageService} from '../../../core/services/storage/storage.service';
import {ProductDatabase} from "../product.database";
import {Subject} from "rxjs";
import {storageKeys} from "../../../core/variables/storageKeys";

@Component({
    selector: 'fs-product-page-list',
    templateUrl: './product-page-list.component.html',
    styleUrls: ['./product-page-list.component.scss']
})
export class ProductPageListComponent {

    @Output() productEdited: EventEmitter<object> = new EventEmitter<object>();
    @Output() productSimpleEdited: EventEmitter<object> = new EventEmitter<object>();
    @Output() productSimpleTwoEdited: EventEmitter<object> = new EventEmitter<object>();

    public amountDaysToExpire: object = null;
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public productFilterOn: boolean = true;
    public productList: object[] = [{loading: true}];
    public productSearchForm: FormGroup;
    public productFilterReset: EventEmitter<void> = new EventEmitter<void>();
    public productOptionGroup: object[] = [];
    public productOptionGroupSelected: string[] = [];
    public productOptionLab: object[] = [];
    public productOptionLabSelected: string[] = [];
    public productOptionPharmacological: object[] = [];
    public productOptionPharmacologicalSelected: string[] = [];
    public productOptionPromotion: object[] = [];
    public productOptionPromotionSelected: string[] = [];
    public productOptionSubstance: object[] = [];
    public productOptionSubstanceSelected: string[] = [];
    public productOptionTherapeutic: object[] = [];
    public productOptionTherapeuticSelected: string[] = [];
    public productOptionType: object[] = [];
    public productOptionTypeSelected: string[] = [];
    public searchForm: FormGroup;
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public titleService: Title,
        public productDatabase: ProductDatabase,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
    ) {
        this.titleService.setTitle('Farmácia SMO - Produtos');
        this.productSearchForm = this.formBuilder.group({
            'commissioned': new FormControl(null, [Validators.required]),
            'description': new FormControl(null, []),
            'minProfit': new FormControl(null, []),
            'maxProfit': new FormControl(null, []),
            'in_promotion': new FormControl(null, [Validators.required]),
            'never_sold': new FormControl(null, [Validators.required]),
            'stock': new FormControl(null, [Validators.required]),
        });
        this.productDatabase.getProductFilterData((res) => {
            if (!res.error) {
                this.productOptionGroup = res.data.dataGroup;
                this.productOptionLab = res.data.dataLab;
                this.productOptionPromotion = res.data.promotions;
                this.productOptionType = res.data.dataType;
            }
        });
        const navigation = this.router.getCurrentNavigation()
        if (navigation.extras.state && navigation.extras.state.promotionIdSelected) {
            this.productOptionPromotionSelected.push(navigation.extras.state.promotionIdSelected);
            this.loadProductList(null,true);
        }
    }

    getFilterListPharmacological(inputText: string): void {
        if (inputText.length < 4) {return;}
        this.productDatabase.getFilterListPharmacological({name: inputText},(res) => {
            if (!res.error) {
                this.productOptionPharmacological = res.data;
            }
        })
    }
    getFilterListSubstance(inputText: string): void {
        if (inputText.length < 4) {return;}
        this.productDatabase.getFilterListSubstance({name: inputText},(res) => {
            if (!res.error) {
                this.productOptionSubstance = res.data;
            }
        })
    }
    getFilterListTherapeutic(inputText: string): void {
        if (inputText.length < 1) {return;}
        this.productDatabase.getFilterListTherapeutic({name: inputText},(res) => {
            if (!res.error) {
                this.productOptionTherapeutic = res.data;
            }
        })
    }

    setArraySelected(array: object[], itemIdField: string = 'id'): string[] {
        let arrayReturn: string[] = [];
        array.forEach(item => {
            arrayReturn.push(item[itemIdField]);
        });
        return arrayReturn;
    }

    quickLoadProduct(): void {
        let description = this.productSearchForm.get('description').value;
        this.resetFilter();
        this.productSearchForm.get('description').setValue(description);
        this.loadProductList();
    }

    resetFilter(): void {
        this.productSearchForm.reset();
        this.productOptionGroupSelected = [];
        this.productOptionLabSelected = [];
        this.productOptionPharmacologicalSelected = [];
        this.productOptionSubstanceSelected = [];
        this.productOptionPromotionSelected = [];
        this.productOptionTherapeuticSelected = [];
        this.productOptionTypeSelected = [];
        this.productFilterReset.emit();
    }

    loadProductList(page: number = null, resetError: boolean = false): void {
        let productData: object = {
            ...this.productSearchForm.value,
            dataGroup: this.productOptionGroupSelected,
            dataLab: this.productOptionLabSelected,
            dataPharmacological: this.productOptionPharmacologicalSelected,
            dataSubstance: this.productOptionSubstanceSelected,
            dataTherapeutic: this.productOptionTherapeuticSelected,
            dataType: this.productOptionTypeSelected,
            promotion_id: this.productOptionPromotionSelected,
            page: page
        };
        this.productDatabase.postProductList(productData, (res) => {
            if (!res.error) {
                if (res.data.products.data.length > 0) {
                    this.amountDaysToExpire = res.data.expirationDate;
                    this.productList = res.data.products.data;
                    this.paginationCurrentPage = res.data.products.current_page || 1;
                    this.paginationLastPage = res.data.products.last_page || null;
                    this.productFilterOn = false;
                } else {
                    this.functionsService.openSnackBar('Nenhum produto encontrado.');
                    this.productList = null;
                    if (resetError) {
                        this.resetFilter();
                    }
                }
            }
        });
    }


    emitProductEdited(productData): void {
        this.productEdited.emit(productData);
    }

    emitProductSimpleEdited(productData): void {
        this.productSimpleEdited.emit(productData)
    }

    emitProductSimpleTwoEdited(productData): void {
        this.productSimpleTwoEdited.emit(productData)
    }
}
