import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../core/services/storage/storage.service";
import {ProductDatabase} from "../product.database";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'fs-product-page-list-guide',
    templateUrl: './product-page-list-guide.component.html',
    styleUrls: ['./product-page-list-guide.component.scss']
})
export class ProductPageListGuideComponent implements OnInit {

    public modalPostGuide: boolean = false;
    public modalDeleteProductGuide: boolean;
    public productGuideForm: FormGroup;
    public productList: object[] = [{loading: true}];
    public productListFilteredSearch: object[] = [];
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public productListCurrentOrder: string = '';
    public productListCurrentOrderDesc: boolean = false;
    public productSelectedCode: string = null;
    public searchForm: FormGroup;

    constructor (
        public formBuilder: FormBuilder,
        public router: Router,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public productDatabase: ProductDatabase,
        public storageService: StorageService,
    ) {
        this.productGuideForm = this.formBuilder.group({
            'allSelected': new FormControl('', [Validators.required]),
            'lines': this.formBuilder.array([this.createLine()]),
        });
        this.searchForm = this.formBuilder.group({
            'search': new FormControl('', [Validators.required]),
        });
    }

    createLine(): FormGroup {
        return this.formBuilder.group({
            'selected': new FormControl(false, [Validators.required]),
            'fraction': new FormControl(false, [Validators.required]),
        });
    }

    get productGuideLinesForm() {return this.productGuideForm.controls;}
    get productGuideLines() {return this.productGuideLinesForm['lines'] as FormArray;}
    addProductGuideLine() {
        this.productGuideLines.push(this.createLine());
    }

    ngOnInit(): void {
        this.loadProductList();
    }

    loadProductList(pageNumber: number = null, resetOrder: boolean = true): void {
        this.productDatabase.getProductListGuide({page: pageNumber},(res) => {
            if (!res.error) {
                this.productList = res.data.guideList.data;
                this.paginationCurrentPage = res.data.guideList.current_page || 1;
                this.paginationLastPage = res.data.guideList.last_page || null;
                this.loadForm();
                if (resetOrder) {
                    this.productListCurrentOrder = '';
                }
            }
        });
    }
    loadForm(): void {
        this.productGuideLines.clear();
        this.productList.forEach((product,productIndex) => {
            this.productList[productIndex] = {...this.productList[productIndex], percent: this.getPercent(product)}
            this.addProductGuideLine();

        })
        this.filterProductSearch(this.searchForm.get('search').value);
    }

    filterProductSearch(inputText: string): void {
        this.searchForm.get('search').setValue(inputText);
        this.productListFilteredSearch = this.productList.filter(item => {
            return (item['description'].concat(item['presentation'])).toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        });

        if (this.productListCurrentOrder != '') {
            let selected = this.productListCurrentOrder;
            this.productListCurrentOrder = ''
            if (selected != 'bar_code' && selected != 'description') {
                if (this.productListCurrentOrderDesc) this.orderByNumber(selected);
                this.orderByNumber(selected);
            } else {
                if (this.productListCurrentOrderDesc) this.orderByString(selected);
                this.orderByString(selected);
            }
        }
    }

    orderByString(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName && this.productListCurrentOrderDesc == false) {
            this.productListFilteredSearch.reverse();
            this.productListCurrentOrderDesc = true;
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoder(this.productList, fieldName);
            this.productListCurrentOrderDesc = false;
        }
        this.productListCurrentOrder = fieldName;
    }
    orderByNumber(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName && this.productListCurrentOrderDesc == false) {
            this.productListFilteredSearch.reverse();
            this.productListCurrentOrderDesc = true;
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoderNumber(this.productList, fieldName);
            this.productListCurrentOrderDesc = false;
        }
        this.productListCurrentOrder = fieldName;
    }

    productGuideFormSelectAll(allOne: string): void {
        let checkAll: number = 0;
        for (let lineIndex = 0; lineIndex < this.productGuideLines.length; lineIndex++) {
            if (allOne == 'all') {
                this.productGuideLines.at(lineIndex).get('selected').setValue(this.productGuideForm.controls['allSelected'].value)
            } else{
                if (this.productGuideLines.at(lineIndex).get('selected').value) {checkAll++;}
            }
        }
        if (allOne == 'one') {
            this.productGuideForm.controls['allSelected'].setValue(checkAll == this.productGuideLines.length);
        }
    }


    showModalPostGuide(): void {
        this.modalPostGuide = true;
    }
    sendProductGuide(): void {
        let productList: string[] = [];
        for (let i = 0; i < this.productGuideLines.length; i++) {
            if (this.productGuideLines.at(i).get('selected').value) {
                productList.push(this.productList[i]['code']);
            }
        }
        this.productDatabase.putProductListGuide({productList: JSON.stringify(productList)}, (res) =>{
            if (!res.error) {
                this.modalPostGuide = false;
                this.loadProductList(this.paginationCurrentPage);
            }
        })
    }
    hideModalPostGuide(): void {
        this.modalPostGuide = false;
    }

    getPercent(productData): number {
        let percent: number = productData['max_price_17'] * 100 / productData['sell_value'] -100;
        return parseInt(productData['sell_value']) == 0 ? 0 : parseFloat(percent.toFixed(2));
    }
    getPercentColor(percent: number): string {
        if (percent < 0) {
            return 'cl-red';
        } else if (percent > 70) {
            return 'cl-green';
        } else {
            return 'cl-blue';
        }
    }

    toggleFraction(productCode: string, applyFraction: number) {
        let params: object = {product_code: productCode, apply_fraction: applyFraction == 1 ? 0 : 1};

        this.productDatabase.putProductGuideFraction(params, (res) =>{
            if (!res.error) {
                this.loadProductList(this.paginationCurrentPage,false);
            }
        })
    }

    showModalDeleteProduct(productCode: string): void {
        this.productSelectedCode = productCode;
        this.modalDeleteProductGuide = true;
    }
    deleteGuiadaFarmacia(): void {
        let codeProd: string = this.productSelectedCode;
        this.productDatabase.deleteProduct(this.productSelectedCode, (res) => {
            if (!res.error) {
                this.functionsService.openSnackBar('Excluído com sucesso.');
                this.productList = this.productList.filter(prod => prod['code'] != codeProd);
                this.filterProductSearch(this.searchForm.controls['search'].value);
                this.hideModalDeleteProduct();
            }
        });
    }
    hideModalDeleteProduct(): void {
        this.modalDeleteProductGuide = false;
        this.productSelectedCode = null;
    }

}
