import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

import {DatabaseService} from '../../../core/services/database/database.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconRegularService} from '../../../core/services/icons/icon-regular.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {StorageService} from '../../../core/services/storage/storage.service';
import {ProductDatabase} from "../product.database";
import {Subject} from "rxjs";
import {storageKeys} from "../../../core/variables/storageKeys";
import {ConfigurationDatabase} from "../../configuration/configuration.database";

@Component({
    selector: 'fs-product-page-list-simple',
    templateUrl: './product-page-list-simple.component.html',
    styleUrls: ['./product-page-list-simple.component.scss']
})
export class ProductPageListSimpleComponent {

    public modalProduct: boolean = false;
    public modalInactiveProduct: boolean;
    public modalProductPromotion: boolean = false;
    public modalProductRemovePromotion: boolean = false;
    public modalProductRemovePromotionGrouped: boolean = false;
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public productFilterOn: boolean = true;
    public productFilterSim: boolean = false;
    public productFilterSimCompact: boolean = false;
    public productLineForm: FormGroup;
    public productList: object[] = [{loading: true}];
    public productListCurrentOrder: string = '';
    public productListFilteredSearch: object[] = [];
    public productSearchForm: FormGroup;
    public productSelectedCode: string = null;
    public productSelectedPromotionId: string = null;
    public productSelectedSellValue: string = null;
    public productFilterReset: EventEmitter<void> = new EventEmitter<void>();
    public productOptionEnabled: boolean = true;
    public productOptionGroup: object[] = [];
    public productOptionGroupSelected: string[] = [];
    public productOptionLab: object[] = [];
    public productOptionLabSelected: string[] = [];
    public productOptionPharmacological: object[] = [];
    public productOptionPharmacologicalSelected: string[] = [];
    public productOptionPromotion: object[] = [];
    public productOptionPromotionSelected: string[] = [];
    public productOptionSubstance: object[] = [];
    public productOptionSubstanceSelected: string[] = [];
    public productOptionTherapeutic: object[] = [];
    public productOptionTherapeuticSelected: string[] = [];
    public productOptionType: object[] = [];
    public productOptionTypeSelected: string[] = [];
    public productMoneyIconArray: object[] = [{color: 'cl-red',title: 'Tributado'}, {color: 'cl-green',title: 'ST'}, {color: 'cl-blue',title: 'Isento'}, {color: 'cl-gray',title: 'Não Tributado'},]
    public searchForm: FormGroup;
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public titleService: Title,
        private configurationDatabase: ConfigurationDatabase,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        private productDatabase: ProductDatabase,
        public storageService: StorageService,
    ) {
        this.titleService.setTitle('Farmácia SMO - Produtos - Simples');
        this.productSearchForm = this.formBuilder.group({
            // 'commissioned': new FormControl(null, [Validators.required]),
            // 'description': new FormControl(null, []),
            // 'minProfit': new FormControl(null, []),
            // 'maxProfit': new FormControl(null, []),
            // 'in_promotion': new FormControl(null, [Validators.required]),
            // 'never_sold': new FormControl(null, [Validators.required]),
            // 'stock': new FormControl(null, [Validators.required]),
        });
        this.productLineForm = this.formBuilder.group({
            // 'lines': this.formBuilder.array([this.createLine()]),
        });
        this.searchForm = this.formBuilder.group({
            'search': new FormControl('', [Validators.required]),
        });
        this.productDatabase.getProductFilterData((res) => {
            // if (!res.error) {
            //     this.productOptionGroup = res.data.dataGroup;
            //     this.productOptionLab = res.data.dataLab;
            //     this.productOptionPromotion = res.data.promotions;
            //     this.productOptionType = res.data.dataType;
            // }
        });
        // this.productDatabase.getProductFilterData()
        // const navigation = this.router.getCurrentNavigation()
        // if (navigation.extras.state && navigation.extras.state.promotionIdSelected) {
        //     this.productOptionPromotionSelected.push(navigation.extras.state.promotionIdSelected);
        //     this.loadProductList(null,true);
        // }
    }
//
//     createLine(): FormGroup {
//         return this.formBuilder.group({
//             'commission': new FormControl('0', [Validators.required]),
//             'fix_cost': new FormControl('0', [Validators.required]),
//             'icms': new FormControl('0', [Validators.required]),
//             'icms1': new FormControl('0', [Validators.required]),
//             'icms2': new FormControl('0', [Validators.required]),
//             'icms3': new FormControl('0', [Validators.required]),
//             'icms4': new FormControl('0', [Validators.required]),
//             'sell_price': new FormControl('0', [Validators.required]),
//             'sell_price_select': new FormControl(null, [Validators.required]),
//             'contribSocial': new FormControl('0', [Validators.required]),
//             'irrf': new FormControl('0', [Validators.required]),
//             'extraIrrf': new FormControl('0', [Validators.required]),
//             'percPis': new FormControl('0', [Validators.required]),
//             'percCofins': new FormControl('0', [Validators.required]),
//             'mediumCost': new FormControl('0', [Validators.required]),
//             'typeProduct': new FormControl('0', [Validators.required]),
//             'taxCode': new FormControl('0', [Validators.required]),
//             'taxType': new FormControl('0', [Validators.required]),
//         });
//     }
//
//     get lineForm() {return this.productLineForm.controls;}
//     get lines() {return this.lineForm['lines'] as FormArray;}
//
//     addLine() {
//         this.lines.push(this.createLine());
//     }
//
//     getFilterListPharmacological(inputText: string): void {
//         if (inputText.length < 4) {return;}
//         this.productDatabase.getFilterListPharmacological({name: inputText},(res) => {
//             if (!res.error) {
//                 this.productOptionPharmacological = res.data;
//             }
//         })
//     }
//     getFilterListSubstance(inputText: string): void {
//         if (inputText.length < 4) {return;}
//         this.productDatabase.getFilterListSubstance({name: inputText},(res) => {
//             if (!res.error) {
//                 this.productOptionSubstance = res.data;
//             }
//         })
//     }
//     getFilterListTherapeutic(inputText: string): void {
//         if (inputText.length < 1) {return;}
//         this.productDatabase.getFilterListTherapeutic({name: inputText},(res) => {
//             if (!res.error) {
//                 this.productOptionTherapeutic = res.data;
//             }
//         })
//     }
//
//     setArraySelected(array: object[], itemIdField: string = 'id'): string[] {
//         let arrayReturn: string[] = [];
//         array.forEach(item => {
//             arrayReturn.push(item[itemIdField]);
//         });
//         return arrayReturn;
//     }
//
//     resetFilter(): void {
//         this.productSearchForm.reset();
//         this.productOptionGroupSelected = [];
//         this.productOptionLabSelected = [];
//         this.productOptionPharmacologicalSelected = [];
//         this.productOptionSubstanceSelected = [];
//         this.productOptionPromotionSelected = [];
//         this.productOptionTherapeuticSelected = [];
//         this.productOptionTypeSelected = [];
//         this.productFilterReset.emit();
//     }
//
//     quickLoadProduct(): void {
//         console.log('quick load');
//         let description = this.productSearchForm.get('description').value;
//         this.resetFilter();
//         this.productSearchForm.get('description').setValue(description);
//         this.loadProductList();
//     }
//
//     loadProductList(page: number = null, resetError: boolean = false): void {
//         console.log('load');
//         let productData: object = {
//             ...this.productSearchForm.value,
//             dataGroup: this.productOptionGroupSelected,
//             dataLab: this.productOptionLabSelected,
//             dataPharmacological: this.productOptionPharmacologicalSelected,
//             dataSubstance: this.productOptionSubstanceSelected,
//             dataTherapeutic: this.productOptionTherapeuticSelected,
//             dataType: this.productOptionTypeSelected,
//             promotion_id: this.productOptionPromotionSelected,
//             page: page
//         };
//         this.productDatabase.postProductListSimple(productData, (res) => {
//             if (!res.error) {
//                 if (res.data.products.data.length > 0) {
//                     this.setLineForm(res.data.products.data);
//                     this.paginationCurrentPage = res.data.products.current_page || 1;
//                     this.paginationLastPage = res.data.products.last_page || null;
//                 } else {
//                     this.functionsService.openSnackBar('Nenhum produto encontrado.');
//                     if (resetError) {
//                         this.resetFilter();
//                     }
//                 }
//             }
//         });
//     }
//     setLineForm(productList: object[]): void {
//         this.lines.clear();
//         this.productList = [];
//         for (let productIndex = 0; productIndex < productList.length; productIndex++) {
//             this.addLine();
//             Object.keys(this.lines.at(productIndex).value).forEach(field => {
//                 this.lines.at(productIndex).get(field).setValue(productList[productIndex]['tax'][field] || 0);
//             });
//             let promotionSelectAvailable = true;
//             if (productList[productIndex]['promotion_rule'].length == 0) {
//                 promotionSelectAvailable = false;
//             } else {
//                 this.lines.at(productIndex).get('sell_price_select').setValue(productList[productIndex]['promotion_rule'][0]['price']);
//             }
//             this.lines.at(productIndex).get('commission').setValue(productList[productIndex]['product_extras'][0]['commission']);
//             this.productList.push({
//                 ...productList[productIndex],
//                 icmsValue: '',
//                 icms: '',
//                 icms1: '',
//                 icms2: '',
//                 icms3: '',
//                 icms4: '',
//                 ContribSocial: '',
//                 irrfCalc: '',
//                 extraIrrfCalc: '',
//                 percPis: '',
//                 percCofins: '',
//                 custo: '',
//                 margemLiquida: '',
//                 receitaLiquida: '',
//                 custoVariavel: '',
//                 lucroLiquidoValor: '',
//                 lucroLiquidoPerc: '',
//                 promotionSelectAvailable: promotionSelectAvailable,
//                 lucroLiquidoPercClass: '',
//                 tributeCompact: '',
//             });
//         }
//         this.productList.forEach((prod, prodInd) => {
//             this.calcLine(prod['code'],false    );
//         });
//         this.filterProductSearch(this.searchForm.controls['search'].value);
//         this.productListCurrentOrder = 'description';
//         this.productFilterOn = false;
//     }
//     calcLine(productCode: string, sellPrice: boolean = false){
//         let lineIndex = this.productList.findIndex(item => item['code'] == productCode);
//         let sell_price = this.lines.at(lineIndex).get('sell_price_select').value ||  this.lines.at(lineIndex).get('sell_price').value;
//
//         if (sellPrice) {
//             this.lines.at(lineIndex).get('sell_price_select').setValue(null);
//             sell_price = this.lines.at(lineIndex).get('sell_price').value;
//         }
//
//         let calcResult: object = this.cal_tax_product(
//             parseFloat(this.lines.at(lineIndex).get('icms1').value),
//             parseFloat(this.lines.at(lineIndex).get('icms2').value),
//             parseFloat(this.lines.at(lineIndex).get('icms3').value),
//             parseFloat(this.lines.at(lineIndex).get('icms4').value),
//             parseFloat(sell_price),
//             parseFloat(this.lines.at(lineIndex).get('mediumCost').value),
//             parseFloat(this.lines.at(lineIndex).get('typeProduct').value),
//             parseFloat(this.lines.at(lineIndex).get('taxCode').value),
//             parseFloat(this.lines.at(lineIndex).get('commission').value),
//             parseFloat(this.lines.at(lineIndex).get('fix_cost').value),
//             parseFloat(this.lines.at(lineIndex).get('taxType').value),
//         );
//
//         //valid here the value "icms" is "0";
//         console.log(calcResult);
//
//         this.productList[lineIndex]['icms'] = !isNaN(calcResult['icms']) ? parseFloat(calcResult['icms']).toFixed(2) : '0.00';
//         this.productList[lineIndex]['icmsValue'] = !isNaN(calcResult['icmsValue']) ? parseFloat(calcResult['icmsValue']).toFixed(2) : '0.00';
//         this.productList[lineIndex]['custo'] = !isNaN(calcResult['custo']) ? parseFloat(calcResult['custo']).toFixed(2) : '0.00';
//         this.productList[lineIndex]['margemLiquida'] = !isNaN(calcResult['margemLiquida']) ? parseFloat(calcResult['margemLiquida']).toFixed(2) : '0.00';
//         this.productList[lineIndex]['receitaLiquida'] = !isNaN(calcResult['receitaLiquida']) ? parseFloat(calcResult['receitaLiquida']).toFixed(2) : '0.00';
//         this.productList[lineIndex]['custoVariavel'] = !isNaN(calcResult['custoVariavel']) ? parseFloat(calcResult['custoVariavel']).toFixed(2) : '0.00';
//         this.productList[lineIndex]['lucroLiquidoValor'] = !isNaN(calcResult['lucroLiquidoValor']) ? parseFloat(calcResult['lucroLiquidoValor']).toFixed(2) : '0.00';
//         this.productList[lineIndex]['lucroLiquidoPerc'] = !isNaN(calcResult['lucroLiquidoPerc']) ? parseFloat(calcResult['lucroLiquidoPerc']).toFixed(2) : '0.00';
//
//         this.lines.at(lineIndex).get('icms').setValue(!isNaN(calcResult['icms']) ? parseFloat(calcResult['icms']).toFixed(2) : '0.00');
//
//
//         if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) <= 10){
//             this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-red';
//         }
//         if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) > 10 && parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) <= 30){
//             this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-blue';
//         }
//         if (parseInt(this.productList[lineIndex]['lucroLiquidoPerc']) > 30){
//             this.productList[lineIndex]['lucroLiquidoPercClass'] = 'cl-green';
//         }
//     }
//     cal_tax_product(icms1: number,icms2: number,icms3: number,icms4: number, sell_price: number, mediumCost: number,typeProduct: number, taxCode: number, commission: number, fix_cost: number, taxType: number): object {
//
//
//         /**
//          *1,2,5,6 tem 4 %
//          * MONOFASICO NAO TEM PIS/COFINS, TANTO EM 25% OU 17%
//          */
//
//         /*
//  TaxCode 0=>'Tributado',            1=>'Monofasico',            3=>'Aliquita 0%',            4=>'Isento',
//  taxType 0=>'Tributado',            1=>'Substituição',            2=>'Isento',            3=>'Não Tributado',
//
// Produto ST + Monofasico/isento/aliquota 0% = 2,02%
// 78936683
//
// ICMS ST / PIS-COFINS tributado= 2,64%
// 7896549302520
//
// ICMS TRIBUTO + MONOFASICO = 3,38%
// 7896261013483
//
// ICMS TRIBUTADO + PIS COFINS TRIBUTADO   4%
// 7500435169486
//
//
//          */
//         let icms = 0;
//
//         if (['1'].includes(taxType.toString())  && ['1','3','4'].includes(taxCode.toString()) ) {
//             icms = icms1;
//         }else if(['1'].includes(taxType.toString()) && ['0'].includes(taxCode.toString()) ) {
//             icms = icms2;
//         }else if(['0'].includes(taxType.toString()) && ['1','3'].includes(taxCode.toString()) ) {
//             icms = icms3;
//             console.log(icms);
//         }else if(['0'].includes(taxType.toString()) && ['0'].includes(taxCode.toString()) ) {
//             icms = icms4;
//         }
//         let arrayCheck1 = ['0', '3'];
//         let icmsValue = sell_price * icms /100;
//         let cost: number = (icmsValue + mediumCost) + (sell_price * commission /100);
//         let costTax: number = icmsValue + (sell_price * commission /100);
//         let margemLiquida = sell_price * 100 / cost - 100;
//         let receitaLiquida = sell_price - costTax;
//         let lucroLiquidoValor = receitaLiquida - mediumCost;
//         let lucroLiquidoPerc = lucroLiquidoValor / sell_price * 100;
//         // console.log(icms);
//         // console.log(icmsValue);
//          // console.log(lucroLiquidoPerc);
//         // console.log(sell_price);
//         // console.log(lucroLiquidoValor);
//         // console.log(costTax);
//         // console.log(lucroLiquidoPerc);
//         return {
//             icms: icms,
//             icmsValue: icmsValue,
//             custo: cost,
//             margemLiquida: margemLiquida,
//             receitaLiquida: receitaLiquida,
//             lucroLiquidoValor: lucroLiquidoValor,
//             lucroLiquidoPerc: lucroLiquidoPerc,
//         };
//     }
//     validLineValue(productCode: string, inputName): void {
//         let productIndex = this.productList.findIndex(item => item['code'] == productCode);
//         let inputValue = this.lines.at(productIndex).get(inputName).value;
//         if (inputValue.includes(',')) {
//             inputValue = inputValue.split(',').join('.');
//             this.lines.at(productIndex).get(inputName).setValue(inputValue);
//         }
//     }
//
//     orderByString(fieldName: string): void {
//         if (this.productListCurrentOrder == fieldName) {
//             this.productListFilteredSearch.reverse();
//         } else {
//             this.productListFilteredSearch = this.functionsService.arrayReoder(this.productList,fieldName);
//         }
//         this.productListCurrentOrder = fieldName;
//     }
//     orderByNumber(fieldName: string): void {
//         if (this.productListCurrentOrder == fieldName) {
//             this.productListFilteredSearch.reverse();
//         } else {
//             this.productListFilteredSearch = this.functionsService.arrayReoderNumber(this.productList,fieldName);
//         }
//         this.productListCurrentOrder = fieldName;
//     }
//
//     filterProductSearch(inputText: string): void {
//         this.searchForm.controls['search'].setValue(inputText);
//         this.productListFilteredSearch = this.productList.filter(item => {
//             return (item['description'].concat(item['presentation'])).toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
//         });
//     }
//     productFilterToggleSimulation(): void {
//         this.productFilterSim = !this.productFilterSim;
//     }
//
//     hideProduct(code: string): void {
//         let productIndex = this.productList.findIndex(product => product['code'] == code);
//         this.productList.splice(productIndex,1);
//         this.lines.removeAt(productIndex);
//         this.filterProductSearch(this.searchForm.controls['search'].value);
//     }
//
//     showModalProduct(code: string): void {
//         this.productSelectedCode = code;
//         this.modalProduct = true;
//     }
//     hideModalProduct(): void {
//         this.productSelectedCode = null;
//         this.modalProduct = false;
//     }
//
//     showModalInactiveProduct(productCode: string): void {
//         this.productSelectedCode = productCode;
//         this.modalInactiveProduct = true;
//     }
//     inactiveProduct(): void {
//         let codeProd: string = this.productSelectedCode;
//         this.productDatabase.deleteProduct(this.productSelectedCode, (res) => {
//             if (!res.error) {
//                 this.functionsService.openSnackBar('Produto inativado com sucesso.');
//                 this.productList = this.productList.filter(prod => prod['code'] != codeProd);
//                 this.filterProductSearch(this.searchForm.controls['search'].value);
//                 this.hideModalInactiveProduct();
//             }
//         });
//     }
//     hideModalInactiveProduct(): void {
//         this.modalInactiveProduct = false;
//         this.productSelectedCode = null;
//     }
//
//     showModalProductPromotion(code: string,sellValue: string = null): void {
//         this.productSelectedCode = code;
//         this.productSelectedSellValue = sellValue;
//         this.modalProductPromotion = true;
//     }
//     hideModalProductPromotion(): void {
//         this.productSelectedCode = null;
//         this.productSelectedSellValue = null;
//         this.modalProductPromotion = false;
//     }
//
//     showModalProductRemovePromotion(code: string, promotionId: string = null): void {
//         this.productSelectedCode = code;
//         this.productSelectedPromotionId = promotionId;
//         this.modalProductRemovePromotion = true;
//     }
//     productRemovePromotion(): void {
//         this.showModalProductRemoveGrouped()
//     }
//     showModalProductRemoveGrouped(): void {
//         let productIndex = this.productList.findIndex(product => product['code'] == this.productSelectedCode);
//         if (this.productList[productIndex]['product_code_main'] != null) {
//             this.modalProductRemovePromotion = false;
//             this.modalProductRemovePromotionGrouped = true;
//         } else {
//             this.confirmDenyGrouped();
//         }
//     }
//     confirmDenyGrouped(grouped: boolean = false): void {
//         this.productDatabase.deleteProductFromPromotion({product_code: this.productSelectedCode, promotion_id:  this.productSelectedPromotionId,grouped: grouped}, (res) => {
//             if (!res.error) {
//                 this.functionsService.openSnackBar('Produto removido com sucesso.');
//                 this.loadProductList();
//                 this.hideModalProductRemovePromotion();
//             }
//         });
//     }
//     hideModalProductRemovePromotion(): void {
//         this.productSelectedCode = null;
//         this.productSelectedPromotionId = null;
//         this.modalProductRemovePromotion = false;
//         this.modalProductRemovePromotionGrouped = false;
//     }
//
//     countStorage(productExtraData: object[]): number {
//         let totalStorage: number = 0;
//         productExtraData.forEach(item => {
//             totalStorage = parseInt(item['stock'])+totalStorage
//         })
//         return totalStorage;
//     }

}
