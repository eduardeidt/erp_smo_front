import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../core/services/storage/storage.service";
import {ProductDatabase} from "../product.database";

@Component({
    selector: 'fs-product-page-list-simple-stock',
    templateUrl: './product-page-list-simple-stock.component.html',
    styleUrls: ['./product-page-list-simple-stock.component.scss']
})
export class ProductPageListSimpleStockComponent implements OnInit {

    public productForm: FormGroup;
    public productList: object[] = [{loading: true}];
    public productListFilteredSearch: object[] = [];
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public productListCurrentOrder: string = '';
    public productListCurrentOrderDesc: boolean = false;
    public productSelectedCode: string = null;
    public searchForm: FormGroup;


    constructor (
        public formBuilder: FormBuilder,
        public router: Router,
        private titleService: Title,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public productDatabase: ProductDatabase,
        public storageService: StorageService,

) {
        this.titleService.setTitle('Farmácia SMO - Produtos - Simples');
        this.productForm = this.formBuilder.group({
            'allSelected': new FormControl('', [Validators.required]),
            'lines': this.formBuilder.array([this.createLine()]),
        });
        this.searchForm = this.formBuilder.group({
            'search': new FormControl('', [Validators.required]),
        });
    }

    createLine(): FormGroup {
        return this.formBuilder.group({
            'selected': new FormControl(false, [Validators.required]),
            'fraction': new FormControl(false, [Validators.required]),
        });
    }

    get productSimpleStockLinesForm() {return this.productForm.controls;}
    get productSimpleStockLines() {return this.productSimpleStockLinesForm['lines'] as FormArray;}
    addProductSimpleStockLine() {
        this.productSimpleStockLines.push(this.createLine());
    }
    ngOnInit(): void {
        this.loadProductList();
    }

    loadProductList(pageNumber: number = null, resetOrder: boolean = true): void {
        this.productDatabase.getProductSimpleStock((res) => {
            if (!res.error) {
                this.productList = res.data.productsList;
                this.loadForm();
                if (resetOrder) {
                    this.productListCurrentOrder = '';
                }
            }
        });
    }
    loadForm(): void {
        this.productSimpleStockLines.clear();
        this.productList.forEach((product,productIndex) => {
            this.productList[productIndex] = {...this.productList[productIndex]}
            this.addProductSimpleStockLine();

        })
        this.filterProductSearch(this.searchForm.get('search').value);
    }

    filterProductSearch(inputText: string): void {
        this.searchForm.get('search').setValue(inputText);
        this.productListFilteredSearch = this.productList.filter(item => {
            return (item['description'].concat(item['presentation'])).toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        });

        if (this.productListCurrentOrder != '') {
            let selected = this.productListCurrentOrder;
            this.productListCurrentOrder = ''
            if (selected != 'bar_code' && selected != 'description') {
                if (this.productListCurrentOrderDesc) this.orderByNumber(selected);
                this.orderByNumber(selected);
            } else {
                if (this.productListCurrentOrderDesc) this.orderByString(selected);
                this.orderByString(selected);
            }
        }
    }

    orderByString(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName && this.productListCurrentOrderDesc == false) {
            this.productListFilteredSearch.reverse();
            this.productListCurrentOrderDesc = true;
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoder(this.productList, fieldName);
            this.productListCurrentOrderDesc = false;
        }
        this.productListCurrentOrder = fieldName;
    }
    orderByNumber(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName && this.productListCurrentOrderDesc == false) {
            this.productListFilteredSearch.reverse();
            this.productListCurrentOrderDesc = true;
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoderNumber(this.productList, fieldName);
            this.productListCurrentOrderDesc = false;
        }
        this.productListCurrentOrder = fieldName;
    }



}
