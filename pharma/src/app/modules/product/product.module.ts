import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {NgxMaskModule} from "ngx-mask";
import {ProductComponentListComponent} from "./components/component-list/product-component-list.component";
import {ProductComponentListSimpleComponent} from "./components/component-list-simple/product-component-list-simple.component";
import {ProductComponentListSimpleTwoComponent } from './components/component-list-simple-two/product-component-list-simple-two.component';
import {ProductModalFormComponent} from "./modal-form/product-modal-form.component";
import {ProductModalPromotionComponent} from './modal-promotion/product-modal-promotion.component';
import {ProductPageListComponent} from './page-list/product-page-list.component';
import {ProductPageListGuideComponent} from "./page-list-guide/product-page-list-guide.component";
import {ProductPageListSimpleComponent} from "./page-list-simple/product-page-list-simple.component";
import {ProductPageListSimpleStockComponent} from "./page-list-simple-stock/product-page-list-simple-stock.component";
import {ProductPageListTestComponent} from "./page-list-test/product-page-list-test.component";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
    declarations: [
        ProductComponentListComponent,
        ProductComponentListSimpleComponent,
        ProductComponentListSimpleTwoComponent,
        ProductModalPromotionComponent,
        ProductModalFormComponent,
        ProductPageListComponent,
        ProductPageListTestComponent,
        ProductPageListGuideComponent,
        ProductPageListSimpleComponent,
        ProductPageListSimpleStockComponent,
    ],
    exports: [
        ProductModalPromotionComponent
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        SharedModule,
        NgxMaskModule
    ]
})
export class ProductModule { }
