import { Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

import {DatabaseService} from "../../../core/services/database/database.service";
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../core/services/storage/storage.service";

@Component({
    selector: 'fs-login-page-password-recover',
    templateUrl: './login-page-password-recover.component.html',
    styleUrls: ['./login-page-password-recover.component.scss']
})
export class LoginPagePasswordRecoverComponent{

    public hash: string;
    public passwordType: boolean = true;
    public passwordRecoverForm: FormGroup;
    public stateFields: object = {password:{class:''},passwordConfirm:{class:''}};

    constructor(
        public activatedRoute : ActivatedRoute,
        public formBuilder: FormBuilder,
        public router: Router,
        public databaseService: DatabaseService,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
    ) {
        this.activatedRoute.params.subscribe(params => {
            this.hash = params['hash'];
        });
        this.passwordRecoverForm = this.formBuilder.group({
            'password': new FormControl('', [Validators.required]),
            'passwordConfirm': new FormControl('', [Validators.required]),
        });
    }

    validForm(): boolean {
        if (!this.checkErrorField('password') || !this.checkErrorField('passwordConfirm')) {
            this.functionsService.openSnackBar('emptyField');
            return false;
        }
        if (this.passwordRecoverForm.controls['password'].value != this.passwordRecoverForm.controls['passwordConfirm'].value) {
            this.functionsService.openSnackBar('As senhas não correspondem','error');
            return false;
        }
        return true;
    }

    recoverPassword(): void {
        if (!this.validForm()) {return}


    }

    changeType(): void {
        this.passwordType = !this.passwordType;
    }

    checkErrorField(fieldName): boolean {
        if (this.passwordRecoverForm.controls[fieldName].valid) {
            this.stateFields[fieldName].class = '';
            return true;
        } else {
            this.stateFields[fieldName].class = 'emptyError';
            return false;
        }
    }
}
