import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";


import {ReactiveFormsModule} from "@angular/forms";
import {LoginFormComponent} from "./form/login-form.component";
import {LoginPagePasswordResetComponent} from "./page-password-reset/login-page-password-reset.component";
import {LoginPagePasswordRecoverComponent} from "./page-password-recover/login-page-password-recover.component";
import {SharedModule} from "../../shared/shared.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";



@NgModule({
    declarations: [
        LoginFormComponent,
        LoginPagePasswordRecoverComponent,
        LoginPagePasswordResetComponent,
    ],
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        SharedModule,
        FontAwesomeModule,
        MatFormFieldModule,
        MatInputModule,
    ]
})
export class LoginModule { }
