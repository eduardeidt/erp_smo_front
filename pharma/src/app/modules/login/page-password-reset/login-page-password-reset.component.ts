import { Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {DatabaseService} from "../../../core/services/database/database.service";
import {StorageService} from "../../../core/services/storage/storage.service";
import {Router} from "@angular/router";
import {FunctionsService} from "../../../core/services/functions/functions.service";

@Component({
    selector: 'fs-login-page-password-reset',
    templateUrl: './login-page-password-reset.component.html',
    styleUrls: ['./login-page-password-reset.component.scss']
})
export class LoginPagePasswordResetComponent{

    public emailError: string = '';
    public passwordResetForm: FormGroup;
    public stateFields: object = {email:{class:''}};

    constructor(
        public formBuilder: FormBuilder,
        public databaseService: DatabaseService,
        public storageService: StorageService,
        public functionsService: FunctionsService,
        public router: Router,
    ) {
        this.passwordResetForm = this.formBuilder.group({
            'email': new FormControl('', [Validators.required]),
        });
    }

    passwordReset(){}
}
