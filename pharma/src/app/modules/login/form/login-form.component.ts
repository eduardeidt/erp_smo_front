import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from '@angular/router';
import {Title} from "@angular/platform-browser";

import {AuthService} from "../../../core/services/auth/auth.service";
import {DatabaseService} from "../../../core/services/database/database.service";
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../core/services/storage/storage.service";
import {storageKeys} from "../../../core/variables/storageKeys";
import {ValidService} from "../../../core/services/valid/valid.service";

@Component({
    selector: 'fs-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

    public passwordType: boolean = true;
    public loginForm: FormGroup;
    public stateFields: object = {email: {class: ''}, password: {class: ''}};

    public modalAlertBrowserCompatibility: boolean;
    public isChrome: boolean;
    public isFirefox: boolean;

    public user: FormControl = new FormControl('', [Validators.required, Validators.min(4),]);
    public password: FormControl = new FormControl('', [Validators.required]);


    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public titleService: Title,
        public authService: AuthService,
        public databaseService: DatabaseService,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
        public validService: ValidService,
    ) {
        this.titleService.setTitle('Farmácia SMO - Login');
        this.loginForm = this.formBuilder.group({
            'email': new FormControl('', [Validators.required]),
            'password': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
        });
    }
    ngOnInit(): void {
        this.modalAlertBrowserCompatibility = this.checkBrowser();
    }

    login(): void {
        localStorage.clear();
        sessionStorage.clear();
        let user = this.user.value
        let password = this.password.value;
        this.databaseService.postLogin({user, password}, (res) => {
            if (!res.error) {
                this.authService.logIn(res.data,user);
            } else {
                this.loginForm.controls['password'].setValue('');
            }
        });
    }

    changeType(): void {
        this.passwordType = !this.passwordType;
    }

    showModalAlertBrowserCompatibility(): void {
        this.modalAlertBrowserCompatibility = true;
    }

    closeModalAlertBrowserCompatibility(): void {
        this.modalAlertBrowserCompatibility = false;
    }

    checkBrowser(): boolean {
        this.isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        // this.isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
        this.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

        return !(this.isChrome || this.isFirefox);
    }
}
