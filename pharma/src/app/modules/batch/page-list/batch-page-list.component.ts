import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconRegularService} from '../../../core/services/icons/icon-regular.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {StorageService} from '../../../core/services/storage/storage.service';
import {BatchDatabase} from '../batch.database';

import {storageKeys} from '../../../core/variables/storageKeys';
import {ProductDatabase} from '../../product/product.database';

@Component({
    selector: 'fs-product-page-list',
    templateUrl: './batch-page-list.component.html',
    styleUrls: ['./batch-page-list.component.scss']
})
export class BatchPageListComponent {

    @Output() reloadListEmitter: EventEmitter<number> = new EventEmitter<number>();

    public intervalFirstDate: string = null;
    public branchId: number = 0;
    public intervalSecondDate: string = null;
    public modalInactiveProduct: boolean;
    public modalProductPromotion: boolean = false;
    public modalConfirmExpired: boolean = false;
    public modalExpiredDate: boolean = false;
    public modalExpiredNewDate: boolean = false;
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public productFilterSim: boolean = false;
    public productFilterSimCompact: boolean = false;
    public productList: object[] = [{loading: true}];
    public expireDateList: object[];
    public productBatchId: number = null;
    public productFilterReset: EventEmitter<void> = new EventEmitter<void>();
    public productOptionGroup: object[] = [];
    public productOptionGroupSelected: string[] = [];
    public productOptionLab: object[] = [];
    public productOptionLabSelected: string[] = [];
    public productOptionPromotion: object[] = [];
    public productOptionPromotionSelected: string[] = [];
    public productOptionType: object[] = [];
    public productOptionTypeSelected: string[] = [];
    public groupList: string[] = [];
    public labList: string[] = [];
    public typeList: string[] = [];
    public productForm: FormGroup;
    public productListCurrentOrder: string = '';
    public productListFilteredSearch: object[] = [];
    public productSelectedCode: string = null;
    public productSelectedPromotionId: string = null;
    public productSelectedSellValue: string = null;
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;
    public shortExpirationDate: object;
    public mediumExpirationDate: number;
    public longExpirationDate: number;

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public titleService: Title,
        public batchDatabase: BatchDatabase,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public productDatabase: ProductDatabase,
        public storageService: StorageService,
    ) {
        this.titleService.setTitle('Farmácia SMO - Lotes/Validade');

        this.productForm = this.formBuilder.group({
            'description': new FormControl('', [Validators.required]),
            'search': new FormControl('', [Validators.required]),
        });

        this.loadProductList();
    }

    setArraySelected(array: object[], itemIdField: string = 'id'): string[] {
        const arrayReturn: string[] = array.map(item => item[itemIdField]);
    
        this.groupList = arrayReturn;
    
        this.loadProductList(this.paginationCurrentPage);
    
        return this.groupList;
    }

    setArraySelected1(array: object[], itemIdField: string = 'id'): string[] {
        const arrayReturn: string[] = array.map(item => item[itemIdField]);
    
        this.labList = arrayReturn;
    
        this.loadProductList(this.paginationCurrentPage);
    
        return this.labList;
    }

    setArraySelected2(array: object[], itemIdField: string = 'id'): string[] {
        const arrayReturn: string[] = array.map(item => item[itemIdField]);
    
        this.typeList = arrayReturn;
    
        this.loadProductList(this.paginationCurrentPage);
    
        return this.typeList;
    }

    setBranch(branch: number): void {
        this.branchId = branch;
        this.loadProductList(this.paginationCurrentPage);
    }

    setFirstDate(date: string): void {
        this.intervalFirstDate = date;
        this.loadProductList(this.paginationCurrentPage);
    }

    setSecondDate(date: string): void {
        this.intervalSecondDate = date;
        this.loadProductList(this.paginationCurrentPage);
    }
    loadProduct(page: number = null): void {
        this.reloadListEmitter.emit(page);
    }
    loadProductList(pageNumber: number = null): void {
        if (this.intervalFirstDate == null || this.intervalSecondDate == null) {
            return;
        }
        let params: object = {
            description: this.productForm.get('description').value,
            branch_id: this.branchId,
            page: pageNumber,
            groupList: this.groupList.join(','),
            labList: this.labList.join(','),
            typeList: this.typeList.join(','),
            beginDate: this.intervalFirstDate,
            endDate: this.intervalSecondDate,
        };
        this.productList = [{loading: true}];
        this.productDatabase.getProductFilterData((res) => {
            if (!res.error) {
                this.productOptionGroup = res.data.dataGroup;
                this.productOptionLab = res.data.dataLab;
                this.productOptionPromotion = res.data.promotions;
                this.productOptionType = res.data.dataType;
            }
        });
        this.batchDatabase.getBatchList(params, (res) => {
            if (!res.error) {
                this.productList = res.data.productList.data;
                this.filterProductSearch(this.productForm.get('search').value);
                this.paginationCurrentPage = res.data.productList.current_page || 1;
                this.paginationLastPage = res.data.productList.last_page || null;
                this.expireDateList = res.data.expirationDate[0];
                this.shortExpirationDate = this.expireDateList['short_expiration_date'];
                this.mediumExpirationDate = this.expireDateList['medium_expiration_date'];
                this.longExpirationDate = this.expireDateList['long_expiration_date'];
            }
        });
    }

    orderByString(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName) {
            this.productListFilteredSearch.reverse();
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoder(this.productList, fieldName);
        }
        this.productListCurrentOrder = fieldName;
        this.filterProductSearch(this.productForm.get('search').value);
    }

    orderByNumber(fieldName: string): void {
        if (this.productListCurrentOrder == fieldName) {
            this.productListFilteredSearch.reverse();
        } else {
            this.productListFilteredSearch = this.functionsService.arrayReoderNumber(this.productList, fieldName);
        }
        this.productListCurrentOrder = fieldName;
        this.filterProductSearch(this.productForm.get('search').value);
    }

    filterProductSearch(inputText: string): void {
        this.productForm.controls['search'].setValue(inputText);
        this.productListFilteredSearch = this.productList.filter(item => {
            return (item['description']).toLowerCase().includes(inputText.toLowerCase()) || item['product_code'].toLowerCase().includes(inputText.toLowerCase());
        });
    }

        showModalInactiveProduct(id: number): void {
        this.productBatchId = id;
        this.modalInactiveProduct = true;
    }
    hideModalInactiveProduct(): void {
        this.modalInactiveProduct = false;
        this.productBatchId = null;
    }

    showModalProductPromotion(code: string, sellValue: string = null): void {
        this.productSelectedCode = code;
        this.productSelectedSellValue = sellValue;
        this.modalProductPromotion = true;
    }

    hideModalProductPromotion(): void {
        this.productBatchId = null;
        this.productSelectedSellValue = null;
        this.modalProductPromotion = false;
    }

    showModalConfirmExpired(ProductBatchId: number): void {
        this.productBatchId = ProductBatchId;
        this.modalConfirmExpired = true;
    }
    confirmExpiredProduct(){
        let product_batch_id: number = this.productBatchId;
        this.batchDatabase.inactiveProductBatch(this.productBatchId, (res) => {
            if (!res.error) {
                this.functionsService.openSnackBar('Produto marcado como vencido com sucesso.');
                this.productList = this.productList.filter(prod => prod['id'] != product_batch_id);
                this.filterProductSearch(this.productForm.controls['search'].value);
                this.hideModalConfirmExpired();
            }
        });
        this.hideModalConfirmExpired();
    }
    hideModalConfirmExpired(): void {
        this.productBatchId = null;
        this.modalConfirmExpired = false;
    }

    showModalEditExpirationData(code: string): void {
        this.productSelectedCode = code;
        this.modalExpiredDate = true;
    }

    hideModalEditExpirationData(): void {
        this.productSelectedCode = null;
        this.modalExpiredDate = false;
    }

    showModalEditExpirationNewData(): void {
        // this.productSelectedCode = code;
        this.modalExpiredNewDate = true;
    }

    hideModalEditExpirationNewData(): void {
        this.productSelectedCode = null;
        this.modalExpiredNewDate = false;
    }

    getDate(date: string): string {
        return this.functionsService.dateENtoBR(date.split(' ')[0]);
    }

}
