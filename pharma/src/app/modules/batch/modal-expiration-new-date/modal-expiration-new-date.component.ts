import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {StorageService} from '../../../core/services/storage/storage.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';

import {storageKeys} from '../../../core/variables/storageKeys';
import {Router} from '@angular/router';
import {IconRegularService} from '../../../core/services/icons/icon-regular.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {BatchDatabase} from '../batch.database';

@Component({
    selector: 'fs-modal-expiration-new-date',
    templateUrl: './modal-expiration-new-date.component.html',
    styleUrls: ['./modal-expiration-new-date.component.scss']
})
export class ModalExpirationNewDateComponent implements OnChanges {

    @Input() showModal: boolean = true;
    @Input() productCode: string;

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() productPosted = new EventEmitter<any>();
    @Output() productUpdated = new EventEmitter<any>();

    public productList: object[] = [{loading: true}];
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;
    public dateSelected: string = this.functionsService.dateBRtoEN(this.functionsService.currentDateBR('00/00/0000', 'min'));
    public productForm: FormGroup;
    public selectedCompany: FormGroup;
    // public selectedData: FormGroup;
    public productData: object[] = [];
    // public stateFields: object = {newExpirationDate:{class:''}};

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public batchDatabase: BatchDatabase,
        public storageService: StorageService,
    ) {
        this.selectedCompany = this.formBuilder.group({
            'branchId': new FormControl(null, [Validators.required]),
        });
        this.productForm = this.formBuilder.group({
            'batch': new FormControl(null, [Validators.required]),
            'bar_code': new FormControl(null, [Validators.required]),
            'amount': new FormControl(null, [Validators.required]),
            'newExpirationDate': new FormControl(null, [Validators.required]),
        });
    }
    ngOnChanges(): void {}
    setNewExpirationDate(){
        if (this.selectedCompany.get('branchId').value == null) {
            this.functionsService.openSnackBar('Selecione uma filial');
            return;
        }
        if (this.productForm.get('batch').value == null) {
            this.functionsService.openSnackBar('informe o lote do produto');
            return;
        }
        if (this.productForm.get('bar_code').value == null) {
            this.functionsService.openSnackBar('informe o código de barras do produto');
            return;
        }
        if (this.productForm.get('amount').value == null) {
            this.functionsService.openSnackBar('informe a quantidade de produtos');
            return;
        }
        if (this.productForm.get('newExpirationDate').value == null) {
            this.functionsService.openSnackBar('informe uma data de vencimento');
            return;
        }
        let params: object = {
            branchId: this.selectedCompany.get('branchId').value,
            batch: this.productForm.get('batch').value,
            bar_code: this.productForm.get('bar_code').value,
            amount: this.productForm.get('amount').value,
            newExpirationDate: this.functionsService.dateBRtoEN(this.productForm.get('newExpirationDate').value),
        };
        this.batchDatabase.postBatch(params, (res) =>{
            console.log(res);
            if (!res.error) {
                this.functionsService.openSnackBar(res.msg);
                this.closeModal();
                this.resetFilter();
            }else{
                this.functionsService.openSnackBar(res.error.error.error.msg);
            }
        });
    }
    resetFilter(): void {
        this.productForm.reset();
        this.selectedCompany.reset();
    }
    closeModal(): void {
        this.hideModal.emit(false);
        this.resetFilter();
    }
}
