import {Injectable} from '@angular/core';

import {DatabaseService} from '../../core/services/database/database.service';

@Injectable({
    providedIn: 'root'
})
export class BatchDatabase {

    constructor(
        private databaseService: DatabaseService,
    ){}

    getBatchList(params, callback) {
        this.databaseService.get('/productBatch', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    inactiveProductBatch(productBatchId, callback) {
        this.databaseService.put('/productBatch/inactiveProductBatch/' + productBatchId, [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postBatch(params, callback) {
        this.databaseService.post('/productBatch', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    updateExpiredDate(params, callback) {
        this.databaseService.put('/productBatch/updateExpirationDate', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
}
