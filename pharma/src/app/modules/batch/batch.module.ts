import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";

import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {NgxMaskModule} from "ngx-mask";
import {BatchPageListComponent} from './page-list/batch-page-list.component';
import {SharedModule} from "../../shared/shared.module";
import {ProductModule} from "../product/product.module";
import { ModalExpirationDateComponent } from './modal-expiration-date/modal-expiration-date.component';
import { ModalExpirationNewDateComponent} from './modal-expiration-new-date/modal-expiration-new-date.component';
import {MatIconModule} from "@angular/material/icon";


@NgModule({
    declarations: [
        BatchPageListComponent,
        ModalExpirationDateComponent,
        ModalExpirationNewDateComponent,
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        SharedModule,
        NgxMaskModule,
        ProductModule,
        MatIconModule
    ]
})
export class BatchModule { }
