import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {StorageService} from '../../../core/services/storage/storage.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';

import {storageKeys} from '../../../core/variables/storageKeys';
import {Router} from '@angular/router';
import {IconRegularService} from '../../../core/services/icons/icon-regular.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {ProductDatabase} from '../../product/product.database';
import {BatchDatabase} from '../batch.database';


@Component({
    selector: 'fs-modal-expiration-date',
    templateUrl: './modal-expiration-date.component.html',
    styleUrls: ['./modal-expiration-date.component.scss']
})
export class ModalExpirationDateComponent implements OnChanges {

    @Input() showModal: boolean = true;
    @Input() productCode: string;

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() productPosted = new EventEmitter<any>();
    @Output() productUpdated = new EventEmitter<any>();

    public productList: object[] = [{loading: true}];
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;
    public dateSelected: string = this.functionsService.dateBRtoEN(this.functionsService.currentDateBR('00/00/0000', 'min'));
    public productForm: FormGroup;
    public selectedCompany: FormGroup;
    public productData: object[] = [];

    constructor(
        public formBuilder: FormBuilder,
        public router: Router,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public productDatabase: ProductDatabase,
        public batchDatabase: BatchDatabase,
        public storageService: StorageService,
    ) {
        this.selectedCompany = this.formBuilder.group({
            'branchId': new FormControl(null, [Validators.required]),
        });
        this.productForm = this.formBuilder.group({
            'product_code': new FormControl('', [Validators.required]),
            'description': new FormControl('', [Validators.required]),
            'newExpirationDate': new FormControl('', [Validators.required]),
        });
    }

    ngOnChanges(): void {
        if (this.productCode && this.showModal) {
            this.productDatabase.getProduct(this.productCode, (res) => {
                if (!res.error) {
                    this.productData = res.data['product'];
                    Object.keys(this.productForm.controls).forEach(field => {
                        this.productForm.controls[field].setValue(this.productData['product_extras'][0][field]);
                    });
                    this.productForm.controls['product_code'].setValue(this.productData['code']);
                    this.productForm.controls['description'].setValue(this.productData['description']);
                } else {
                    this.closeModal();
                }
            });
        }
    }

    setNewExpirationDate(){
        if (this.selectedCompany.get('branchId').value == null) {
            this.functionsService.openSnackBar('Selecione uma unidade');
            return;
        }
        let params: object = {
            product_code: this.productForm.get('product_code').value,
            branchId: this.selectedCompany.get('branchId').value,
            newExpirationDate: this.functionsService.dateBRtoEN(this.productForm.get('newExpirationDate').value),
        };
        this.batchDatabase.updateExpiredDate(params, (res) =>{
            if (!res.error) {
                this.functionsService.openSnackBar('Data de validade alterada com sucesso.');
                this.closeModal();
            }
        });
    }

    // setDateSelected(date: string) {
    //     this.dateSelected = date;
    //     console.log(this.dateSelected);
    // }

    closeModal(): void {
        this.hideModal.emit(false);
    }

}
