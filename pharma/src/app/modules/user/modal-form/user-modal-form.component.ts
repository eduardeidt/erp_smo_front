import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';


import {DatabaseService} from '../../../core/services/database/database.service';
import {StorageService} from '../../../core/services/storage/storage.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';

@Component({
    selector: 'fs-user-modal-form',
    templateUrl: './user-modal-form.component.html',
    styleUrls: ['./user-modal-form.component.scss']
})
export class UserModalFormComponent implements OnChanges {

    @Input() showModal: boolean = false;
    @Input() userId: string = null;

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() userData = new EventEmitter<any>();
    @Output() userPosted = new EventEmitter<any>();
    @Output() userUpdated = new EventEmitter<any>();

    public updateOn: boolean = false;
    public stateFields: object = {
        name: {class: ''},
        email: {class: ''},
        user: {class: ''},
        password: {class: ''},
        admin: {class: ''},
        active: {class: ''},
        branch_ids: {class: ''}
    };
    public branchList: object[] = [{loading: true}];
    public formSubmitAttempt: boolean = false;

    public userForm: FormGroup;

    constructor(
        public formBuilder: FormBuilder,
        public databaseService: DatabaseService,
        public storageService: StorageService,
        public functionsService: FunctionsService,
        public iconSolidService: IconSolidService,
    ) {
        this.userForm = this.formBuilder.group({
            'name': new FormControl(null, [Validators.required]),
            'email': new FormControl(null, [Validators.required, Validators.email]),
            'user': new FormControl(null, [Validators.required]),
            'password': new FormControl(null, [Validators.minLength(6)]),
            'branch_ids': new FormControl(null, [Validators.required]),
            'admin': new FormControl(false),
            'active': new FormControl(true),
        });
    }

    ngOnChanges(): void {
        if (this.showModal) {
            if (this.userId == null) {
                this.userForm.get('password').setValidators([Validators.required, Validators.minLength(6)]);
            } else {
                this.userForm.get('password').clearValidators();
            }
            this.userForm.get('password').updateValueAndValidity();


            this.databaseService.getBranchs((res) => {
                if (!res.error) {
                    res = res.data;
                    let firstElement = [{'id': '', 'city': ''}];
                    this.branchList = firstElement.concat(res['branchs']);
                }
            });
        }
        if (this.userId != null && this.showModal) {
            this.updateOn = true;
            this.databaseService.getUser(this.userId, (res) => {
                if (!res.error) {
                    let userData: object = res.data['user'];
                    this.userForm.controls['name'].setValue(userData['name']);
                    this.userForm.controls['email'].setValue(userData['email']);
                    this.userForm.controls['user'].setValue(userData['user']);
                    this.userForm.controls['admin'].setValue(userData['admin']);
                    this.userForm.controls['active'].setValue(userData['active']);
                    this.userForm.controls['branch_ids'].setValue(userData['branch_ids']);
                } else {
                    this.closeModal();
                }
            });
        }
    }

    validateAllFormFields(formGroup: FormGroup): void {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    sendUser(): void {
        if (this.userForm.valid) {
            if (this.updateOn) {
                this.putUser({...this.userForm.value, code: this.userId});
            } else {
                this.postUser({...this.userForm.value});
            }
        } else {
            this.validateAllFormFields(this.userForm);
            this.formSubmitAttempt = true;
        }
    }

    putUser(userData: object): void {
        this.databaseService.putUser(this.userId, userData, (res) => {
            if (!res.error) {
                // this.userUpdated.emit({...userData});
                this.functionsService.openSnackBar('Produto atualizado com sucesso.');
                this.closeModal();
            }
        });
    }

    postUser(userData: object): void {
        this.databaseService.postUser(userData, (res) => {
            if (!res.error) {
                // this.userUpdated.emit({...userData});
                this.functionsService.openSnackBar('Usuário cadastrado com sucesso.');
                this.closeModal();
            }
        });
    }

    closeModal(): void {
        this.updateOn = false;
        this.userForm.reset();
        this.formSubmitAttempt = false;
        this.hideModal.emit(false);
    }
}
