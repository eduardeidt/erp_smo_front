import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DatabaseService} from '../../../core/services/database/database.service';
import {Title} from '@angular/platform-browser';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconRegularService} from '../../../core/services/icons/icon-regular.service';

@Component({
    selector: 'fs-user-page-list',
    templateUrl: './user-page-list.component.html',
    styleUrls: ['./user-page-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UserPageListComponent implements OnInit {

    public userList: object[] = [{loading: true}];
    public userListSearch: object = [];
    public modalUser: boolean = false;
    public modalRemoveUser: boolean = false;
    public userSelectedId: string = null;

    constructor(
        public databaseService: DatabaseService,
        public titleService: Title,
        public iconSolidService: IconSolidService,
        public iconRegularService: IconRegularService,
        public functionService: FunctionsService,
    ) {
        this.titleService.setTitle('Farmácia SMO - Usuários');
    }

    ngOnInit(): void {
        this.loadUserList();
    }

    showModalRemoveUser(id: string): void {
        this.userSelectedId = id;
        this.modalRemoveUser = true;
    }

    hideModalRemoveUser(): void {
        this.userSelectedId = null;
        this.modalRemoveUser = false;
    }

    filterUserSearch(inputText: string): void {
        this.userListSearch = this.userList.filter(item => {
            return item['name'].toLowerCase().includes(inputText.toLowerCase()) || item['user'].toLowerCase().includes(inputText.toLowerCase()) || item['email'].toLowerCase().includes(inputText.toLowerCase());
        });
    }

    deleteUser(id: string): void {
        this.hideModalRemoveUser();
        this.databaseService.deleteUser(id, (res) => {
            if (!res.error) {
                this.functionService.openSnackBar('Usuário excluída com sucesso.');
                this.loadUserList();
            } else {
            }
        });
    }

    activeUser(id: string): void {
        let params = {active: 1};
        this.databaseService.putUser(id, params, (res) => {
            if (!res.error) {
                this.functionService.openSnackBar('Usuário ativado com sucesso.');
                this.loadUserList();
            }
        });
    }


    loadUserList(): void {
        this.databaseService.getUsers((res) => {
            if (!res.error) {
                res = res.data;
                this.userList = res['users'];
                this.userListSearch = this.userList;
            }
        });
    }

    showModalUser(id: string = null): void {
        this.userSelectedId = id;
        this.modalUser = true;
    }

    hideModalUser(): void {
        this.loadUserList();
        this.userSelectedId = null;
        this.modalUser = false;
    }
}
