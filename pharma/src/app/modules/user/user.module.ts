import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserPageListComponent} from './page-list/user-page-list.component';
import {MatTabsModule} from '@angular/material/tabs';
import {SharedModule} from '../../shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {RouterModule} from '@angular/router';
import {UserModalFormComponent} from './modal-form/user-modal-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatOptionModule} from '@angular/material/core';

@NgModule({
    declarations: [UserPageListComponent, UserModalFormComponent],
    imports: [
        CommonModule,
        MatTabsModule,
        SharedModule,
        FontAwesomeModule,
        RouterModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCheckboxModule,
        FormsModule,
        MatOptionModule
    ]
})

export class UserModule {
}
