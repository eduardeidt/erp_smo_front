import {Injectable} from '@angular/core';

import {DatabaseService} from '../../core/services/database/database.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationDatabase {

    constructor(
        private databaseService: DatabaseService,
    ){}


    deleteNotification(params, callback) {
        this.databaseService.delete('/notification', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

    getNotificationList(params, callback) {
        this.databaseService.get('/notification/index', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getProductPriceLabel(param,callback) {
        this.databaseService.getFile('/reports/productPriceLabel', param)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

    putNotification(params, callback): void {
        this.databaseService.put('/notification' , params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
}
