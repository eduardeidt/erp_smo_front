import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotificationPageHomeComponent} from './page-home/notification-page-home.component';
import {MatTabsModule} from '@angular/material/tabs';
import {SharedModule} from '../../shared/shared.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
    declarations: [NotificationPageHomeComponent],
    imports: [
        CommonModule,
        MatTabsModule,
        SharedModule,
        FontAwesomeModule,
        RouterModule,
        ReactiveFormsModule
    ]
})

export class NotificationModule {
}
