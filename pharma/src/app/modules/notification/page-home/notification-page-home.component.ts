import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DatabaseService} from '../../../core/services/database/database.service';
import {Title} from '@angular/platform-browser';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';
import {FormControl, Validators} from "@angular/forms";
import {NotificationDatabase} from "../notification.database";
import {StorageService} from '../../../core/services/storage/storage.service';
import {storageKeys} from '../../../core/variables/storageKeys';

@Component({
    selector: 'fs-notification-page-home',
    templateUrl: './notification-page-home.component.html',
    styleUrls: ['./notification-page-home.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NotificationPageHomeComponent implements OnInit {

    // $scope.tabId = 1; //default template loaded
    //
    // $scope.onTabSelected = function(tabId) {
    //     //you can add some loading before rendering
    //     $scope.tabId = tabId;
    // };

    public branchList: object[] = [];
    public branchListSelectedId: number[] = [1];
    public modalCheckNotification: boolean = false;
    public modalCheckMultiNotification: boolean = false;
    public modalRemoveNotification: boolean = false;
    public modalRemoveMultiNotification: boolean = false;
    public modalProductPromotion: boolean = true;
    public notificationList: object[] = [{loading:true}];
    public notificationListFiltered: object[] = [];
    public notificationListSelectedCode: string[] = [];
    public notificationListSelectedId: string[] = [];
    public notificationSelectedId: string = '';
    public paginationCurrentPage: number = 1;
    public paginationLastPage: number = 1;
    public type: string = '1';


    public allPriceCheck: FormControl = new FormControl('', [Validators.required]);

    constructor(
        public notificationDatabase: NotificationDatabase,
        public titleService: Title,
        public iconSolidService: IconSolidService,
        public functionService: FunctionsService,
        public storageService: StorageService,
    ) {
        this.titleService.setTitle('Farmácia SMO - Notificações');
        let branchString = storageService.decodeFromLocalStorage(storageKeys.branchIds)
        branchString.split(',').forEach(id => {
            this.branchList.push({
                id: id, name: 'Filial '+id
            })
        })
    }

    ngOnInit(): void {
        this.loadNotificationList();
    }

    onTabChange(event): void {
        this.type = event.tab.textLabel;
        this.loadNotificationList();
    }

    setBranchListId(listId: object[]): void {
        this.branchListSelectedId = [];
        listId.forEach(branch => {
            this.branchListSelectedId.push(branch['id'])
        });
        this.loadNotificationList();
    }

    loadNotificationList(page: number = null): void {
        let params = {type: this.type, branch_ids: JSON.stringify(this.branchListSelectedId), page: page};
        this.notificationDatabase.getNotificationList(params, (res) => {
            if (!res.error) {
                this.notificationList = res.data.notifications.data;
                this.notificationListFiltered = this.notificationList;
                this.paginationCurrentPage = res.data.notifications.current_page || 1;
                this.paginationLastPage = res.data.notifications.last_page || null;
                if (this.type == '2') {
                }
                this.filterNotificationSearch('');
            }
        });
    }
    filterNotificationSearch(inputText: string): void {
        this.notificationListFiltered = this.notificationList.filter(item => {
            return item['description'].toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        });
    }

    showModalCheckNotification(id: string = null): void {
        if (id == null && this.notificationListSelectedId.length == 0) {
            this.functionService.openSnackBar('Selecione uma notificação para precificar');
            return;
        }
        this.notificationSelectedId = id;
        this.modalCheckNotification = true;
    }
    checkNotification(id: string = null): void {
        let notificationListId: string = id != null ? JSON.stringify([id]) : JSON.stringify(this.notificationListSelectedId)
        this.notificationDatabase.putNotification({notificationListId,status : 1}, (res) => {
            if (!res.error) {
                this.functionService.openSnackBar(id ? 'Notificação marcada com sucesso.' : 'Notificações marcadas com sucesso.');
                this.hideModalCheckNotification();
                this.loadNotificationList();
            } else {
            }
        });
    }
    hideModalCheckNotification(): void {
        this.notificationSelectedId = null;
        this.modalCheckNotification = false;
    }

    showModalRemoveNotification(id: string = null): void {
        if (id == null && this.notificationListSelectedId.length == 0) {
            this.functionService.openSnackBar('Selecione uma notificação para remover');
            return;
        }
        this.notificationSelectedId = id;
        this.modalRemoveNotification = true;
    }
    deleteNotification(id: string): void {
        let notificationListId: string = id != null ? JSON.stringify([id]) : JSON.stringify(this.notificationListSelectedId)
        this.notificationDatabase.deleteNotification({notificationListId}, (res) => {
            if (!res.error) {
                this.functionService.openSnackBar(id ? 'Notificação excluída com sucesso.' : 'Notificações excluídas com sucesso.');
                this.hideModalRemoveNotification();
                this.loadNotificationList();
                this.resetAllPrice();
            }
        });
    }
    hideModalRemoveNotification(): void {
        this.notificationSelectedId = null;
        this.modalRemoveNotification = false;
    }


    resetAllPrice(): void{
        this.allPriceCheck.setValue(false);
        this.checkAllPrice('all')
    }

    checkAllPrice(allOne: string = 'one',notification: object = null): void {
        if (allOne == 'all') {
            if (this.allPriceCheck.value){
                this.notificationListFiltered.forEach(item => {
                    this.notificationListSelectedCode.push(item['code']);
                    this.notificationListSelectedId.push(item['id']);
                });
            } else {
                this.notificationListSelectedCode = [];
                this.notificationListSelectedId = [];
            }
            return;
        }
        if (this.notificationListSelectedCode.indexOf(notification['code']) >= 0) {
            this.notificationListSelectedCode = this.notificationListSelectedCode.filter(item => item != notification['code']);
            this.notificationListSelectedId = this.notificationListSelectedId.filter(item => item != notification['id']);
        } else {
            this.notificationListSelectedCode.push(notification['code']);
            this.notificationListSelectedId.push(notification['id']);
        }
        this.allPriceCheck.setValue(this.notificationListSelectedCode.length == this.notificationListFiltered.length);
    }

    printTag(): void {
        if (this.notificationListSelectedCode.length == 0) {
            this.functionService.openSnackBar('selecione uma notificação')
            return;
        }
        this.notificationDatabase.getProductPriceLabel({productList: JSON.stringify(this.notificationListSelectedCode)},(res) => {
           if (!res.error) {
               this.functionService.downloadFile(res.data,'pdf','Labels');
               // this.functionService.downloadFile(res.data,type,fileName.split('.'+type)[0]);
           }
        });
    }



}
