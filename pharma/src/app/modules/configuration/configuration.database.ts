import {Injectable} from '@angular/core';

import {DatabaseService} from '../../core/services/database/database.service';

@Injectable({
    providedIn: 'root'
})
export class ConfigurationDatabase {

    constructor(
        private databaseService: DatabaseService,
    ){}


    getParameters(callback) {
        this.databaseService.get('/parameters', [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getPharmaGuidebook(callback) {
        this.databaseService.getFile('/parameters/guiaDaFarmacia', [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }


    postParameters(parametersData, callback) {
        this.databaseService.post('/parameters', parametersData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

}
