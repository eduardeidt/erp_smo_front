import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfigurationPageHomeComponent} from './page-home/configuration-page-home.component';
import {MatTabsModule} from '@angular/material/tabs';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
    declarations: [ConfigurationPageHomeComponent],
    imports: [
        CommonModule,
        MatTabsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        SharedModule
    ]
})

export class ConfigurationModule {
}
