import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {DatabaseService} from '../../../core/services/database/database.service';
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {ConfigurationDatabase} from "../configuration.database";

@Component({
    selector: 'fs-configuration-page-home',
    templateUrl: './configuration-page-home.component.html',
    styleUrls: ['./configuration-page-home.component.scss']
})
export class ConfigurationPageHomeComponent implements OnInit {

    public parametersConfigurationForm : FormGroup;

    constructor(
        public formBuilder: FormBuilder,
        public configurationDatabase: ConfigurationDatabase,
        private functionsService: FunctionsService

    ) {
        this.parametersConfigurationForm = this.formBuilder.group({
            'icms1': new FormControl(null, [Validators.required]),
            'icms2': new FormControl(null, [Validators.required]),
            'icms3': new FormControl(null, [Validators.required]),
            'icms4': new FormControl(null, [Validators.required]),
            'irrf': new FormControl(null, [Validators.required]),
            'irrfExtra': new FormControl(null, [Validators.required]),
            'socialContribution': new FormControl(null, [Validators.required]),
            'shortExpirationDate': new FormControl(null, [Validators.required]),
            'mediumExpirationDate': new FormControl(null, [Validators.required]),
            'longExpirationDate': new FormControl(null, [Validators.required]),
        });
        this.configurationDatabase.getParameters((res) => {
            if (!res.error) {
                this.parametersConfigurationForm.controls['icms1'].setValue(res.data['icms1']);
                this.parametersConfigurationForm.controls['icms2'].setValue(res.data['icms2']);
                this.parametersConfigurationForm.controls['icms3'].setValue(res.data['icms3']);
                this.parametersConfigurationForm.controls['icms4'].setValue(res.data['icms4']);
                this.parametersConfigurationForm.controls['irrf'].setValue(res.data['irrf']);
                this.parametersConfigurationForm.controls['irrfExtra'].setValue(res.data['irrf_extra']);
                this.parametersConfigurationForm.controls['socialContribution'].setValue(res.data['social_contribution']);
                this.parametersConfigurationForm.controls['shortExpirationDate'].setValue(res.data['short_expiration_date']);
                this.parametersConfigurationForm.controls['mediumExpirationDate'].setValue(res.data['medium_expiration_date']);
                this.parametersConfigurationForm.controls['longExpirationDate'].setValue(res.data['long_expiration_date']);
            }
        });
    }
    ngOnInit(): void {
    }

    downloadFile(): void{
        this.configurationDatabase.getPharmaGuidebook((res) => {
            if (!res.error) {
                this.functionsService.downloadFile(res.data,'txt','Guia');
            }
        })
    }

    saveParameters(): void {
        this.configurationDatabase.postParameters(this.parametersConfigurationForm.value,(res)=>{
            if (!res.error){
                res.data;
            }
        })
    }
}
