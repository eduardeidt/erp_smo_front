import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatTabsModule} from '@angular/material/tabs';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from "@angular/router";

import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {PromotionModalFormComponent} from './modal-form/promotion-modal-form.component';
import {PromotionModalProductPromotion} from "./modal-product-promotion/promotion-modal-product-promotion";
import {PromotionPageAnalyzeComponent} from "./page-analyze/promotion-page-analyze.component";
import {PromotionPageListComponent} from './page-list/promotion-page-list.component';
import {PromotionPageListProductComponent} from "./page-list-product/promotion-page-list-product.component";
import {NgxMaskModule} from 'ngx-mask';
import {SharedModule} from '../../shared/shared.module';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {ProductModule} from "../product/product.module";

@NgModule({
    declarations: [
        PromotionModalFormComponent,
        PromotionModalProductPromotion,
        PromotionPageAnalyzeComponent,
        PromotionPageListComponent,
        PromotionPageListProductComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        MatTabsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        SharedModule,
        FontAwesomeModule,
        NgxMaskModule,
        SharedModule,
        ProductModule,
    ]
})

export class PromotionModule {}
