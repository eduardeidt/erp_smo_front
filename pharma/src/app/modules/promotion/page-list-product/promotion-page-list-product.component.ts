import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconRegularService} from '../../../core/services/icons/icon-regular.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {PromotionDatabase} from '../promotion.database';
import {StorageService} from '../../../core/services/storage/storage.service';

@Component({
    selector: 'fs-promotion-page-list',
    templateUrl: './promotion-page-list-product.component.html',
    styleUrls: ['./promotion-page-list-product.component.scss']
})
export class PromotionPageListProductComponent implements OnInit {

    public modalProductPromotion = false;
    public modalDeleteNextPromotion = false;
    public productSelectedCode: string = null;
    public promotionId: string = null;
    public promotionListProduct: object[] = [{loading: true}];
    public promotionListProductFilteredSearch: object[] = [];

    constructor(
        public router: Router,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public promotionDatabase: PromotionDatabase,
        public storageService: StorageService,
    ) {
        const navigation = this.router.getCurrentNavigation();
        if (navigation.extras.state && navigation.extras.state.promotionIdSelected) {
            this.promotionId = navigation.extras.state.promotionIdSelected;
            this.loadProductList();
        } else {
            this.router.navigate(['/promotion']).then(() => false);
        }
    }

    ngOnInit(): void {}

    loadProductList(): void {
        this.promotionDatabase.getPromotionListProduct(this.promotionId, (res) => {
            if (!res.error) {
                this.promotionListProduct = res.data.productList;
                this.filterProductSearch('');
            }
        });
    }

    filterProductSearch(inputText: string): void {
        this.promotionListProductFilteredSearch = this.promotionListProduct.filter(item => {
            return (item['description'].concat(item['presentation'])).toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        });
    }
    showModalProductPromotion(code: string): void {
        this.productSelectedCode = code;
        this.modalProductPromotion = true;

    }
    hideModalProductPromotion(): void {
        this.productSelectedCode = null;
        this.modalProductPromotion = false;

    }
    hideModalDeleteNextPromotion(): void {
        this.modalDeleteNextPromotion = false;
        this.productSelectedCode = null;
    }

    showModalDeleteNextPromotion(productCode: string = null): void {
        this.productSelectedCode = productCode;
        this.modalDeleteNextPromotion = true;
    }
    deleteNextPromotion(): void {
        const productCode = this.productSelectedCode;
        this.promotionDatabase.deleteNextPromotion(productCode, (res) => {
            if (!res.error) {
                this.loadProductList();
                this.hideModalDeleteNextPromotion();
            }
        });
    }
}
