import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {DatabaseService} from '../../../core/services/database/database.service';
import {StorageService} from '../../../core/services/storage/storage.service';
import {FunctionsService} from '../../../core/services/functions/functions.service';
import {IconSolidService} from '../../../core/services/icons/icon-solid.service';
import {PromotionDatabase} from "../promotion.database";

@Component({
    selector: 'fs-promotion-modal-product-promotion',
    templateUrl: './promotion-modal-product-promotion.html',
    styleUrls: ['./promotion-modal-product-promotion.scss'],
})
export class PromotionModalProductPromotion implements OnChanges {

    @Input() showModal: boolean = true;
    @Input() productPrice: string;
    @Input() promotionListLoad: object[] = [];

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() promotionListSaved = new EventEmitter<object[]>();

    public dataloaded: boolean = false;
    public promotionForm: FormGroup;
    public promotionList: object[] = [];

    constructor(
        public formBuilder: FormBuilder,
        public databaseService: DatabaseService,
        public functionsService: FunctionsService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
        private promotionDatabase: PromotionDatabase
    ) {
        this.promotionForm = this.formBuilder.group({
            'amount': new FormControl('', [Validators.required]),
            'perc': new FormControl('', [Validators.required]),
            'price': new FormControl('', [Validators.required]),
        });
    }

    ngOnChanges(): void {
        if (this.promotionListLoad != null) {
            this.promotionListLoad.forEach(item => this.promotionList.push(item));
            this.dataloaded = true;
        } else {
            this.dataloaded = true;
        }
    }

    addPromotion(): void {
        if (this.promotionForm.controls['amount'].value == '') {
            this.functionsService.openSnackBar('Digite a quantidade.');
            return;
        }
        if (this.promotionForm.controls['perc'].value == '') {
            this.functionsService.openSnackBar('Digite a desconto.');
            return;
        }
        if (this.promotionForm.controls['price'].value == '') {
            this.functionsService.openSnackBar('Digite o valor.');
            return;
        }
        if (this.promotionList.findIndex(promotion => promotion['amount'] == this.promotionForm.controls['amount'].value) >= 0) {
            this.functionsService.openSnackBar('Uma promoção para essa quantidade já foi cadastrada.');
            return;
        }
        this.promotionList.push(this.promotionForm.value);
        this.promotionForm.reset('');
    }

    removePromotion(promoIndex): void {
        this.promotionList.splice(promoIndex, 1);
    }

    calcForm(inputName: string): void {
        let sellValue = parseFloat(this.productPrice);
        let discountPercent = parseFloat(this.promotionForm.controls['perc'].value) || 0;
        let discountValue = parseFloat(this.promotionForm.controls['price'].value) || 0;

        if (inputName == 'amount' || inputName == 'perc') {
            this.promotionForm.controls['price'].setValue((sellValue / 100 * (100-discountPercent)).toFixed(2));
        }
        if (inputName == 'price') {
            discountPercent = (sellValue-discountValue) * 100 /sellValue;
            if (discountPercent.toString().includes('.')) {
                this.promotionForm.controls['perc'].setValue((discountPercent).toFixed(2));
            } else {
                this.promotionForm.controls['perc'].setValue(discountPercent);
            }
        }
    }

    validInputValue(inputName: string): void {
        let inputValue = this.promotionForm.controls[inputName].value;
        if (inputValue.includes(',')) {
            this.promotionForm.controls[inputName].setValue(inputValue.split(',').join('.'));
        }
    }

    savePromotionList(): void {
        this.promotionListSaved.emit(this.promotionList);
        this.closeModal();
    }

    closeModal(): void {
        this.hideModal.emit(false);
        this.promotionForm.reset();
        this.promotionList = [];
        this.dataloaded = false;
    }
}
