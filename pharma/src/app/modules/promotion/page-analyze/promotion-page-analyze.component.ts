import {Component} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

import {DatabaseService} from "../../../core/services/database/database.service";
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../core/services/storage/storage.service";
import {ProductDatabase} from "../../product/product.database";

@Component({
    selector: 'fs-promotion-page-analyze.component',
    templateUrl: './promotion-page-analyze.component.html',
    styleUrls: ['./promotion-page-analyze.component.scss']
})
export class PromotionPageAnalyzeComponent {

    public autoCompleteList: object[] = [];
    public autoCompleteListSelected: string[] = [];
    public fileMissingProducts: string = '';
    public fileMissingProductsBtnToggle: boolean = false;
    public modalConfirmAutoComplete: boolean = false;
    public modalProductPromotion: boolean = false;
    public modalRemoveProduct: boolean = false;
    public modalTranferUpProduct: boolean = false;
    public productAnalysisList: object[] = [{loading: true}];
    public productAnalysisListFilteredSearch: object[] = [];
    public productAnalyzedList: object[] = [{loading: true}];
    public productAnalyzedListFilteredSearch: object[] = [];
    public productAnalysisLineForm: FormGroup;
    public productAnalyzedLineForm: FormGroup;
    public productSelectedCode: string = null;
    public productSelectedPrice: string = null;
    public productSelectedPromotionRule: object[] = null;
    public promotionId: string = '';

    constructor(
        public activatedRoute : ActivatedRoute,
        public formBuilder: FormBuilder,
        public router: Router,
        public titleService: Title,
        public databaseService: DatabaseService,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public productDatabase: ProductDatabase,
        public storageService: StorageService,
    ) {
        this.titleService.setTitle('Farmácia SMO - Promoções - análise');
        // const navigation = this.router.getCurrentNavigation()
        // if (navigation.extras.state && navigation.extras.state.promotionId) {
        //     this.promotionId = navigation.extras.state.promotionId;
        //     this.getPromotionAlalyzeList();
        // } else {
        //     this.router.navigate(['/promotion']).then(()=>false);
        // }
        this.activatedRoute.params.subscribe(params => {
            this.promotionId = params['promotionId'];
            this.getPromotionAnalyzeList();
        });
        this.productAnalysisLineForm = this.formBuilder.group({
            'lines': this.formBuilder.array([this.createLineAnalysis()]),
            'allSelected': new FormControl('', [Validators.required]),
        });
        this.productAnalyzedLineForm = this.formBuilder.group({
            'lines': this.formBuilder.array([this.createLineAnalyzed()]),
            'allSelected': new FormControl('', [Validators.required]),
        });
    }

    createLineAnalysis(): FormGroup {
        return this.formBuilder.group({
            'selected': new FormControl(false, [Validators.required]),
            'discountValue': new FormControl('0', [Validators.required]),
            'discountPercent': new FormControl('0', [Validators.required]),
            'sell_value': new FormControl('0', [Validators.required]),
            'alter_sell_value': new FormControl(false, [Validators.required]),
        });
    }
    createLineAnalyzed(): FormGroup {
        return this.formBuilder.group({
            'selected': new FormControl(false, [Validators.required]),
        });
    }
    get analysisLinesForm() {return this.productAnalysisLineForm.controls;}
    get analysisLine() {return this.analysisLinesForm['lines'] as FormArray;}
    addAnalysisLine() {
        this.analysisLine.push(this.createLineAnalysis());
    }

    get analyzedLinesForm() {return this.productAnalyzedLineForm.controls;}
    get analyzedLine() {return this.analyzedLinesForm['lines'] as FormArray;}
    addAnalyzedLine() {
        this.analyzedLine.push(this.createLineAnalyzed());
    }

    getProductAutocompleteList(inputText: string): void {
        if (inputText.length < 4) {return;}
        this.productDatabase.getProductAutocomplete({description: inputText}, (res) =>{
            if (!res.error) {
                this.autoCompleteList = res.data;
            }
        });
    }
    validAutoCompleteListSelected(codeList: string[]): void {
        this.autoCompleteListSelected = codeList;
        let checkGrouped: number = 0;
        this.autoCompleteListSelected.forEach(code => {
            let productData = this.autoCompleteList.filter(product => product['code'] == code);
            if (productData[0]['product_code_main'] != null) {
                checkGrouped++;
            }
        })

        if (checkGrouped > 0) {
            this.modalConfirmAutoComplete = true;
        } else {
            this.postAutoCompleteListSeletected();
        }
    }

    postAutoCompleteListSeletected(grouped: boolean = false): void {
        this.modalConfirmAutoComplete = false;
        let dataHere: object = {promotion_id: this.promotionId, code: this.autoCompleteListSelected, grouped: grouped}
        this.databaseService.postPromotionAnalyzeProduct(dataHere,(res) => {
            if (!res.error) {
                this.getPromotionAnalyzeList();
            }
        });
    }

    getPromotionAnalyzeList(): void {
        this.databaseService.getPromotionListAnalyze(this.promotionId,(res) => {
            if (!res.error) {
                this.productAnalysisList = res.data.productOnAnalysis;
                this.analysisLine.clear();
                for (let productIndex = 0; productIndex < this.productAnalysisList.length; productIndex++) {
                    this.addAnalysisLine();
                    this.analysisLine.at(productIndex).get('sell_value').setValue(this.productAnalysisList[productIndex]['sell_value']);
                    if (this.productAnalysisList[productIndex]['promotion_rule'].length > 0){
                        this.analysisLine.at(productIndex).get('discountValue').setValue(this.productAnalysisList[productIndex]['promotion_rule'][0]['price']);
                        this.analysisLine.at(productIndex).get('discountPercent').setValue(this.productAnalysisList[productIndex]['promotion_rule'][0]['perc']);
                    }
                }
                this.fileMissingProducts = res.data.fileMissingProducts
                this.productAnalysisListFilteredSearch = this.productAnalysisList;
                this.productAnalyzedList = res.data.productOnAnalyzed;
                this.analyzedLine.clear();
                for (let productIndex = 0; productIndex < this.productAnalyzedList.length; productIndex++) {
                    this.addAnalyzedLine();
                }
                this.productAnalyzedListFilteredSearch = this.productAnalyzedList;
            }
        });
    }

    filterProductAnalysisSearch(inputText: string): void {
        this.productAnalysisListFilteredSearch = this.productAnalysisList.filter(item => {
            return  item['description'].toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        })
    }
    filterProductAnalyzedSearch(inputText: string): void {
        this.productAnalyzedListFilteredSearch = this.productAnalyzedList.filter(item => {
            return  item['description'].toLowerCase().includes(inputText.toLowerCase()) || item['bar_code'].toLowerCase().includes(inputText.toLowerCase());
        })
    }

    analysisFormSelectAll(allOne: string): void {
        let checkAll: number = 0
        for (let lineIndex = 0; lineIndex < this.analysisLine.length; lineIndex++) {
            if (allOne == 'all') {
                this.analysisLine.at(lineIndex).get('selected').setValue(this.productAnalysisLineForm.controls['allSelected'].value)
            } else{
                if (this.analysisLine.at(lineIndex).get('selected').value) {checkAll++;}
            }
        }
        if (allOne == 'one') {
            this.productAnalysisLineForm.controls['allSelected'].setValue(checkAll==this.analysisLine.length);
        }
    }
    analyzedFormSelectAll(allOne: string): void {
        let checkAll: number = 0
        for (let lineIndex = 0; lineIndex < this.analyzedLine.length; lineIndex++) {
            if (allOne == 'all') {
                this.analyzedLine.at(lineIndex).get('selected').setValue(this.productAnalyzedLineForm.controls['allSelected'].value)
            } else{
                if (this.analyzedLine.at(lineIndex).get('selected').value) {checkAll++;}
            }
        }
        if (allOne == 'one') {
            this.productAnalyzedLineForm.controls['allSelected'].setValue(checkAll==this.analyzedLine.length);
        }
    }

    validAnalysisLineValue(productCode: string, inputName): void {
        let productIndex = this.productAnalysisList.findIndex(item => item['code'] == productCode);
        let inputValue = this.analysisLine.at(productIndex).get(inputName).value;
        if (inputValue.includes(',')) {
            this.analysisLine.at(productIndex).get(inputName).setValue(inputValue.split(',').join('.'));
        }
    }
    calcAnalysisLine(productCode: string, inputName: string): void {
        let productIndex = this.productAnalysisList.findIndex(item => item['code'] == productCode);
        let sellValue = parseFloat(this.analysisLine.at(productIndex).get('sell_value').value);
        let discountPercent = parseFloat(this.analysisLine.at(productIndex).get('discountPercent').value);
        let discountValue = parseFloat(this.analysisLine.at(productIndex).get('discountValue').value);

        if (inputName == 'discountPercent' || inputName == 'sell_value') {
            this.analysisLine.at(productIndex).get('discountValue').setValue((sellValue / 100 * (100-discountPercent)).toFixed(2));
        }
        if (inputName == 'discountValue') {
            discountPercent = (sellValue-discountValue) * 100 /sellValue;
            if (discountPercent.toString().includes('.')) {
                this.analysisLine.at(productIndex).get('discountPercent').setValue((discountPercent).toFixed(2));
            } else {
                this.analysisLine.at(productIndex).get('discountPercent').setValue(discountPercent);
            }
        }
        this.analysisLine.at(productIndex).get('selected').setValue(true);
    }

    showModalProductPromotion(productCode: string): void {
        let productIndex = this.productAnalysisList.findIndex(item => item['code'] == productCode);
        this.productSelectedCode = productCode;
        this.productSelectedPrice = this.analysisLine.at(productIndex).get('sell_value').value;
        this.productSelectedPromotionRule = this.productAnalysisList[productIndex]['promotion_rule'];
        this.modalProductPromotion = true;
    }
    setProductPromotionRule(promotionRuleList: object[]): void {
        let productIndex = this.productAnalysisList.findIndex(item => item['code'] == this.productSelectedCode);
        this.postProductList([{
            code: this.productAnalysisList[productIndex]['code'],
            sell_value: this.analysisLine.at(productIndex).get('sell_value').value,
            promotion_rule_new: promotionRuleList,
            status: 1
        }]);
    }
    hideModalProductPromotion(): void {
        this.modalProductPromotion = false;
        this.productSelectedCode = null;
        this.productSelectedPrice = null;
        this.productSelectedPromotionRule = null;
    }

    showModalProductPromotionView(productCode: string): void {
        let productIndex = this.productAnalyzedList.findIndex(item => item['code'] == productCode);
        this.productSelectedCode = productCode;
        this.productSelectedPrice = this.productAnalysisList[productIndex]['sell_value'];
        this.productSelectedPromotionRule = this.productAnalysisList[productIndex]['promotion_rule'];
        this.modalProductPromotion = true;
    }
    setProductPromotionRuleView(promotionRuleList: object[]): void {
        let productIndex = this.productAnalysisList.findIndex(item => item['code'] == this.productSelectedCode);
        this.postProductList([{
            code: this.productAnalysisList[productIndex]['code'],
            sell_value: this.productAnalysisList[productIndex]['sell_value'],
            promotion_rule_new: promotionRuleList,
            status: 0
        }]);
    }
    hideModalProductPromotionView(): void {
        this.modalProductPromotion = false;
        this.productSelectedCode = null;
        this.productSelectedPrice = null;
        this.productSelectedPromotionRule = null;
    }

    showModalRemoveProduct(productCode: string): void {
        this.productSelectedCode = productCode;
        this.modalRemoveProduct = true;
    }
    removeProduct(): void {
        let productCode: string = this.productSelectedCode;
        this.databaseService.deletePromotionAnalyzeRemoveProduct({product_code: productCode, promotion_id: this.promotionId},(res) => {
            if (!res.error) {
                this.getPromotionAnalyzeList()
                this.hideModalRemoveProduct()
            }
        })
    }
    hideModalRemoveProduct(): void {
        this.modalRemoveProduct = false;
        this.productSelectedCode = null;
    }

    showModalTransferUpProduct(productCode: string): void {
        this.productSelectedCode = productCode;
        this.modalTranferUpProduct = true;
    }
    transferUpProduct(): void {
        let productCode: string = this.productSelectedCode;
        let productIndex: number = this.productAnalyzedList.findIndex(product => product['code'] == productCode);
        let productData: object[] = [{
            code: this.productAnalyzedList[productIndex]['code'],status: 1
        }]
        this.postProductList(productData);
        this.hideModalTransferUpProduct()

    }
    hideModalTransferUpProduct(): void {
        this.modalRemoveProduct = false;
        this.modalTranferUpProduct = null;
    }

    transferMultDown(): void {
        let productData: object[] = [];
        for (let lineIndex = 0; lineIndex < this.analysisLine.length; lineIndex++) {
            if (this.analysisLine.at(lineIndex).get('selected').value == true) {
                productData.push({
                    code: this.productAnalysisList[lineIndex]['code'],
                    sell_value: this.analysisLine.at(lineIndex).get('sell_value').value,
                    promotion_rule_new: [{amount:1,price: this.analysisLine.at(lineIndex).get('discountValue').value,perc: this.analysisLine.at(lineIndex).get('discountPercent').value}],
                    status: 0,
                    alter_sell_value: this.analysisLine.at(lineIndex).get('alter_sell_value').value
                })
            }
        }
        if (productData.length == 0) {
            this.functionsService.openSnackBar('selecione ao menos um produto');
            return;
        }
        this.postProductList(productData);
    }
    transferMultUp(): void {
        let productData: object[] = [];
        for (let lineIndex = 0; lineIndex < this.analyzedLine.length; lineIndex++) {
            if (this.analyzedLine.at(lineIndex).get('selected').value == true) {
                productData.push({
                    code: this.productAnalyzedList[lineIndex]['code'],
                    status: 1
                });
            }
        }
        if (productData.length == 0) {
            this.functionsService.openSnackBar('selecione ao menos um produto');
            return;
        }
        this.postProductList(productData);
    }
    postProductList(productList: object[]): void {
        this.databaseService.postPromotionAnalyzeProductToAnalyzed({promotion_id: this.promotionId, productList: productList},(res) => {
            if (!res.error) {
                this.getPromotionAnalyzeList()
            }
        })
    }

    importData(file: File): void {
        let type = file.name.split('.').reverse().toString().split(',')[0];
        if (type !== 'csv') {
            this.functionsService.openSnackBar('O arquivo de importação deve ser do tipo CSV');
            return;
        }
        const formData = new FormData();
        formData.append('file', file);
        formData.append('promotion_id', this.promotionId);

        this.databaseService.postPromotionAnalyzeImportFile(formData,(res)=>{
            if(!res.error){
                this.getPromotionAnalyzeList();
            }
        });
    }
    importMissing(): void {
        this.databaseService.postPromotionAnalyzeImportFile({promotion_id: this.promotionId, missing:true}, (res) => {
            if (!res.error) {
                window.location.reload();
            }
        })
    }
    downloadMissing(): void {
        this.databaseService.getPromotionImportMissingFile({promotion_id: this.promotionId}, (res) => {
            if (!res.error) {
                this.functionsService.downloadFile(res.data,'csv','produtos_sem_cadastro_'+this.promotionId);
            }
        })
    }
    sendPromotion(): void {
        this.databaseService.postPromotionEffecture({promotion_id: this.promotionId}, (res) => {
            if (!res.error) {
                this.router.navigate(['/promotion']).then(() => false);
            }
        })
    }
}
