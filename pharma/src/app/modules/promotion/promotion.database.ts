import {Injectable} from '@angular/core';

import {DatabaseService} from '../../core/services/database/database.service';

@Injectable({
    providedIn: 'root'
})
export class PromotionDatabase {

    constructor(
        private databaseService: DatabaseService,
    ){}


    getPromotionListProduct(promotionId, callback) {
        this.databaseService.get('/promotion/getProductList/' + promotionId, [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    deleteNextPromotion(productCode, callback) {
        this.databaseService.delete('/productPromotion/nextPromotion/' + productCode,[])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

}
