import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";

import {DatabaseService} from "../../../core/services/database/database.service";
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../core/services/storage/storage.service";

@Component({
    selector: 'fs-promotion-page-list',
    templateUrl: './promotion-page-list.component.html',
    styleUrls: ['./promotion-page-list.component.scss']
})
export class PromotionPageListComponent implements OnInit {

    public promotionSelectedId: string;
    public modalPromotion: boolean = false;
    public modalDeletePromotion: boolean = false;
    public promotionListStatus1: object[] = [{loading: true}];
    public promotionListStatus1FilteredSearch: object[] = [];
    public promotionListStatus2: object[] = [{loading: true}];
    public promotionListStatus2FilteredSearch: object[] = [];
    public promotionListStatus3: object[] = [{loading: true}];
    public promotionListStatus3FilteredSearch: object[] = [];

    constructor(
        public databaseService : DatabaseService,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
    ) {
        this.getPromotionList();
    }

    getPromotionList(): void {
        this.databaseService.getPromotionList((res) => {
            if (!res.error) {
                this.promotionListStatus1 = res.data.promotionsActive;
                this.promotionListStatus1FilteredSearch = this.promotionListStatus1;
                this.promotionListStatus2 = res.data.promotionsFinished;
                this.promotionListStatus2FilteredSearch = this.promotionListStatus2;
                this.promotionListStatus3 = res.data.promotionsInAnalysis;
                this.promotionListStatus3FilteredSearch = this.promotionListStatus3;
            }
        });
    }

    ngOnInit(): void {}

    showWeekDay(dayList: string): string {
        let dayName = ["", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb","Dom"];
        let dayReturn: string = '';
        dayList.split(',').forEach(letter => {
            dayReturn =  dayReturn+', '+dayName[parseInt(letter)];
        })
        return dayReturn.slice(2);
    }


    showModalPromotion(id: string = null){
        this.promotionSelectedId = id;
        this.modalPromotion = true;
    }
    hideModalPromotion(){
        this.modalPromotion = false;
        this.promotionSelectedId = null;
    }

    showModalDeletePromotion(id: string = null): void {
        this.promotionSelectedId = id;
        this.modalDeletePromotion = true;
    }
    deletePromotion(): void {
        let id = this.promotionSelectedId;
        this.databaseService.deletePromotion(id,(res) => {
            if (!res.error) {
                this.getPromotionList();
                this.hideModalDeletePromotion();
            }
        })
    }
    hideModalDeletePromotion(): void {
        this.modalDeletePromotion = false;
        this.promotionSelectedId = null;
    }
}
