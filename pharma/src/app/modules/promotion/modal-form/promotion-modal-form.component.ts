import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

import {DatabaseService} from "../../../core/services/database/database.service";
import {StorageService} from "../../../core/services/storage/storage.service";
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {storageKeys} from "../../../core/variables/storageKeys";

@Component({
    selector: 'fs-promotion-modal-form',
    templateUrl: './promotion-modal-form.component.html',
    styleUrls: ['./promotion-modal-form.component.scss']
})
export class PromotionModalFormComponent implements OnChanges {

    @Input() showModal: boolean = true;
    @Input() promotionId: string;
    @Input() promotionIndex: string;

    @Output() hideModal = new EventEmitter<boolean>();
    @Output() promotionData = new EventEmitter<any>();
    @Output() promotionPosted = new EventEmitter<any>();
    @Output() promotionUpdated = new EventEmitter<any>();

    public updateOn: boolean = false;
    public stateFields: object = {name:{class:''},begin_date_form:{class:''},end_date_form:{class:''},priority:{class:''},};
    public day_of_week: number[] = [1,2,3,4,5,6,7];

    public promotionForm: FormGroup;

    constructor(
        public formBuilder : FormBuilder,
        public databaseService : DatabaseService,
        public storageService: StorageService,
        public functionsService: FunctionsService,
    ){
        this.promotionForm = this.formBuilder.group({
            'name': new FormControl('', [Validators.required]),
            'begin_date_form': new FormControl('', [Validators.required]),
            'end_date_form': new FormControl('', [Validators.required]),
            'priority': new FormControl('3', [Validators.required]),
        });
    }

    ngOnChanges(): void {
        if (this.promotionId && this.showModal) {
            this.updateOn = true;
            this.databaseService.getPromotion( this.promotionId,(res)=>{
                if (!res.error) {
                    res = res.data;
                    this.promotionForm.controls['name'].setValue(res['name']);
                    this.promotionForm.controls['begin_date_form'].setValue(this.functionsService.dateENtoBR(res['begin_date']));
                    this.promotionForm.controls['end_date_form'].setValue(this.functionsService.dateENtoBR(res['end_date']));
                    this.promotionForm.controls['priority'].setValue(res['priority']);

                    this.day_of_week = [];
                    res['day_of_week'].split(',').forEach(num => {
                        this.setDayId(parseInt(num))
                    });
                } else {
                    this.closeModal();
                }
            })
        }
    }

    setDayId(id: number): void {
        if (this.day_of_week.find(item => item == id )) {
            this.day_of_week = this.day_of_week.filter(item => item != id);
        } else {
            this.day_of_week.push(id);
        }
    }

    sendPromotion(): void {
        if (!this.checkErrorField('name')) {
            this.functionsService.openSnackBar('Informe um nome para a Promocao!')
        }
        if (!this.checkErrorField('begin_date_form')) {
            this.functionsService.openSnackBar('Informe uma data de início!')
        }
        if (!this.checkErrorField('end_date_form')) {
            this.functionsService.openSnackBar('Informe uma data de fim!')
        }
        if (!this.checkErrorField('priority')) {
            this.functionsService.openSnackBar('Informe uma prioridade!')
        }
        if (this.day_of_week.length == 0) {
            this.functionsService.openSnackBar('Informe ao menos 1 dia da semana!')
        }

        let begin_date = this.functionsService.dateBRtoEN(this.promotionForm.controls['begin_date_form'].value)
        let end_date = this.functionsService.dateBRtoEN(this.promotionForm.controls['end_date_form'].value)
        let promotionData: object = {...this.promotionForm.value,begin_date,end_date,day_of_week: this.day_of_week};

        this.updateOn ? this.putPromotion(promotionData,this.promotionId) : this.postPromotion(promotionData);
    }
    postPromotion(promotionData: object): void {
        this.databaseService.postPromotion(promotionData,(res)=>{
            if (!res.error) {
                let id: string = res.data.id;
                this.promotionPosted.emit({...promotionData,id})
            }
            this.closeModal();
        });
    }
    putPromotion(promotionData: object, promotion_id: string): void {
        this.databaseService.putPromotion(promotionData, promotion_id,(res)=>{
            if (!res.error) {
                let index = this.promotionIndex;
                let id = promotion_id;
                this.promotionUpdated.emit({...promotionData,id,index});
            }
        });
        this.closeModal();
    }

    checkErrorField(fieldName: string): boolean {
        if (this.promotionForm.controls[fieldName].valid) {
            this.stateFields[fieldName]['class'] = '';
            return true;
        } else {
            this.stateFields[fieldName]['class'] = 'emptyError';
            return false;
        }
    }

    closeModal(): void {
        this.updateOn = false;
        this.promotionForm.reset();
        this.hideModal.emit(false);
        this.day_of_week = [1,2,3,4,5,6,7]
    }
}
