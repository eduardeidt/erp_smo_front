import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {CoreModule} from "../../core/core.module";
import {DateExt} from "../../core/pipes/date-ext.pipe";
import {HomePageComponent} from "./page/home-page.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {ChartsModule} from "ng2-charts";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
    declarations: [
        HomePageComponent,
    ],
    imports: [
        CoreModule,
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        ChartsModule,
        SharedModule,
    ],
    providers: [
        DateExt
    ]
})
export class HomeModule {
}
