import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router";

import {DatabaseService} from "../../../core/services/database/database.service";
import {FunctionsService} from "../../../core/services/functions/functions.service";
import {IconRegularService} from "../../../core/services/icons/icon-regular.service";
import {IconSolidService} from "../../../core/services/icons/icon-solid.service";
import {StorageService} from "../../../core/services/storage/storage.service";
import {storageKeys} from "../../../core/variables/storageKeys";
import {ChartDataSets, ChartOptions, ChartType} from "chart.js";
import {Colors, Label} from "ng2-charts";
import {HomeDatabase} from "../home.database";

@Component({
    selector: 'fs-home',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

    public barChartOptions: ChartOptions = {responsive: true,scales: {xAxes: [{}], yAxes: [{ticks: {beginAtZero: true}}]},};
    public barChartLabelsMonth: Label[] = [];
    public barChartLabelsYear: Label[] = [];
    public barChartType: ChartType = 'bar';
    public barChartLegend = false;
    public barChartColors: Colors[] = [];
    public barChartDataMonth: ChartDataSets[] = [{ data: [],borderDash: [0] }];
    public barChartDataYear: ChartDataSets[] = [{ data: [],borderDash: [0] }];

    public chartMonthForm: FormGroup;
    public chartYearForm: FormGroup;

    public companyId: string = this.storageService.decodeFromLocalStorage(storageKeys.companyId);
    public dateSelected: string = '';
    public dateStart: string = this.functionsService.currentDateEN('0000-00-00','min');

    public salutation: string;
    public selectOptionListBranch: object[] = [];
    public selectOptionListUser: object[] = [];
    public userId: string = this.storageService.decodeFromLocalStorage(storageKeys.userId);
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;
    public userName: string = this.storageService.decodeFromLocalStorage(storageKeys.userName);

    constructor(
        public formBuilder: FormBuilder,
        public titleService: Title,
        public router: Router,
        public homeDatabase: HomeDatabase,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
    ) {
        this.titleService.setTitle('SmoPharma - Home');

        this.chartMonthForm = this.formBuilder.group({
            'branch': new FormControl(null, [Validators.required]),
            'seller': new FormControl(null, [Validators.required]),
        });
        this.chartYearForm = this.formBuilder.group({
            'branch': new FormControl(null, [Validators.required]),
            'seller': new FormControl(null, [Validators.required]),
        });
    }

    ngOnInit() {
        this.salutation = this.functionsService.getSalutation();

        if (this.userAdmin) {
            let arrayBgColor = [];
            for (let i = 0;i <= 31; i++) {arrayBgColor.push('#008483')}
            this.barChartColors = [{backgroundColor: arrayBgColor},];

            this.homeDatabase.getHomeData((res) => {
                if (!res.error) {
                    let homeData = res.data.homeData;
                    this.selectOptionListBranch = homeData['branchList'];
                    this.selectOptionListUser = homeData['userList'];
                    this.setMonthGraphicData(homeData['monthData'])
                    this.setYearGraphicData(homeData['yearData'])
                }
            })
        }
    }

    setMonthDate(date: string): void {
        if (this.dateStart != '') {this.dateStart = '';return}
        this.dateSelected = date;
        this.getMonthData();
    }

    getMonthData(): void {
        let params = {
            date: this.dateSelected,
            branchId: this.chartMonthForm.get('branch').value,
            userId: this.chartMonthForm.get('seller').value,
        }
        this.homeDatabase.getMonthData(params,(res) => {
            if (!res.error) {
                this.setMonthGraphicData(res.data.monthData)
            }
        });
    }
    setMonthGraphicData(monthData: object): void {
        this.barChartLabelsMonth = this.barChartDataMonth = [];
        this.barChartLabelsMonth = monthData['labelData'];
        this.barChartDataMonth = [{data: monthData['valueData'],label: 'R$'}]
    }

    getYearData(): void {
        let params = {
            branchId: this.chartYearForm.get('branch').value,
            userId: this.chartYearForm.get('seller').value,
        }

        this.homeDatabase.getYearData(params,(res) => {
            if (!res.error) {
                this.setYearGraphicData(res.data.yearData);
            }
        });
    }
    setYearGraphicData(yearData: object): void {
        this.barChartLabelsYear = this.barChartDataYear = [];
        this.barChartLabelsYear = yearData['labelData'];
        this.barChartDataYear = [{data: yearData['valueData'],label: 'R$'}]
    }

}
