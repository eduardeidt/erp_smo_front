import {Injectable} from '@angular/core';

import {DatabaseService} from '../../core/services/database/database.service';

@Injectable({
    providedIn: 'root'
})
export class HomeDatabase {

    constructor(
        private databaseService: DatabaseService,
    ){}


    getHomeData(callback) {
        this.databaseService.get('/home/getHomeData', [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getMonthData(params,callback) {
        this.databaseService.get('/home/getMonthData', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getYearData(params,callback) {
        this.databaseService.get('/home/getYearData', params)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

}
