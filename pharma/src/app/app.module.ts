import {AppRoutingModule} from './app-routing.module';
import {BrowserModule, Title} from '@angular/platform-browser';
import {CoreModule} from './core/core.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import {CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule} from '@angular/core';
import {MAT_DATE_LOCALE} from '@angular/material/core';

import {NgxMaskModule} from 'ngx-mask';
import {AppComponent} from './app.component';
import {DateExt} from './core/pipes/date-ext.pipe';
import {HomeModule} from './modules/home/home.module';
import {LoginModule} from './modules/login/login.module';
import {DateFnsConfigurationService, DateFnsModule} from 'ngx-date-fns';
import {SharedModule} from './shared/shared.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ptBR} from 'date-fns/locale';
import {registerLocaleData} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ProductModule} from './modules/product/product.module';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {MatTabsModule} from '@angular/material/tabs';
import {NotificationModule} from './modules/notification/notification.module';
import {PromotionModule} from './modules/promotion/promotion.module';
import {ConfigurationModule} from './modules/configuration/configuration.module';
import {UserModule} from './modules/user/user.module';
import {BatchModule} from "./modules/batch/batch.module";

registerLocaleData(localePt, 'pt');
const ptBrConfig = new DateFnsConfigurationService();
ptBrConfig.setLocale(ptBR);

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        CoreModule,
        NgxMaskModule.forRoot(),
        DateFnsModule.forRoot(),
        ReactiveFormsModule,
        SharedModule,
        AppRoutingModule,
        BatchModule,
        ConfigurationModule,
        HomeModule,
        LoginModule,
        NotificationModule,
        UserModule,
        ProductModule,
        PromotionModule,
        NgMultiSelectDropDownModule.forRoot(),
        NgxSpinnerModule,
        MatTabsModule
    ],
    providers: [
        DateExt,
        {
            provide: MAT_DATE_LOCALE, useValue: 'pt-BR'
        },
        {
            provide: LOCALE_ID, useValue: 'pt'
        },
        {
            provide: DateFnsConfigurationService, useValue: ptBrConfig
        },
        Title,
    ],
    bootstrap: [AppComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
})

export class AppModule {
}
