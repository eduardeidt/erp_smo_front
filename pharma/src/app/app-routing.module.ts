import {NgModule} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

import {AuthGuard} from "./core/services/auth/auth.guard";
import {ContentComponent} from "./core/components/content/content.component";
import {HomePageComponent} from "./modules/home/page/home-page.component";
import {LoginFormComponent} from "./modules/login/form/login-form.component";
import {LoginPagePasswordResetComponent} from "./modules/login/page-password-reset/login-page-password-reset.component";
import {LoginPagePasswordRecoverComponent} from "./modules/login/page-password-recover/login-page-password-recover.component";
import {ProductPageListComponent} from "./modules/product/page-list/product-page-list.component";
import {NotificationPageHomeComponent} from './modules/notification/page-home/notification-page-home.component';
import {PromotionPageListComponent} from './modules/promotion/page-list/promotion-page-list.component';
import {ConfigurationPageHomeComponent} from './modules/configuration/page-home/configuration-page-home.component';
import {PromotionPageAnalyzeComponent} from "./modules/promotion/page-analyze/promotion-page-analyze.component";
import {UserPageListComponent} from './modules/user/page-list/user-page-list.component';
import {BatchPageListComponent} from "./modules/batch/page-list/batch-page-list.component";
import {PromotionPageListProductComponent} from "./modules/promotion/page-list-product/promotion-page-list-product.component";
import {ProductPageListGuideComponent} from "./modules/product/page-list-guide/product-page-list-guide.component";
import {ProductPageListTestComponent} from "./modules/product/page-list-test/product-page-list-test.component";
import {ProductPageListSimpleComponent} from "./modules/product/page-list-simple/product-page-list-simple.component";
import {ProductPageListSimpleStockComponent} from "./modules/product/page-list-simple-stock/product-page-list-simple-stock.component";

const routes: Routes = [
    {path: 'login', component: LoginFormComponent},
    {path: 'password-reset', component: LoginPagePasswordResetComponent},
    {path: 'updateAuth/:hash', component: LoginPagePasswordRecoverComponent},
    {
        path: '',
        component: ContentComponent,
        canActivate: [AuthGuard],
        runGuardsAndResolvers: 'always',
        children: [
            {path: '', redirectTo: 'home', pathMatch: 'full'},
            {path: 'batch', component: BatchPageListComponent},
            {path: 'configuration', component: ConfigurationPageHomeComponent},
            {path: 'home', component: HomePageComponent},
            {path: 'notification', component: NotificationPageHomeComponent},
            {path: 'product', component: ProductPageListComponent},
            // {path: 'product-simple', component: ProductPageListSimpleComponent},
            {path: 'product-guide', component: ProductPageListGuideComponent},
            {path: 'promotion', component: PromotionPageListComponent},
            {path: 'promotion-analyse/:promotionId', component: PromotionPageAnalyzeComponent},
            {path: 'promotion-list-product', component: PromotionPageListProductComponent},
            {path: 'promotion-list-test', component: ProductPageListTestComponent},
            {path: 'simple-stock', component: ProductPageListSimpleStockComponent},
            {path: 'user', component: UserPageListComponent},

            {path: '**', component: HomePageComponent},
        ],
    },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
