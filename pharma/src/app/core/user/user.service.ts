// form-form-form-import { Injectable } from '@angular/core';
// form-form-form-import { TokenService } from '../token/token.service';
// form-form-form-import { BehaviorSubject } from 'rxjs';
// form-form-form-import { User } from './user';
//
// @Injectable({ providedIn: 'root'})
// export class UserService {
//
//     private userSubject = new BehaviorSubject<User>(null);
//     private userName: string;
//
//     constructor(private tokenService: TokenService) {
//
//         this.tokenService.hasToken() &&
//             this.decodeAndNotify();
//     }
//
//     setToken(token: string) {
//         this.tokenService.setToken(token);
//         this.decodeAndNotify();
//     }
//
//     getUser() {
//         return this.userSubject.asObservable();
//     }
//
//     private decodeAndNotify() {
//         const token = this.tokenService.getToken();
//         const user = jtw_decode(token) as User;
//         this.userName = user.name;
//         this.userSubject.next(user);
//     }
//
//     logout() {
//         this.tokenService.removeToken();
//         this.userSubject.next(null);
//     }
//
//     isLogged() {
//         return this.tokenService.hasToken();
//     }
//
//     getUserName() {
//         return this.userName;
//     }
// }
