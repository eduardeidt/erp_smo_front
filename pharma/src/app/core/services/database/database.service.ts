import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';

import {storageKeys} from '../../variables/storageKeys';
import {StorageService} from '../storage/storage.service';
import {FunctionsService} from '../functions/functions.service';

const databaseUrl = 'http://187.33.160.201:8088'; // homologação applicativa
// const databaseUrl = 'http://localhost:8000'; // local
// const databaseUrl = 'http://erp_laravel.com.br'; // local - eduard
// const databaseUrl = 'http://187.33.160.201:8767'; // prod
// const databaseUrl = 'http://farmaciasmo.ddns-intelbras.com.br:8767'; // local - eduard

const systemVersion = 'V1.12.0';

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {

    public loadingOn: EventEmitter<boolean> = new EventEmitter();

    constructor(
        public httpClient: HttpClient,
        public router: Router,
        public storageService: StorageService,
        public functionsService: FunctionsService,
    ) {}

    currentDatabaseUrl() {
        return databaseUrl;
    }
    currentSistemVersion() {
        return systemVersion;
    }
    emitLoadingOn(val: boolean) {
        this.loadingOn.emit(val);
    }
    getLoadingOn() {
        return this.loadingOn;
    }

    createHeader(authenticated = true) {
        let token = '';
        if (authenticated) {
            token = this.storageService.decodeFromLocalStorage(storageKeys.userToken);
        }
        return new HttpHeaders({
            'Authorization': 'Bearer ' + token,
        });
    }

    delete(url, params, authenticated = true) {
        let header = this.createHeader(authenticated);
        this.emitLoadingOn(true);
        return this.httpClient.delete(databaseUrl + '/api' + url, {headers: header, params: params}).pipe(tap(res => {
            this.msgEmmiter(res);
        }, (error) => {
            this.msgEmmiter(error);
        }));
    }
    get(url, params, authenticated = true, loadingScreen: boolean = true) {
        let header = this.createHeader(authenticated);
        this.emitLoadingOn(loadingScreen);
        return this.httpClient.get(databaseUrl + '/api' + url, {headers: header, params: params}).pipe(tap(res => {
            this.msgEmmiter(res);
        }, (error) => {
            this.msgEmmiter(error);
        }));
    }
    getFile(url, params, authenticated = true) {
        let header = this.createHeader();
        return this.httpClient.get(databaseUrl + '/api' + url, {headers: header, params: params, responseType: 'arraybuffer'}).pipe(tap(res => {
            this.msgEmmiter(res);
        }, (error) => {
            this.msgEmmiter(error);
        }));
    }
    post(url, body, authenticated = true, loadingScreen: boolean = true) {
        let header = this.createHeader(authenticated);
        this.emitLoadingOn(loadingScreen);
        return this.httpClient.post(databaseUrl + '/api' + url, body, {headers: header}).pipe(tap(res => {
            this.msgEmmiter(res);
        }, (error) => {
            this.msgEmmiter(error);
        }));
    }
    put(url, body, authenticated = true) {
        let header = this.createHeader(authenticated);
        this.emitLoadingOn(true);
        return this.httpClient.put(databaseUrl + '/api' + url, body, {headers: header}).pipe(tap(res => {
            this.msgEmmiter(res);
        }, (error) => {
            this.msgEmmiter(error);
        }));
    }


    deletePromotion(promotionId, callback) {
        this.delete('/promotion/' + promotionId, [])
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    deletePromotionAnalyzeRemoveProduct(productData, callback) {
        this.delete('/productPromotion', productData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    deleteUser(user_id, callback) {
        this.delete('/user/' + user_id, [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

    getBranchs(callback) {
        this.get('/branch', [])
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getFileZip(data, action, action_id, callback) {
        this.getFile('/generic/downloadFile', {...data, action, action_id})
            .subscribe((res) => {callback({data: res})}, (error) => {callback({error});});
    }
    getPromotion(promotionId, callback) {
        this.get('/promotion/' + promotionId, [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getPromotionNotInactive(callback) {
        this.get('/promotion/getNotInactive', [])
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getPromotionList(callback) {
        this.get('/promotion', [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getPromotionListAnalyze(promotionId, callback) {
        this.get('/productPromotion/getListToAnalyze/' + promotionId, [])
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getPromotionImportMissing(promotionData, callback) {
        this.get('/productPromotion/importMissingProducts', promotionData)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getPromotionImportMissingFile(promotionData, callback) {
        this.getFile('/productPromotion/showMissingProducts', promotionData)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getSync(params,callback) {
        this.get('/sync', params)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getUsers(callback) {
        this.get('/user', [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    getUser(userId, callback) {
        this.get('/user/'+userId, [])
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

    postLogin(loginData, callback) {
        this.post('/login', loginData, false)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postProduct(promotionData, callback) {
        this.post('/promotion', promotionData)
        .subscribe((res) => { callback({data: res}) }, (error) => { callback({error}) })
    }
    postProductPromo(productId, productData, callback) {
        this.post('/product/promotion', productData)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postPromotion(promotionData, callback) {
        this.post('/promotion', promotionData)
        .subscribe((res) => { callback({data: res}) }, (error) => { callback({error}) })
    }
    postPromotionEffecture(promotionData, callback) {
        this.post('/productPromotion/effectPromotion', promotionData)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postPromotionAnalyzeProduct(formData, callback) {
        this.post('/productPromotion/insertAnalyzeItem', formData)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postPromotionAnalyzeImportFile(formData, callback) {
        this.post('/productPromotion/importCsvToAnalyze', formData)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postPromotionAnalyzeProductToAnalyzed(productData, callback) {
        this.post('/productPromotion/confirmProductOnPromotion', productData)
        .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    postUser(userData, callback) {
        this.post('/user', userData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }


    putPromotion(promotionData, promotionId, callback) {
        this.put('/promotion/' + promotionId, promotionData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }
    putUser(userId, userData, callback) {
        this.put('/user/' + userId, userData)
            .subscribe((res) => {callback({data: res});}, (error) => {callback({error});});
    }

    logout() {
        let token = this.storageService.decodeFromLocalStorage(storageKeys.userToken);
        this.post('/logout', token, true).subscribe();
        this.goToLoginPage();
    }

    refreshToken() {
        let header = this.createHeader(false);
        let token = this.storageService.decodeFromLocalStorage(storageKeys.userToken);
        this.httpClient.post(databaseUrl + '/refresh', {token}, {headers: header})
            .subscribe((res) => {
                    this.storageService.encodeToLocalStorage(storageKeys.userToken, res);
                    window.location.reload();
                }, (error) => {
                    this.functionsService.openSnackBar('Sessão expirada, é necessário fazer login novamente', 'default', 5000);
                    this.goToLoginPage();
                }
            );
    }

    goToLoginPage() {
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigate(['/login']).then(() => false);
    }

    msgEmmiter(data) {
        this.emitLoadingOn(false);
        if (data.status && data.status == 401) {
            let token = this.storageService.decodeFromLocalStorage(storageKeys.userToken);
            if (token != null) {
                this.refreshToken();
                return;
            }
        }
        if (data.error && data.error.error.msg) {
            this.functionsService.openSnackBar(data.error.error.msg);
        }
        if (data.msg) {
            this.functionsService.openSnackBar(data.msg);
        }
    }
}
