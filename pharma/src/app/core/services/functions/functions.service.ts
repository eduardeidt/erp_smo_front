import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackBarComponent} from '../../../shared/components/snack-bar/snack-bar.component';

@Injectable({
    providedIn: 'root'
})
export class FunctionsService {

    constructor(
        public snackBar: MatSnackBar,
    ) {
    }

    // *word*
    arrayFilterWords(array, value, arrayPosition) {
        let regExp = new RegExp(this.removeSings(value));
        return array.filter((oc) => {
            if (!oc[arrayPosition]) return;
            return this.removeSings(oc[arrayPosition]).match(regExp);
        })
    }
    arrayFilterAll(array, value, arrayPosition) {
        let regExp = new RegExp(value.toString());
        return array.filter((oc) => {
            if (!oc[arrayPosition]) return;
            return oc[arrayPosition].toString().match(regExp);
        })
    }
    arrayReoder(array: object[], field: string, desc: boolean = false): object[]{
        return array.sort((a,b) => {
            if (desc) return a[field].toUpperCase() < b[field].toUpperCase() ? 1 : -1;
            return a[field].toUpperCase() > b[field].toUpperCase() ? 1 : -1;
        });
    }
    arrayReoderNumber(array: object[], field: string, desc: boolean = false): object[]{
        return array.sort((a,b) => {
            if (desc) return a[field] < b[field] ? 1 : -1;
            return a[field] > b[field] ? 1 : -1;
        });
    }
    removeSings(word: string) {
        word = word.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        return word.replace(/([-.*+?^=!:$%{}()|\[\]\/\\])/g, "").toLowerCase();
    }
    removeSpaces(word: string) {
        return word.replace(/\s/g, "").toLowerCase();
    }

    // *date*
    dateSplit(date: string): string[] {
        let day,month,year: string;
        if (date.includes('-')){
            day = date.split('-')[2];
            month = date.split('-')[1];
            year = date.split('-')[0];
            return [day,month,year];
        }
        if (date.includes('/')){
            day = date.split('/')[0];
            month = date.split('/')[1];
            year = date.split('/')[2];
            return [day,month,year];
        }
    }
    currentDateBR(date, condition: string = 'max' || 'min') {
        return this.dateENtoBR(this.currentDateEN(date,condition));
    }
    currentDateEN(date, condition: string = 'max' || 'min') {
        let dateArray: string[] = this.dateSplit(date);

        let day: number = parseInt(dateArray[0]);
        let month: number = parseInt(dateArray[1]);
        let year: number = parseInt(dateArray[2]);
        let today = new Date();

        let currentDay: number = parseInt(String(today.getDate()).padStart(2, '0'));
        let currentMonth: number = parseInt(String(today.getMonth() + 1).padStart(2, '0'));
        let currentYear: number = today.getFullYear();

        if (condition == 'max') {
            if (year == currentYear) {
                if (month == currentMonth) {
                    if (day > currentDay) {
                        day = currentDay;
                    }
                }
                if (month > currentMonth) {
                    month = currentMonth;
                    day = currentDay;
                }
            }
            if (year > currentYear) {
                year = currentYear;
                month = currentMonth;
                day = currentDay;
            }
        } else {
            if (year == currentYear) {
                if (month == currentMonth) {
                    if (day < currentDay) {
                        day = currentDay;
                    }
                }
                if (month < currentMonth) {
                    month = currentMonth;
                    day = currentDay;
                }
            }
            if (year < currentYear) {
                year = currentYear;
                month = currentMonth;
                day = currentDay;
            }
        }

        let dayReturn: string = String(day);
        let monthReturn: string = String(month);
        let yearReturn: string = String(year);

        if (dayReturn.length == 1) dayReturn = '0'+day;
        if (monthReturn.length == 1) monthReturn = '0'+month;
        if (yearReturn.length == 1) yearReturn = '0'+year;

        return  yearReturn + '-' + monthReturn + '-' + dayReturn;
    }
    dateENtoBR(date) {
        if (date.length <10){date = '0000-00-00';}
        return date.split('-').reverse().join('/');
    }
    dateBRtoEN(date) {
        if (date.length <10){date = '00-00-0000';}
        return date.split('/').reverse().join('-');
    }

    getDayAfter(date: string | Date, daysAfter: number = 1): string {
        let after = new Date(date);
        after.setDate(after.getDate() + (daysAfter + 1));
        let day = String(after.getDate()).padStart(2, '0');
        let month = String(after.getMonth() + 1).padStart(2, '0');
        let year = after.getFullYear();
        return year + '-' + month + '-' + day;
    }
    getDayBefore(date: string | Date, daysBefore: number = 1): string {
        let before = new Date(date);
        before.setDate(before.getDate() - (daysBefore - 1));
        let dd: string = String(before.getDate()).padStart(2, '0');
        let mm: string = String(before.getMonth() + 1).padStart(2, '0');
        let aaaa: string = String(before.getFullYear());
        return aaaa + '-' + mm + '-' + dd;
    }
    getDayName(date: string): string{
        let refer = new Date(date);
        refer.setDate(refer.getDate() + 1);
        let arrayDay = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"];
        return arrayDay[refer.getDay()];
    }
    getFirstLastDay(date: string = ''): string[] {
        if (date == '') {
            date = this.currentDateBR('01/01/2020','min');
        }
        let currentMonth = this.dateSplit(date)[1];
        let currentYear = this.dateSplit(date)[2];
        let beginDate: string = currentYear+'-'+currentMonth+'-01';
        let nextMonth: string = (parseInt(currentMonth)+1).toString()
        if (nextMonth.length == 1) {
            nextMonth= '0'+nextMonth;
        }
        let endDate: string = currentMonth != '12' ? this.getDayBefore(currentYear+'-'+nextMonth+'-01',1) : currentYear+'-12-31';
        return [beginDate,endDate];
    }
    getFullDateBR(date: string | Date): string {
        let refer = new Date(date);
        refer.setDate(refer.getDate() + 1);
        let weekDay = refer.getDay();
        let year = refer.getFullYear();
        let month = refer.getMonth();
        let day = refer.getDate();
        let arrayDay = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"];
        let arrayMonth = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
        return arrayDay[weekDay] + ', ' + day + ' de ' + arrayMonth[month] + ' de ' + year;
    }
    getMonthAfterBR(date: string,monthAfter: number = 1): string {
        return this.dateENtoBR(this.getMonthAfterEN(date,monthAfter));
    }
    getMonthAfterEN(date: string, monthAfter: number = 1): string {
        let dateArray: string[] = this.dateSplit(date);

        let day: string = dateArray[0]
        let month: string = dateArray[1]
        let year: string = dateArray[2]

        if (parseInt(month)+monthAfter > 12) {
            month = String((monthAfter+parseInt(month))-12)
            year = String(parseInt(year)+1)
        } else {
            month = String(parseInt(month)+monthAfter)
        }
        if (month.length == 1) month = '0'+month;

        let dateReturn = year + '-' + month + '-' + day
        let dayNum: number = parseInt(this.removeSings(dateReturn));
        let lastDayNum: number = parseInt(this.removeSings(this.getFirstLastDay(dateReturn)[1]));

        if (dayNum > lastDayNum) {
            dateReturn = this.getFirstLastDay(dateReturn)[1];
        }

        return dateReturn;
    }
    getMonthBeforeBR(date: string,monthBefore: number = 1): string {
        return this.dateENtoBR(this.getMonthBeforeEN(date,monthBefore));
    }
    getMonthBeforeEN(date: string, monthBefore: number = 1): string {
        let dateArray: string[] = this.dateSplit(date);

        let day: string = dateArray[0]
        let month: string = dateArray[1]
        let year: string = dateArray[2]

        if (parseInt(month)-monthBefore < 1) {
            month = String(12-(monthBefore-parseInt(month)))
            year = String(parseInt(year)-1)
        } else {
            month = String(parseInt(month)-monthBefore)
        }
        if (month.length == 1) month = '0'+month;

        let dateReturn = year + '-' + month + '-' + day
        let dayNum: number = parseInt(this.removeSings(dateReturn));
        let lastDayNum: number = parseInt(this.removeSings(this.getFirstLastDay(dateReturn)[1]));

        if (dayNum > lastDayNum) {
            dateReturn = this.getFirstLastDay(dateReturn)[1];
        }

        return dateReturn;
    }
    getMonthName(date: string): string {
        let month = parseInt(this.dateSplit(date)[1]);
        let arrayMonth = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
        return arrayMonth[month - 1];
    }
    getSmallDateBR(date: string | Date): string {
        let before = new Date(date);
        before.setDate(before.getDate());
        let dd = String(before.getDate()).padStart(2, '0');
        let mm = String(before.getMonth() + 1).padStart(2, '0');
        let aaaa = before.getFullYear();
        return dd + '/' + mm + '/' + aaaa;
    }
    getSmallDateEN(date: string | Date): string {
        let before = new Date(date);
        before.setDate(before.getDate());
        let dd = String(before.getDate()).padStart(2, '0');
        let mm = String(before.getMonth() + 1).padStart(2, '0');
        let aaaa = before.getFullYear();
        return aaaa + '-' + mm + '-' + dd;
    }
    monthsDiff(firstDate: string | Date, SecondDate: string | Date): number {
        let date1: Date = new Date(firstDate);
        let date2: Date = new Date(SecondDate);
        let years = this.yearsDiff(firstDate, SecondDate);
        return (years * 12) + (date2.getMonth() - date1.getMonth());
    }
    yearsDiff(firstDate: string | Date, SecondDate: string | Date): number {
        let date1: Date = new Date(firstDate);
        let date2: Date = new Date(SecondDate);
        return date2.getFullYear() - date1.getFullYear();
    }

    // *file*
    downloadFile(res, type: string, name: string,onlyView = false) {
        let a: any = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        let blob = new Blob([res], {type: 'application/' + type});
        let url = window.URL.createObjectURL(blob);

        a.href = url;
        if (onlyView) {
            a.target = '_blank';
        } else {
            a.download = name + "." + type;
        }
        a.click();
        document.body.removeChild(a);
        setTimeout(function(){
            // For Firefox it is necessary to delay revoking the ObjectURL
            window.URL.revokeObjectURL(url);
        }, 100);
    }

    // *Hour / Times*
    calcTimeInsideInterval(timeStart: string, TimeEnd: string, intervalStart: string, intervalEnd: string, fullDay: boolean = false): string {
        let totalTime = 0;
        let timeStartNum: number = this.hourToMinute(timeStart);
        let timeEndNum: number = this.hourToMinute(TimeEnd);
        let intervalStartNum: number = this.hourToMinute(intervalStart);
        let intervalEndNum: number = this.hourToMinute(intervalEnd);

        if (timeStartNum != 0 && timeEndNum == 0){
            timeEndNum = 1439;
        }
        if (timeStartNum == 0 && timeEndNum == 0 && fullDay){
            timeEndNum = 1439;
        }

        if (intervalStartNum > intervalEndNum){
            if (timeStartNum <= timeEndNum) {
                for (let time = timeStartNum; time <= timeEndNum; time++) {
                    if (time <= intervalEndNum || intervalStartNum <= time) {
                        totalTime++;
                    }
                }
            } else {
                for (let time = 0; time <= timeEndNum; time++) {
                    if (time <= intervalEndNum || intervalStartNum <= time) {
                        totalTime++;
                    }
                }
                for (let time = timeStartNum; time <= 1439; time++) {
                    if (time <= intervalEndNum || intervalStartNum <= time) {
                        totalTime++;
                    }
                }
            }
        } else{
            if (timeStartNum <= timeEndNum) {
                for (let time = timeStartNum; time <= timeEndNum; time++) {
                    if (intervalStartNum <= time && time <= intervalEndNum) {
                        totalTime++;
                    }
                }
            } else {
                for (let time = 0; time <= timeEndNum; time++) {
                    if (intervalStartNum <= time && time <= intervalEndNum) {
                        totalTime++;
                    }
                }
                for (let time = timeStartNum; time <= 1439; time++) {
                    if (intervalStartNum <= time && time <= intervalEndNum) {
                        totalTime++;
                    }
                }
            }
        }
        if (totalTime != 0) {
            totalTime--;
        }
        return this.minuteToHour(totalTime);
    }
    convertNightTime(hour): string {
        let nightTime: number = this.hourToMinute(hour);
        nightTime = nightTime/0.875;
        return this.minuteToHour(nightTime);
    }
    diffTime(times = [],equal = false): string {
        let t = [];
        let finalT = 0;
        let count = 0;
        times.forEach(item => {
            t.push(60 * parseInt(item.split(':')[0]) + parseInt(item.split(':')[1]));
            count++;
        });
        for (let i = 0; i < count; i++) {
            if(equal == false && t[i + 1] <  t[i]){t[i + 1] = t[i + 1]+1440}
            if(equal == true  && t[i + 1] <= t[i]){t[i + 1] = t[i + 1]+1440}
            finalT = finalT + (t[i + 1] - t[i]);
            i++;
        }
        let finalH = parseInt((finalT / 60).toString().split('.')[0]);
        let finalM = finalT - (finalH * 60);

        return this.numberToHour(finalH, finalM);
    }
    hourToMinute(hour): number {
        return 60 * parseInt(hour.split(':')[0]) + parseInt(hour.split(':')[1]);
    }
    minuteToHour(minute): string {
        let finalH = parseInt((minute / 60).toString().split('.')[0]);
        let finalM = minute - (finalH * 60);
        return this.numberToHour(finalH,finalM);
    }
    multiplyTime(time, mult): string {
        let finalT = 0;
        finalT = 60 * parseInt(time.split(':')[0]) + parseInt(time.split(':')[1]);

        finalT = finalT * mult;
        let finalH = parseInt((finalT / 60).toString().split('.')[0]);
        let finalM = finalT - (finalH * 60);

        return this.numberToHour(finalH, finalM);
    }
    numberToHour(finalH: number, finalM: number): string {
        let displayTime: string = '';
        let displaySign: string = '';
        //if is a negative number, return sign "-" in front of the number.
        if (finalH.toString().includes('-') || finalM.toString().includes('-')) {
            finalH = Math.abs(finalH);
            finalM = Math.abs(finalM);
            displaySign = '-'
        }
        if (finalH.toString().length == 1) {
            displayTime = '0';
        }
        displayTime = displayTime + (finalH).toString() + ':';
        if (finalM.toString().length == 1) {
            displayTime = displayTime + '0';
        }
        displayTime = displayTime + (finalM).toString();
        return displaySign+displayTime;
    }
    sumTime(times = []): string {
        let t = [];
        let finalT = 0;
        let count = 0;
        times.forEach(item => {
            t.push(60 * parseInt(item.split(':')[0]) + parseInt(item.split(':')[1]));
            count++;
        });
        for (let i = 0; i < count; i++) {
            finalT = finalT + t[i];
        }
        let finalH = parseInt((finalT / 60).toString().split('.')[0]);
        let finalM = finalT - (finalH * 60);
        return this.numberToHour(finalH, finalM);
    }
    subtractTime(t1: string, t2: string): string {
        let time1: number = 60 *parseInt(t1.split(':')[0]) + parseInt(t1.split(':')[1]);
        let time2: number = 60 *parseInt(t2.split(':')[0]) + parseInt(t2.split(':')[1]);

        let finalT: number = time1 - time2;
        let finalH: number = parseInt((finalT / 60).toString().split('.')[0]);
        let finalM: number = finalT - (finalH * 60);
        finalH = parseInt(finalH.toString());
        finalM = parseInt(finalM.toString());
        return this.numberToHour(finalH, finalM);
    }
    sumTimeNegative(t1: string, t2: string): string {
        let t1Number: number = parseInt(this.removeSings(t1));
        let t2Number: number = parseInt(this.removeSings(t2));
        let t1Negative: boolean = t1.includes('-');
        let t2Negative: boolean = t2.includes('-');

        if (t1Negative == t2Negative) {
            if (t1Negative || t2Negative) {
                return '-'+this.sumTime([t1.slice(1),t2.slice(1)])
            }
            return this.sumTime([t1,t2])
        }
        if (t1Negative != t2Negative) {
            let returnTime: string = '';
            t1Negative ? t1 = t1.slice(1) : t2 = t2.slice(1);
            if (t1Number > t2Number) {
                returnTime = this.subtractTime(t1,t2)
                if (t1Negative) {returnTime = '-'+returnTime}
            } else {
                returnTime = this.subtractTime(t2,t1)
                if (t2Negative) {returnTime = '-'+returnTime}
            }
            return returnTime;
        }
    }

    // *money*
    toMoney(x) {
        let cal = x.toString();
        if (cal.length > 2) {
            let begin = cal.substring(0, cal.length - 2);
            let end = cal.substring(cal.length - 2);
            cal = begin + ',' + end;
            if (cal.length > 6) {
                let begin = cal.substring(0, cal.length - 6);
                let end = cal.substring(cal.length - 6);
                cal = begin + '.' + end;
                if (cal.length > 10) {
                    let begin = cal.substring(0, cal.length - 10);
                    let end = cal.substring(cal.length - 10);
                    cal = begin + '.' + end;
                    if (cal.length > 14) {
                        let begin = cal.substring(0, cal.length - 14);
                        let end = cal.substring(cal.length - 14);
                        cal = begin + '.' + end;
                    }
                }
            }
        }
        return cal;
    }

    // *refresh*
    refresh() {
        window.location.reload();
    }

    // *salutation*
    getSalutation(){
        let today = new Date();
        let salutation: string;

        if (today.getHours() >= 6 && today.getHours() < 12) {
            salutation = 'Bom dia!';
        }
        if (today.getHours() >= 12 && today.getHours() < 18) {
            salutation = 'Boa tarde!';
        }
        if (today.getHours() >= 18 || today.getHours() < 6) {
            salutation = 'Boa noite!';
        }
        return salutation;
    }

    // *snackBar*
    openSnackBar(message, classe ='default', duration = 2000) {
        switch(message){
            case 'emptyField':      {message = 'Todos os campos obrigatórios devems ser preenchidos!';classe='error'; break;}
            case 'endDateBigger':   {message = 'A data final não pode ser menor que a data inicial';break;}
            case 'endHourBigger':   {message = 'A hora final não pode ser menor que a hora inicial';break;}
            case 'downloadError':   {message = 'Não foi possível baixar os arquivos agora';break;}
            case 'opsError':        {message = 'Ops! ocorreu um erro, tente novamente mais tarde.';break;}
            case 'invalidCpf':      {message = 'CPF inválido!';break;}
            case 'invalidCnpj':     {message = 'CNPJ inválido!';break;}
            case 'invalidEmail':    {message = 'Email inválido!';break;}
            case 'selectJourney':   {message = 'Selecione um modelo de escala para continuar';break;}
            case 'typeJourney':     {message = 'Selecione um tipo de escala para continuar';break;}
            case 'teste':           {message = 'teste';break;}
            case '':{message = 'Ocorreu um erro';break;}
            default:{break;}
        }
        this.snackBar.openFromComponent(SnackBarComponent, {
            duration: duration,
            data: {message}, panelClass: [classe],
            horizontalPosition: 'center',
            verticalPosition: 'bottom'
        });
    }

    containsArray(array, search) {
        var searchJson = JSON.stringify(search);
        var arrJson = array.map(JSON.stringify);

        return arrJson.indexOf(searchJson) >= 0;
    }
}
