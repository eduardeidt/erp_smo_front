import {Injectable} from '@angular/core';
import {storageKeys} from "../../variables/storageKeys";
import {StorageService} from "../storage/storage.service";
import {DatabaseService} from "../database/database.service";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        public storageService: StorageService,
        public databaseService: DatabaseService,
        public router: Router,
    ){}

    logIn(data,email){
        this.storageService.encodeToLocalStorage(storageKeys.userLogin, email);
        this.storageService.encodeToLocalStorage(storageKeys.userName, data['user']['name']);
        this.storageService.encodeToLocalStorage(storageKeys.userId, data['user']['id']);
        this.storageService.encodeToLocalStorage(storageKeys.userToken, data['token']);
        this.storageService.encodeToLocalStorage(storageKeys.userAdmin, data['user']['admin'] ? 1 : 0);
        this.storageService.encodeToLocalStorage(storageKeys.branchIds, data['user']['branch_ids']);
        // this.storageService.encodeToLocalStorage(storageKeys.icms1, data['user']['branch_ids']);
        // this.storageService.encodeToLocalStorage(storageKeys.icms2, data['user']['branch_ids']);
        // this.storageService.encodeToLocalStorage(storageKeys.icms3, data['user']['branch_ids']);
        // this.storageService.encodeToLocalStorage(storageKeys.icms4, data['user']['branch_ids']);

        this.router.navigate(['/home']).then(() => false);
    }
}
