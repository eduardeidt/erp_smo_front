import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";

import {Observable} from "rxjs";
import {DatabaseService} from "../database/database.service";
import {StorageService} from "../storage/storage.service";
import {storageKeys} from "../../variables/storageKeys";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate{

    constructor(
        public databaseService: DatabaseService,
        public storageService: StorageService,
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        let userToken = this.storageService.decodeFromLocalStorage(storageKeys.userToken);

        if (userToken == null || userToken == ''){
            this.databaseService.logout();
            return false;
        }
        return true;
    }
}
