import {Injectable} from '@angular/core';

import * as CryptoJS from 'crypto-js';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    public secretPassphrase = 'f@rm&rp@!#$a1b2c3';

    constructor() {

    }

    /**
     * Save encripted data in localStorage
     *
     * @param key
     * @param data
     */
    encodeToLocalStorage(key, data) {
        if (data === null) {
            localStorage.setItem(key, CryptoJS.AES.encrypt(data, this.secretPassphrase).toString());
        }

        if (data !== null) {
            localStorage.setItem(key, CryptoJS.AES.encrypt(data.toString().trim(), this.secretPassphrase).toString());
        }
    }

    /**
     * Return uncrypted data from localStorage by key
     *
     * @param key
     */
    decodeFromLocalStorage(key) {
        //return localStorage.getItem(key);
        let encryptedText = localStorage.getItem(key) ? localStorage.getItem(key) : null;

        if(encryptedText === null) {
            return null;
        }

        return CryptoJS.AES.decrypt(encryptedText.trim(), this.secretPassphrase.trim()).toString(CryptoJS.enc.Utf8);
    }

    /**
     * Remove data from localStorage by key
     *
     * @param key
     */
    removeLocalStorage(key) {
        localStorage.removeItem(key);
    }

    /**
     encodeToSessionStorage(key, data) {
        sessionStorage.setItem(key, data)
    }

     decodeFromSessionStorage(key) {
        return sessionStorage.getItem(key)
    }
     removeSessionStorage(key) {
        sessionStorage.removeItem(key)
    }
     **/
}
