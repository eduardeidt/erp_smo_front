import {Injectable} from '@angular/core';


@Injectable({
    providedIn: 'root'
})
export class ValidService {

    constructor() {}

    removeSings(word: string) {
        return word.replace(/([-.*+?^=!:$%{}()|\[\]\/\\])/g, "").toLowerCase();
    }

    validEmail(email) {
        if(email == null)
            return false;

        let user = email.toString().substring(0, email.toString().indexOf("@"));
        let dom = email.toString().substring(email.toString().indexOf("@") + 1, email.toString().length);

        let format: RegExp;
        format = /[`!@#$%^&*()+ =\[\]{};':"\\|,<>\/?áàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑ~]/;
        if(format.test(user) || format.test(dom)){return  false;}

        return (user.length >= 1) &&
            (dom.length >= 3) &&
            (user.search("@") == -1) &&
            (dom.search("@") == -1) &&
            (user.search(" ") == -1) &&
            (dom.search(" ") == -1) &&
            (dom.search(".") != -1) &&
            (dom.indexOf(".") >= 1) &&
            (dom.lastIndexOf(".") < dom.length - 1);
    }

    validCNPJ(cnpj) {
        cnpj = this.removeSings(cnpj);
        if (cnpj == '') return false;
        if (cnpj.length != 14) return false;
        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999")
            return false;

        // Valida DVs
        let tamanho = cnpj.length - 2
        let numeros = cnpj.substring(0, tamanho);
        let digitos = cnpj.substring(tamanho);
        let soma = 0;
        let pos = tamanho - 7;
        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)) return false;
        return true;
    }

    validCPF(cpf) {
        cpf = this.removeSings(cpf);
        if (cpf == '') return false;
        // Elimina CPFs invalidos conhecidos
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
            return false;
        // Valida 1o digito
        let add = 0;
        for (let i = 0; i < 9; i++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        let rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
        // Valida 2o digito
        add = 0;
        for (let i = 0; i < 10; i++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }

    validHour(hour,max24 = true){
        if(hour.length == 1){hour = '0'+hour+':00';}
        if(hour.length == 2){hour = hour+':00';}
        if(hour.length == 3){hour = hour+'00';}
        if(hour.length == 4){hour = hour+'0';}
        let mn = hour.toString().split(':')[1];
        let hs = hour.toString().split(':')[0];
        if(max24 == true){
            if(hs > 23){hs = '23';mn = '59';}
        }
        if(mn > 59){mn = '59';}
        hour = hs+':'+mn;
        return hour;
    }

    validHourNegativeGreaterThan24(hour) {
        if (hour == null ! || hour.length == 0) {
            return hour;
        }

        let signal = '';
        let time = hour;

        if (hour.charAt(0) == '-') {
            signal = '-';
            time = hour.slice(1);
        }

        if (time.length == 1) {
            time = '00' + time;
        }
        if (time.length == 2) {
            time = '0' + time;
        }
        if (time.length == 3) {
            time = time + ':00';
        }
        if (time.length == 4) {
            time = time + '00';
        }
        if (time.length == 5) {
            time = time + '0';
        }
        let mn = time.toString().split(':')[1];
        let hs = time.toString().split(':')[0];
        if (mn > 59) {
            mn = '00';
        }
        if (hs > 838) {
            hs = '838';
        }

        return signal + hs + ':' + mn;
    }

    validDate(date) {
        if(date.length < 10){date = '00/00/0000'}
        let year = date.toString().split('/')[2];
        let month = date.toString().split('/')[1];
        let day = date.split('/')[0];
        let maxYear = ((new Date()).getFullYear()) + 5;
        if (year > maxYear) {
            year = maxYear;
        }
        if (month > 12) {
            month = 12;
        }
        if (day > 31) {
            if (month == 4 || month == 6 || month == 9 || month == 11) {
                day = 30;
            } else {
                day = 31;
            }
        }
        if (day > 28 && month == 2) {
            if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
                day = 29;
            } else {
                day = 28;
            }
        }
        date = day + '/' + month + '/' + year;
        return date;
    }
}
