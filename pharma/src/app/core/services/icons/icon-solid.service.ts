import {Injectable} from '@angular/core';
import {
    faAngleDoubleLeft, faAngleDoubleRight,
    faArrowDown, faArrowLeft, faArrowRight, faArrowUp,
    faBars, faBell, faBoxOpen, faBuilding,
    faCalculator,
    faCalendarAlt,
    faCaretDown,
    faCheck,
    faCheckDouble, faChevronDown,
    faChevronLeft,
    faChevronRight,
    faClipboard,
    faCoins,
    faCogs,
    faComments,
    faDotCircle,
    faDownload,
    faEnvelopeOpenText,
    faExclamation,
    faExclamationTriangle,
    faEye,
    faFileContract,
    faFileInvoice,
    faFileMedical,
    faFileSignature,
    faHeadset,
    faHistory,
    faImages, faMinusSquare, faMoneyBillWaveAlt,
    faPercentage,
    faPills, faPlus,
    faRetweet,
    faSearch,
    faSignOutAlt,
    faSort, faStar,
    faSyncAlt,
    faTimes,
    faTrash,
    faTrashAlt,
    faUmbrellaBeach,
    faUpload,
    faUser,
    faUserCheck,
    faUserCircle,
    faUserClock,
    faUserLock,
    faUserTie, faIndustry
} from "@fortawesome/free-solid-svg-icons";

@Injectable({
    providedIn: 'root'
})

export class IconSolidService {
    public faAngleDoubleLeft = faAngleDoubleLeft;
    public faAngleDoubleRight = faAngleDoubleRight;
    public faArrowDown = faArrowDown;
    public faArrowLeft = faArrowLeft;
    public faArrowRight = faArrowRight;
    public faArrowUp = faArrowUp;
    public faBars = faBars;
    public faBell = faBell;
    public faBoxOpen = faBoxOpen;
    public faBuilding = faBuilding;
    public faCalendarAlt = faCalendarAlt;
    public faCalculator = faCalculator;
    public faCaretDown = faCaretDown;
    public faClipboard = faClipboard;
    public faCheck = faCheck;
    public faCheckDouble = faCheckDouble;
    public faChevronDown = faChevronDown;
    public faChevronLeft = faChevronLeft;
    public faChevronRight = faChevronRight;
    public faCoins = faCoins;
    public faComments = faComments;
    public faCogs = faCogs;
    public faDotCircle = faDotCircle;
    public faDownload = faDownload;
    public faEnvelopeOpenText = faEnvelopeOpenText;
    public faExclamation = faExclamation;
    public faExclamationTriangle = faExclamationTriangle;
    public faFileInvoice = faFileInvoice;
    public faFileMedical = faFileMedical;
    public faFileContract = faFileContract;
    public faFileSignature = faFileSignature;
    public faHistory = faHistory;
    public faHeadset = faHeadset;
    public faIndustry = faIndustry;
    public faImages = faImages;
    public faMoneyBillWaveAlt = faMoneyBillWaveAlt;
    public faMinuSquare = faMinusSquare;
    public faPercentage = faPercentage;
    public faPills = faPills;
    public faPlus = faPlus;
    public faRetweet = faRetweet;
    public faSearch = faSearch;
    public faSignOutAlt = faSignOutAlt;
    public faStar = faStar;
    public faSort = faSort;
    public faSyncAlt = faSyncAlt;
    public faTimes = faTimes;
    public faTrash = faTrash;
    public faTrashAlt = faTrashAlt;
    public faUmbrellaBeach = faUmbrellaBeach;
    public faUpload = faUpload;
    public faUser = faUser;
    public faUserLock = faUserLock;
    public faUserCheck = faUserCheck;
    public faUserCircle = faUserCircle;
    public faUserClock = faUserClock;
    public faUserTie = faUserTie;
}
