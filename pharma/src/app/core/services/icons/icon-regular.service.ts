import {Injectable} from '@angular/core';
import {
    faBell, faBuilding, faChartBar, faCheckCircle, faClock, faCopy,
    faEdit, faEye, faEyeSlash, faFilePdf, faQuestionCircle, faStar, faTimesCircle
} from "@fortawesome/free-regular-svg-icons";


@Injectable({
    providedIn: 'root'
})

export class IconRegularService {
    public faBell = faBell;
    public faBuilding = faBuilding;
    public faCopy = faCopy;
    public faClock = faClock;
    public faCheckCircle = faCheckCircle;
    public faEye = faEye;
    public faEyeSlash = faEyeSlash;
    public faEdit = faEdit;
    public faChartBar = faChartBar;
    public faFilePdf = faFilePdf;
    public faStar = faStar;
    public faTimesCircle = faTimesCircle;
    public faQuestionCircle = faQuestionCircle;
}
