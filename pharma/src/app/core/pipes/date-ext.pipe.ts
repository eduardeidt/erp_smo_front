import { Pipe, PipeTransform } from '@angular/core';
import {formatDate} from "@angular/common";

function capitalize(s){
    return s[0].toUpperCase() + s.slice(1);
}

@Pipe({name: 'dateExt'})
export class DateExt implements PipeTransform {
    transform(date: string): string {
       return capitalize(formatDate(date, 'EEEE, ', 'pt-BR'))
           +formatDate(date, 'dd', 'pt-BR')
           +' de '
           +capitalize(formatDate(date,'LLLL', 'pt-BR'))
           +' de '
           +formatDate(date,'yyyy', 'pt-BR');
    }
}
