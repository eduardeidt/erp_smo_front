import {Component, EventEmitter, Input, OnInit, Output, QueryList, Renderer2, ViewChildren} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";

import {DatabaseService} from "../../services/database/database.service";
import {FunctionsService} from "../../services/functions/functions.service";
import {IconSolidService} from "../../services/icons/icon-solid.service";
import {StorageService} from "../../services/storage/storage.service";
import {storageKeys} from "../../variables/storageKeys";
import {Title} from "@angular/platform-browser";
import {IconRegularService} from "../../services/icons/icon-regular.service";

@Component({
    selector: 'fs-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {


    @Input() companyTradingName : string;
    @Input() userName: string;
    @Input() expandedMenu: boolean = false;

    public menuToggle: number = 0;
    public menuToggleVerify: number = 0;
    public userAdmin: boolean = this.storageService.decodeFromLocalStorage(storageKeys.userAdmin) == 1;

    constructor(
        private formBuilder: FormBuilder,
        private titleService: Title,
        private renderer : Renderer2,
        private router: Router,
        private databaseService: DatabaseService,
        private functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        private storageService: StorageService,
    ) {
        this.renderer.listen('window','click',()=>{
            this.toogleClass(0);
        });
    }

    ngOnInit() {
        this.userName = this.storageService.decodeFromLocalStorage(storageKeys.userName);
    }

    toogleClass(item: number) {
        this.menuToggleVerify = item == 0 ? this.menuToggleVerify + 1 : 0;
        if (item != 0) {
            if (this.menuToggle != item) {
                this.menuToggle = item;
            } else {
                this.menuToggle = 0;
            }
        }
        if (item == 0 && this.menuToggleVerify > 1) {
            this.menuToggle = 0;
        }
    }

    logout() {
        this.databaseService.logout();
    }
}
