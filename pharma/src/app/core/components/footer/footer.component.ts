import { Component, OnInit } from '@angular/core';
import {DatabaseService} from "../../services/database/database.service";

@Component({
    selector: 'fs-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    public systemVersion = this.databaseService.currentSistemVersion();

    constructor(
        private databaseService: DatabaseService,
    ) { }

    ngOnInit() { }

}
