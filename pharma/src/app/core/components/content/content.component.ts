import {Component, OnInit} from '@angular/core';

import {DatabaseService} from "../../services/database/database.service";
import {LoadingScreenService} from "../../../shared/services/loading-screen/loading-screen.service";
import {StorageService} from "../../services/storage/storage.service";

@Component({
    selector: 'fs-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

    public subscription: any;
    public loadingScreen: boolean = false;
    public time;

    constructor(
        public databaseService: DatabaseService,
        private loadingScreenService: LoadingScreenService,
        public storageService: StorageService,
    ) { }

    ngOnInit() {
        this.subscription = this.databaseService.getLoadingOn().subscribe(val => this.setLoadingOn(val));
    }

    setLoadingOn(val) {
        if (val) {
            this.loadingScreenService.show();
            return;
        }
        clearTimeout(this.time);
        this.time = setTimeout(() => {
            this.loadingScreenService.hide();
        },500);
    }

}
