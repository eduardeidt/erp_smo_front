import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Router} from "@angular/router";

import {DatabaseService} from "../../services/database/database.service";
import {FunctionsService} from "../../services/functions/functions.service";
import {IconRegularService} from "../../services/icons/icon-regular.service";
import {IconSolidService} from "../../services/icons/icon-solid.service";
import {StorageService} from "../../services/storage/storage.service";
import {storageKeys} from "../../variables/storageKeys";
import { environment } from 'src/environments/environment';

@Component({
    selector: 'fs-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    public currentDate: string;
    public menuUserActive: boolean = false;
    public previewUrl: string = '/assets/img/photo-user.png';
    public userName: string = '';
    isHomologation: boolean = !environment.production;

    @ViewChild ('menuUser',{static: false}) menuUser : ElementRef;

    constructor(
        public redender: Renderer2,
        public databaseService : DatabaseService,
        public router: Router,
        public functionsService: FunctionsService,
        public iconRegularService: IconRegularService,
        public iconSolidService: IconSolidService,
        public storageService: StorageService,
    ){
        this.redender.listen('window','click',(e:Event)=>{
            if (e.target != this.menuUser.nativeElement) {
                this.menuUserActive = false;
            }
        });
    }

    ngOnInit(): void {
        let today = new Date();
        this.currentDate = this.functionsService.getFullDateBR(this.functionsService.getDayBefore(today));
        this.userName = this.storageService.decodeFromLocalStorage(storageKeys.userName);
    }

    resetUrlImg(): void {
        this.previewUrl = '/assets/img/photo-user.png';
    }

    syncData(): void{
        this.databaseService.getSync({sync: 1},(res) => {
            if (!res.error) {
                console.log('success');
                this.functionsService.openSnackBar('Dados sincronizados com sucesso!');
            } else {
                console.log(res.error);
            }
        })
    }

    toggleMenuUser(){
        this.menuUserActive = !this.menuUserActive;
    }

    logout(){
        this.databaseService.logout();
    }
}
