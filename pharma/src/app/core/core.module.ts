import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";

import {DateExt} from "./pipes/date-ext.pipe";
import {ContentComponent} from './components/content/content.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FooterComponent} from "./components/footer/footer.component";
import {HeaderComponent} from "./components/header/header.component";
import {MenuComponent} from "./components/menu/menu.component";
import {NgxSpinnerModule} from "ngx-spinner";
import {SharedModule} from "../shared/shared.module";
import {TextContentComponent} from './components/text-content/text-content.component';

@NgModule({
    declarations: [
        DateExt,
        ContentComponent,
        HeaderComponent,
        MenuComponent,
        FooterComponent,
        TextContentComponent
    ],
    exports: [
        DateExt,
        HeaderComponent,
        FooterComponent,
    ],
    imports: [

        CommonModule,
        RouterModule,
        SharedModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        NgxSpinnerModule,
    ]
})
export class CoreModule { }
